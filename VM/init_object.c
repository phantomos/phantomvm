﻿#include "init_object.h"
// for pvm_create_ostack_object(), pvm_create_istack_object()
// for data area definitions
#include "internal_da.h"
// for pvm_get_null_class(), pvm_get_null_object()
#include "root.h"
// for pvm_create_object()
#include "object_create.h"
// for hdir_init etc. additional functions for directory class.
#include "directory.h"
// for memset
#include <stdio.h>
// for errno_t
//#include <corecrt.h>
typedef int errno_t; // extracted from corecrt.h

//bakulev
#define assert(value) //bakulev

// -------- Init and restart function (methods) for NULL class. --------

void pvm_internal_init_void(struct pvm_object_storage * os)
{
    (void)os;
}

// -------- Init and restart function (methods) for CLASS class. --------

void pvm_internal_init_class(struct pvm_object_storage * os)
{
    	struct data_area_4_class *      da = (struct data_area_4_class *)os->da;
    
    	da->object_data_area_size   	= 0;
    	da->object_flags     		= 0;
    	da->object_default_interface 	= pvm_get_null_class();
    	da->sys_table_id             	= -1;
    
    	da->class_name 			= pvm_get_null_object();
            da->class_parent		= pvm_get_null_class();
    
            da->static_vars                 = pvm_create_object( pvm_get_array_class() );
}

// -------- Init and restart function (methods) for INTERFACE class. --------

void pvm_internal_init_interface(struct pvm_object_storage * os)
{
    	memset( os->da, 0, os->_da_size );
}

// -------- Init and restart function (methods) for CODE class. --------

void pvm_internal_init_code(struct pvm_object_storage * os)
{
    	struct data_area_4_code *      da = (struct data_area_4_code *)os->da;
    
    	da->code_size     			= 0;
}

// -------- Init and restart function (methods) for INT class. --------

void pvm_internal_init_int(struct pvm_object_storage * os)
{
        (void)os;
}

// -------- Init and restart function (methods) for STRING class. --------

void pvm_internal_init_string(struct pvm_object_storage * os)
{
    	struct data_area_4_string* data_area = (struct data_area_4_string*)&(os->da);
    
    	memset( (void *)data_area, 0, os->_da_size );
    	data_area->length = 0;
}

// -------- Init and restart function (methods) for ARRAY class. --------

void pvm_internal_init_array(struct pvm_object_storage * os)
{
    	struct data_area_4_array *      da = (struct data_area_4_array *)os->da;
    
    	da->used_slots     			= 0;
    	da->page_size                       = 16;
    	da->page.data                       = 0;
}

// -------- Init and restart function (methods) for PAGE class. --------

void pvm_internal_init_page(struct pvm_object_storage * os)
{
    	assert( ((os->_da_size) % sizeof(struct pvm_object)) == 0); // Natural num of
    
    	int n_slots = (os->_da_size) / sizeof(struct pvm_object);
    	struct pvm_object * data_area = (struct pvm_object *)&(os->da);
    
    	int i;
    	for( i = 0; i < n_slots; i++ )
    		data_area[i] = pvm_get_null_object();
}

// -------- Init and restart function (methods) for THREAD class. --------

void pvm_internal_init_thread(struct pvm_object_storage * os)
{
    struct data_area_4_thread *      da = (struct data_area_4_thread *)os->da;
    
    //bakulev hal_spin_init(&da->spin);
#if OLD_VM_SLEEP
        da->sleep_flag                      = 0;
#endif
    //hal_cond_init(&(da->wakeup_cond), "VmThrdWake");
    
    da->call_frame   			= pvm_create_call_frame_object();
    da->stack_depth				= 1;
    
    da->owner.data = 0;
    da->environment.data = 0;
    
    da->code.code     			= 0;
    da->code.IP 			= 0;
    da->code.IP_max             	= 0;
    
    //da->_this_object 			= pvm_get_null_object();
    
    pvm_exec_load_fast_acc(da); // Just to fill shadows with something non-null
}

// -------- Init and restart function (methods) for CALL_FRAME class. --------

void pvm_internal_init_call_frame(struct pvm_object_storage * os)
{
    	//struct data_area_4_call_frame *da = pvm_data_area( os, call_frame );
    	struct data_area_4_call_frame *da = (struct data_area_4_call_frame *)&(os->da);
    
    	da->IP_max = 0;
    	da->IP = 0;
    	da->code = 0;
    
    	da->this_object = pvm_get_null_object();
    	da->prev = pvm_get_null_object();
    
    	da->istack = pvm_create_istack_object();
    	da->ostack = pvm_create_ostack_object();
    	da->estack = pvm_create_estack_object();
}

// -------- Init and restart function (methods) for ISTACK class. --------

void pvm_internal_init_call_istack(struct pvm_object_storage * os)
{
    
}

// -------- Init and restart function (methods) for OSTACK class. --------

void pvm_internal_init_call_ostack(struct pvm_object_storage * os)
{
    
}

// -------- Init and restart function (methods) for ESTACK class. --------

void pvm_internal_init_call_estack(struct pvm_object_storage * os)
{
    
}

// -------- Init and restart function (methods) for LONG class. --------

void pvm_internal_init_long(struct pvm_object_storage * os)
{
        (void)os;
}

// -------- Init and restart function (methods) for FLOAT class. --------

void pvm_internal_init_float(struct pvm_object_storage * os)
{
        (void)os;
}

// -------- Init and restart function (methods) for DOUBLE class. --------

void pvm_internal_init_double(struct pvm_object_storage * os)
{
        (void)os;
}

// -------- Init and restart function (methods) for BOOT class. --------

void pvm_internal_init_boot(struct pvm_object_storage * os)
{
         (void)os;
         // Nohing!
}

// -------- Init and restart function (methods) for TTY class. --------

void pvm_internal_init_tty(struct pvm_object_storage * os)
{
    
}
void pvm_restart_tty(pvm_object_t o)
{
    
}

// -------- Init and restart function (methods) for MUTEX class. --------

void pvm_internal_init_mutex(struct pvm_object_storage * os)
{
        struct data_area_4_mutex *      da = (struct data_area_4_mutex *)os->da;
    
        da->waiting_threads_array = pvm_create_object( pvm_get_array_class() );
}

// -------- Init and restart function (methods) for COND class. --------

void pvm_internal_init_cond(struct pvm_object_storage * os)
{
        struct data_area_4_cond *      da = (struct data_area_4_cond *)os->da;
    
        da->waiting_threads_array = pvm_create_object( pvm_get_array_class() );
}

// -------- Init and restart function (methods) for BINARY class. --------

void pvm_internal_init_binary(struct pvm_object_storage * os)
{
        (void)os;
        //struct data_area_4_binary *      da = (struct data_area_4_binary *)os->da;
        // empty
}

// -------- Init and restart function (methods) for BITMAP class. --------

void pvm_internal_init_bitmap(struct pvm_object_storage * os)
{
        (void)os;
        // Nothing
}

// -------- Init and restart function (methods) for WORLD class. --------

void pvm_internal_init_world(struct pvm_object_storage * os)
{
        (void)os;
        //struct data_area_4_binary *      da = (struct data_area_4_binary *)os->da;
        // empty
}

// -------- Init and restart function (methods) for CLOSURE class. --------

void pvm_internal_init_closure(struct pvm_object_storage * os)
{
    	struct data_area_4_closure *      da = (struct data_area_4_closure *)os->da;
    	da->object.data = 0;
}

// -------- Init and restart function (methods) for WINDOW class. --------

void pvm_internal_init_window(struct pvm_object_storage * os)
{
    struct data_area_4_window      *da = (struct data_area_4_window *)os->da;
    /*bakulev
    strlcpy( da->title, "Window", sizeof(da->title) );
    
    //da->w.title = da->title;
    da->fg = COLOR_BLACK;
    da->bg = COLOR_WHITE;
    da->x = 0;
    da->y = 0;
    
    drv_video_window_init( &(da->w), PVM_DEF_TTY_XSIZE, PVM_DEF_TTY_YSIZE, 100, 100, da->bg, WFLAG_WIN_DECORATED, da->title );
    
    
    {
    pvm_object_t o;
    o.data = os;
    o.interface = pvm_get_default_interface(os).data;
    
    da->connector = pvm_create_connection_object();
    struct data_area_4_connection *cda = (struct data_area_4_connection *)da->connector.data->da;
    
    phantom_connect_object_internal(cda, 0, o, 0);
    
    // This object needs OS attention at restart
    // TODO do it by class flag in create fixed or earlier?
    pvm_add_object_to_restart_list( o );
    }
	*/
}
void pvm_restart_window(pvm_object_t o)
{
    //bakulev pvm_add_object_to_restart_list( o ); // Again!
    /*bakulev
    struct data_area_4_window *da = pvm_object_da( o, window );
    
    printf("restart WIN\n");
    
    w_restart_init( &da->w );
    
    da->w.title = da->title; // must be correct in snap? don't reset?
    
    / *
    queue_init(&(da->w.events));
    da->w.events_count = 0;
    
    iw_enter_allwq( &da->w );
    
    //event_q_put_win( 0, 0, UI_EVENT_WIN_REPAINT, &da->w );
    ev_q_put_win( 0, 0, UI_EVENT_WIN_REDECORATE, &da->w );
    *bakulev/
    w_restart_attach( &da->w );
	*/
}

// -------- Init and restart function (methods) for DIRECTORY class. --------

void pvm_restart_directory(pvm_object_t o)
{
    //struct data_area_4_directory *da = pvm_object_da( o, directory );
    assert(0);
}

// -------- Init and restart function (methods) for CONNECTION class. --------

void pvm_internal_init_connection(struct pvm_object_storage * os)
{
        struct data_area_4_connection      *da = (struct data_area_4_connection *)os->da;
    
        da->kernel = 0;
        memset( da->name, 0, sizeof(da->name) );
}
void pvm_restart_connection(pvm_object_t o)
{
	/*bakulev
    struct data_area_4_connection *da = pvm_object_da( o, connection );
	printf("restarting connection");
    da->kernel = 0;
    
    int ret = pvm_connect_object(o,0);
    
    if( ret )
        printf("reconnect failed %d\n", ret );
    
    //#warning restart!
	*/
}

// -------- Init and restart function (methods) for SEMA class. --------

void pvm_internal_init_sema(struct pvm_object_storage * os)
{
        struct data_area_4_sema *      da = (struct data_area_4_sema *)os->da;
    
        da->waiting_threads_array = pvm_create_object( pvm_get_array_class() );
}

