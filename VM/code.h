#pragma once

#include "object.h"
#include "internal_da.h"

struct vm_code_linenum
{
	long        ip;
	int         line;
};

int pvm_code_do_get_int(const unsigned char *addr);

unsigned long long pvm_code_do_get_int64(const unsigned char *addr);

void pvm_code_check_bounds(struct pvm_code_handler *code, unsigned int ip, char *where);

unsigned char pvm_code_get_byte_speculative(struct pvm_code_handler *code);

unsigned char pvm_code_get_byte_speculative2(struct pvm_code_handler *code);

unsigned char pvm_code_get_byte(struct pvm_code_handler *code);

int pvm_code_get_int32(struct pvm_code_handler *code);

int64_t pvm_code_get_int64(struct pvm_code_handler *code);

unsigned int pvm_code_get_rel_IP_as_abs(struct pvm_code_handler *code);

struct pvm_object pvm_code_get_string(struct pvm_code_handler *code);
