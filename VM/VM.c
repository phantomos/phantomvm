// VM.cpp : Defines the entry point for the console application.
//

// Including SDKDDKVer.h defines the highest available Windows platform.

// If you wish to build your application for a previous Windows platform, include WinSDKVer.h and
// set the _WIN32_WINNT macro to the platform you wish to support before including SDKDDKVer.h.

#include <SDKDDKVer.h>

#include <stdio.h>
#include <tchar.h>
#include <stdlib.h> // malloc
#include "root.h"
#include "alloc.h"

static size_t size = 220 * 1024 * 1024;
static void *mem;

// -----------------------------------------------------------------------
// bulk

void *bulk_code;
size_t bulk_size;
void *bulk_read_pos;

size_t bulk_seek_f(size_t pos)
{
	bulk_read_pos = (char *)bulk_code + pos;
	return bulk_read_pos >= (void *)((char *)bulk_code + bulk_size);
}

size_t bulk_read_f(size_t count, void *data)
{
	//if (count < 0) return -1;

	size_t left = ((char *)bulk_code + bulk_size) - (char *)bulk_read_pos;

	if (count > left) count = left;

	memcpy(data, bulk_read_pos, count);

	bulk_read_pos = (char *)bulk_read_pos + count;

	return count;
}

static void *dm_mem, *dm_copy;
static int dm_size = 0;
void setDiffMem(void *mem, void *copy, int size)
{
	dm_mem = mem;
	dm_copy = copy;
	dm_size = size;
}

int main_envc;
const char **main_env;

int main()
{
	printf("\n\n\n\n----- Phantom exec test v. 0.5\n\n");

	/*
	run_init_functions(INIT_LEVEL_PREPARE);
	run_init_functions(INIT_LEVEL_INIT); // before video

										 //drv_video_win32.mouse = mouse_callback;
										 //video_drv = &drv_video_win32;
										 //video_drv = &drv_video_x11;
	*/
	//pvm_bulk_init(bulk_seek_f, bulk_read_f);
	/*
	pvm_video_init();
	video_drv->mouse = mouse_callback;

	drv_video_init_windows();
	init_main_event_q();
	init_new_windows();

	scr_mouse_set_cursor(drv_video_get_default_mouse_bmp());

	*/
	mem = malloc(size + 1024 * 10);
	setDiffMem(mem, malloc(size + 1024 * 10), size);

	//hal_init(mem, size); ( vmem_ptr_t va, long vs )
	//printf("Win32 HAL init @%p\n", va);

	//InitializeCriticalSection(&default_critical);

	//win_hal_init();

	//hal.object_vspace = mem;
	//hal.object_vsize = size;

	pvm_alloc_init( mem, size );


	//int rc =
	//CreateThread( 0, 0, (void *) &winhal_debug_srv_thread, 0, 0, 0);
	//if( rc) printf("Win32 can't run debugger thread\n");

	//hal_start_kernel_thread( (void*)&winhal_debug_srv_thread );
	//hal_init(mem, size); end

	//pvm_alloc_threaded_init(); // no threads yet - no lock
	/*
	run_init_functions(INIT_LEVEL_LATE);

	char *dir = getenv("PHANTOM_HOME");
	char *rest = "plib/bin/classes";

	if (dir == NULL)
	{
		dir = "pcode";
		rest = "classes";
	}

	char fn[1024];
	snprintf(fn, 1024, "%s/%s", dir, rest);
	*/
	/*
	if (load_code(&bulk_code, &bulk_size, fn)) //"pcode/classes") )
	{
		printf("No bulk classes file '%s'\n", fn);
		exit(22);
	}
	bulk_read_pos = bulk_code;
	*/
	
	pvm_root_init();

	// TODO use stray catcher in pvm_test too
	//stray();
	
	getchar();

    return 0;
}

