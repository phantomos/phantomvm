﻿#include "gc_object.h"
// for data area definitions
#include "internal_da.h"
// for pvm_get_null_class(), pvm_get_null_object()
#include "root.h"
// for errno_t
//#include <corecrt.h>
typedef int errno_t; // extracted from corecrt.h

//bakulev
#define assert(value) //bakulev

// -------- Init and restart function (methods) for NULL class. --------


// -------- Init and restart function (methods) for CLASS class. --------

void pvm_gc_iter_class(gc_iterator_call_t func, struct pvm_object_storage * os, void *arg)
{
    	#define gc_fcall( f, arg, o )   f( o, arg )
    
    	struct data_area_4_class *da = (struct data_area_4_class *)&(os->da);
    	gc_fcall( func, arg, da->object_default_interface );
    	gc_fcall( func, arg, da->class_name );
    	gc_fcall( func, arg, da->class_parent );
    
            gc_fcall( func, arg, da->static_vars );
    
            gc_fcall( func, arg, da->ip2line_maps );
            gc_fcall( func, arg, da->method_names );
            gc_fcall( func, arg, da->field_names );
}

// -------- Init and restart function (methods) for INTERFACE class. --------

void pvm_gc_iter_interface(gc_iterator_call_t func, struct pvm_object_storage * os, void *arg)
{
        (void)func;
        (void)os;
        (void)arg;
        // Empty
        // Must not be called - this is not really an internal object
        //bakulev panic("Interface GC iterator called");
}

// -------- Init and restart function (methods) for CODE class. --------


// -------- Init and restart function (methods) for INT class. --------


// -------- Init and restart function (methods) for STRING class. --------


// -------- Init and restart function (methods) for ARRAY class. --------

void pvm_gc_iter_array(gc_iterator_call_t func, struct pvm_object_storage * os, void *arg)
{
    	#define gc_fcall( f, arg, o )   f( o, arg )
    
    	struct data_area_4_array *da = (struct data_area_4_array *)&(os->da);
    	if(da->page.data != 0)
    		gc_fcall( func, arg, da->page );
}

// -------- Init and restart function (methods) for PAGE class. --------

void pvm_gc_iter_page(gc_iterator_call_t func, struct pvm_object_storage * os, void *arg)
{
    	int n_slots = (os->_da_size) / sizeof(struct pvm_object);
    	struct pvm_object * data_area = (struct pvm_object *)&(os->da);
    
    	int i;
    	for( i = 0; i < n_slots; i++ )
    	{
    		func( data_area[i], arg );
    	}
}

// -------- Init and restart function (methods) for THREAD class. --------

void pvm_gc_iter_thread(gc_iterator_call_t func, struct pvm_object_storage * os, void *arg)
{
    	#define gc_fcall( f, arg, o )   f( o, arg )
    
    	struct data_area_4_thread *da = (struct data_area_4_thread *)&(os->da);
    	gc_fcall( func, arg, da->call_frame );
    	gc_fcall( func, arg, da->owner );
    	gc_fcall( func, arg, da->environment );
}

// -------- Init and restart function (methods) for CALL_FRAME class. --------

void pvm_gc_iter_call_frame(gc_iterator_call_t func, struct pvm_object_storage * os, void *arg)
{
    	#define gc_fcall( f, arg, o )   f( o, arg )
    
    	struct data_area_4_call_frame *da = (struct data_area_4_call_frame *)&(os->da);
    	gc_fcall( func, arg, da->this_object );
    	gc_fcall( func, arg, da->prev ); // FYI - shall never be followed in normal situation, must contain zero data ptr if being considered by refcount
    	gc_fcall( func, arg, da->istack );
    	gc_fcall( func, arg, da->ostack );
    	gc_fcall( func, arg, da->estack );
}

// -------- Init and restart function (methods) for ISTACK class. --------

void pvm_gc_iter_call_istack(gc_iterator_call_t func, struct pvm_object_storage * os, void *arg)
{
    
}

// -------- Init and restart function (methods) for OSTACK class. --------

void pvm_gc_iter_call_ostack(gc_iterator_call_t func, struct pvm_object_storage * os, void *arg)
{
    
}

// -------- Init and restart function (methods) for ESTACK class. --------

void pvm_gc_iter_call_estack(gc_iterator_call_t func, struct pvm_object_storage * os, void *arg)
{
    
}

// -------- Init and restart function (methods) for LONG class. --------


// -------- Init and restart function (methods) for FLOAT class. --------


// -------- Init and restart function (methods) for DOUBLE class. --------


// -------- Init and restart function (methods) for BOOT class. --------


// -------- Init and restart function (methods) for TTY class. --------

void pvm_gc_finalizer_tty(struct pvm_object_storage * os)
{
    
}

// -------- Init and restart function (methods) for MUTEX class. --------


// -------- Init and restart function (methods) for COND class. --------


// -------- Init and restart function (methods) for BINARY class. --------


// -------- Init and restart function (methods) for BITMAP class. --------

void pvm_gc_iter_call_bitmap(gc_iterator_call_t func, struct pvm_object_storage * os, void *arg)
{
    
}

// -------- Init and restart function (methods) for WORLD class. --------


// -------- Init and restart function (methods) for CLOSURE class. --------

void pvm_gc_iter_call_closure(gc_iterator_call_t func, struct pvm_object_storage * os, void *arg)
{
    
}

// -------- Init and restart function (methods) for WINDOW class. --------

void pvm_gc_iter_call_window(gc_iterator_call_t func, struct pvm_object_storage * os, void *arg)
{
    
}
void pvm_gc_finalizer_window(struct pvm_object_storage * os)
{
    // is it called?
    struct data_area_4_window      *da = (struct data_area_4_window *)os->da;
    //bakulev drv_video_window_destroy(&(da->w));
}

// -------- Init and restart function (methods) for DIRECTORY class. --------

void pvm_gc_iter_call_directory(gc_iterator_call_t func, struct pvm_object_storage * os, void *arg)
{
    
}
void pvm_gc_finalizer_directory(struct pvm_object_storage * os)
{
        //struct data_area_4_window      *da = (struct data_area_4_window *)os->da;
        assert(0);
}

// -------- Init and restart function (methods) for CONNECTION class. --------

void pvm_gc_iter_call_connection(gc_iterator_call_t func, struct pvm_object_storage * os, void *arg)
{
    
}
void pvm_gc_finalizer_connection(struct pvm_object_storage * os)
{
        // is it called?
        struct data_area_4_connection *da = (struct data_area_4_connection *)os->da;
        //#warning disconnect!
		errno_t ret = 0; //bakulev phantom_disconnect_object(da);
        if( ret )
            printf("automatic disconnect failed - %d\n", ret );
}

// -------- Init and restart function (methods) for SEMA class. --------


