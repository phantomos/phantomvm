#pragma once

// for int32_t
#include <stdint.h>
//struct pvm_object_storage;

// This structure must be first in any object
// for garbage collector to work ok

#define PVM_OBJECT_START_MARKER 0x7FAA7F55

// TODO add two bytes after flags to assure alignment
struct object_PVM_ALLOC_Header
{
	unsigned int                object_start_marker;
	volatile int32_t            refCount; // for fast dealloc of locally-owned objects. If grows to INT_MAX it will be fixed at that value and ignored further. Such objects will be GC'ed in usual way
	unsigned char               alloc_flags;
	unsigned char               gc_flags;
	unsigned int                exact_size; // full object size including this header
};

// This is object itself.
//
//   	_ah is allocation header, used by allocator/gc
//   	_class is class object reference.
//      _satellites is used to keep some related things such as weak ptr backlink
//      _flags used to keep some shortcut info about object type
//      _da_size is n of bytes in da[]
//      da[] is object contents
//
// NB! See JIT assembly hardcode for object structure offsets

struct pvm_object_storage; // forward declaration

						   // This struct is poorly named. In fact, it is an object reference!
struct pvm_object
{
	struct pvm_object_storage	* data;
	struct pvm_object_storage	* interface; // method list is here
};

typedef struct pvm_object pvm_object_t;

struct pvm_object_storage
{
	struct object_PVM_ALLOC_Header      _ah;

	struct pvm_object                   _class;
	struct pvm_object                   _satellites; // Points to chain of objects related to this one
	uint32_t                            _flags;
	size_t                             _da_size; // in bytes!

	unsigned char                       da[];
};

typedef struct pvm_object_storage pvm_object_storage_t;