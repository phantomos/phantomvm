#include "directory.h"
// for pvm_create_array_object() and pvm_create_object( pvm_get_array_class() )
#include "root.h"
// for pvm_get_str_len(), pvm_get_str_data()
#include "object_tools.h"
// for pvm_set_field(), pvm_get_field(), etc.
#include "object_create.h"
// for calloc
#include <stdio.h>  
#include <malloc.h>  
// for ENOENT
//#include <errno.h>
#define	ENOENT		2		/* No such file or directory */  // extracted from errno.h
#define EEXIST          17 // extracted from errno.h

//bakulev #warning lock
#define LOCK_DIR(__dir) //bakulev hal_wired_spin_lock(&(__dir)->lock);
#define UNLOCK_DIR(__dir) //bakulev hal_wired_spin_unlock(&(__dir)->lock);
//bakulev
#define assert(value)
#define SHOW_ERROR(message)

//#warning SYS_FREE_O() for removed values - array access funcs do it
//bakulev #warning do we need ref_inc for both saved objects and returned copies in get ?


/**
*
* BUG! TODO no mutex, TODO general short-term sync primitive for persistent mem
*
* General design:
*
* 3 arrays, array of strings (keys), array of obvects (values), bit array of flags - single or multiple value in dir entry.
* Hash func points to array slot.
* If we have just one entry for given key, just hold it and its key in corresponding arrays. Have 0 in multiple flag array.
* If we have more than one entry for given key, hold 2nd level arrays (for values and keys) in corresponding arrays. Have 1 in multiple flag array.
* In 2nd level arrays key and value positions are correspond too. Null in key slot menas that slot is unused.
*
**/

// TODO add parameter for find and remove mode
errno_t hdir_find(hashdir_t *dir, const char *ikey, size_t i_key_len, pvm_object_t *out, int delete_found)
{
	if (dir->nEntries == 0) return ENOENT;

	assert(dir->capacity);
	assert(dir->keys.data != 0);
	assert(dir->values.data != 0);
	assert(dir->flags != 0);

	LOCK_DIR(dir);

	int keypos = calc_hash(ikey, ikey + i_key_len) % dir->capacity;

	// Can't access array slot out of array's real size
	int kasize = get_array_size(dir->keys.data);
	if (keypos >= kasize)
		goto not_found;

	pvm_object_t okey = pvm_get_array_ofield(dir->keys.data, keypos);
	if (pvm_is_null(okey))
	{
	not_found:
		UNLOCK_DIR(dir);
		return ENOENT;
	}

	uint8_t flags = dir->flags[keypos];

	// No indirection?
	if (flags == 0)
	{
		if (0 == hdir_cmp_keys(ikey, i_key_len, okey))
		{
			*out = pvm_get_array_ofield(dir->values.data, keypos);
			ref_inc_o(*out);
			if (delete_found)
			{
				pvm_set_array_ofield(dir->values.data, keypos, pvm_create_null_object());
				pvm_set_array_ofield(dir->keys.data, keypos, pvm_create_null_object());
				dir->nEntries--;
			}
			UNLOCK_DIR(dir);
			return 0;
		}
	}

	// Indirect, find by linear search in 2nd level array
	// okey is arrray

	pvm_object_t keyarray = pvm_get_array_ofield(dir->keys.data, keypos);
	pvm_object_t valarray = pvm_get_array_ofield(dir->values.data, keypos);
	if (pvm_is_null(valarray))
	{
		printf("keyarray exists, valertray not"); // don't panic ;), but actually it is inconsistent
		UNLOCK_DIR(dir);
		return ENOENT;
	}

	size_t indir_size = get_array_size(keyarray.data);

	int i;
	for (i = 0; i < indir_size; i++)
	{
		pvm_object_t indir_key = pvm_get_array_ofield(keyarray.data, i);

		if (0 == hdir_cmp_keys(ikey, i_key_len, indir_key))
		{
			*out = pvm_get_array_ofield(valarray.data, i);
			ref_inc_o(*out);
			if (delete_found)
			{
				pvm_set_array_ofield(valarray.data, i, pvm_create_null_object());
				pvm_set_array_ofield(keyarray.data, i, pvm_create_null_object());
				dir->nEntries--;
			}
			UNLOCK_DIR(dir);
			return 0;
		}
	}

	UNLOCK_DIR(dir);
	return ENOENT;

}

//! Return EEXIST if dup
errno_t hdir_add(hashdir_t *dir, const char *ikey, size_t i_key_len, pvm_object_t add)
{
	pvm_object_t okey;

	assert(dir->capacity);
	assert(dir->keys.data != 0);
	assert(dir->values.data != 0);
	assert(dir->flags != 0);

	LOCK_DIR(dir);

	int keypos = calc_hash(ikey, ikey + i_key_len) % dir->capacity;

	// Can't access array slot out of array's real size
	int kasize = get_array_size(dir->keys.data);
#if 1 // turn off to generate pvm_panic and, if E4C enabled, exception
	if (keypos >= kasize)
		okey = pvm_create_null_object();
	else
#endif
		okey = pvm_get_array_ofield(dir->keys.data, keypos);
	uint8_t flags = dir->flags[keypos];

	// No indirection and key is equal
	if ((!flags) && !pvm_is_null(okey))
	{
		if (0 == hdir_cmp_keys(ikey, i_key_len, okey))
		{
			UNLOCK_DIR(dir);
			return EEXIST;
		}
	}

	// No indirection and corresponding key slot is empty
	if ((!flags) && pvm_is_null(okey))
	{
		// Just put

		pvm_set_array_ofield(dir->keys.data, keypos, pvm_create_string_object_binary(ikey, i_key_len));
		pvm_set_array_ofield(dir->values.data, keypos, add);
		ref_inc_o(add);
		dir->nEntries++;

		UNLOCK_DIR(dir);
		return 0;
	}

	// No indirection, stored key exists and not equal to given
	// Need to convert 1-entry to indirection

	if ((!flags) && !pvm_is_null(okey) && (0 != hdir_cmp_keys(ikey, i_key_len, okey)))
	{
		pvm_object_t oval = pvm_get_array_ofield(dir->values.data, keypos);

		// Now create arrays
		pvm_object_t keya = pvm_create_array_object();
		pvm_object_t vala = pvm_create_array_object();

		// put old key/val
		pvm_append_array(keya.data, okey);
		pvm_append_array(vala.data, oval);

		// put new key/val
		pvm_append_array(keya.data, pvm_create_string_object_binary(ikey, i_key_len));
		pvm_append_array(vala.data, add);
		ref_inc_o(add);
		dir->nEntries++;

		pvm_set_array_ofield(dir->keys.data, keypos, keya);
		pvm_set_array_ofield(dir->values.data, keypos, vala);
		dir->flags[keypos] = 1;

		UNLOCK_DIR(dir);
		return 0;
	}

	assert(flags);

	// Indirection. Scan and check for key to exist

	size_t indir_size = get_array_size(dir->keys.data);

	int i, empty_pos = -1;
	for (i = 0; i < indir_size; i++)
	{
		pvm_object_t indir_key = pvm_get_array_ofield(dir->keys.data, i);
		if (pvm_is_null(indir_key))
		{
			empty_pos = i;
			continue;
		}

		// Have key
		if (0 == hdir_cmp_keys(ikey, i_key_len, indir_key))
		{
			UNLOCK_DIR(dir);
			return EEXIST;
		}
	}

	// put new key/val
	if (empty_pos >= 0)
	{
		pvm_set_array_ofield(dir->keys.data, empty_pos, pvm_create_string_object_binary(ikey, i_key_len));
		pvm_set_array_ofield(dir->values.data, empty_pos, add);
	}
	else
	{
		pvm_append_array(dir->keys.data, pvm_create_string_object_binary(ikey, i_key_len));
		pvm_append_array(dir->values.data, add);
	}
	dir->nEntries++;

	UNLOCK_DIR(dir);
	return 0;
}

static int hdir_cmp_keys(const char *ikey, size_t ikey_len, pvm_object_t okey)
{
	size_t okey_len = pvm_get_str_len(okey);

	if (okey_len > ikey_len) return 1;
	if (ikey_len > okey_len) return -1;

	const char *okeyp = pvm_get_str_data(okey);

	int i;
	for (i = 0; i < ikey_len; i++)
	{
		if (*okeyp > *ikey) return 1;
		if (*ikey > *okeyp) return 1;
	}

	return 0;
}

#define DEFAULT_SIZE 256

//! Return EEXIST if dup
static errno_t hdir_init(hashdir_t *dir, size_t initial_size)
{
	if (initial_size <= 10)
		initial_size = DEFAULT_SIZE;

	if (initial_size > 10000)
	{
		SHOW_ERROR(0, "hdir_init size %d", initial_size);
		initial_size = DEFAULT_SIZE;
	}

	//bakulev hal_spin_init(&dir->lock);

	LOCK_DIR(dir);

	dir->nEntries = 0;
	dir->capacity = initial_size;

	dir->keys = pvm_create_array_object();
	dir->values = pvm_create_array_object();
	dir->flags = calloc(sizeof(uint8_t), initial_size);

	UNLOCK_DIR(dir);

	return 0;
}


/*-- - HashPJW-------------------------------------------------- -
*An adaptation of Peter Weinberger's (PJW) generic hashing
*  algorithm based on Allen Holub's version. Accepts a pointer
*  to a datum to be hashed and returns an unsigned integer.
*     Modified by sandro to include datum_end
*     Taken from http ://www.ddj.com/articles/1996/9604/9604b/9604b.htm?topic=algorithms
*------------------------------------------------------------ - */
#include <limits.h>
#define BITS_IN_int     ( sizeof(int) * CHAR_BIT )
#define THREE_QUARTERS  ((int) ((BITS_IN_int * 3) / 4))
#define ONE_EIGHTH      ((int) (BITS_IN_int / 8))
#define HIGH_BITS       ( ~((unsigned int)(~0) >> ONE_EIGHTH ))

static void update_hash(unsigned int *hash_value, const char *datum)
{
	unsigned int i;

	*hash_value = (*hash_value << ONE_EIGHTH) + *datum;

	if ((i = *hash_value & HIGH_BITS) != 0)
		*hash_value =
		(*hash_value ^ (i >> THREE_QUARTERS)) &
		~HIGH_BITS;
}


unsigned int calc_hash(const char *datum, const char *datum_end)
{
	unsigned int hash_value;

	if (datum_end) {
		for (hash_value = 0; datum < datum_end; ++datum)
		{
			update_hash(&hash_value, datum);
			/*
			hash_value = ( hash_value << ONE_EIGHTH ) + *datum;
			if (( i = hash_value & HIGH_BITS ) != 0 )
			hash_value =
			( hash_value ^ ( i >> THREE_QUARTERS )) &
			~HIGH_BITS;
			*/
		}
	}
	else {
		for (hash_value = 0; *datum; ++datum)
		{
			update_hash(&hash_value, datum);
			/*
			hash_value = ( hash_value << ONE_EIGHTH ) + *datum;
			if (( i = hash_value & HIGH_BITS ) != 0 )
			hash_value =
			( hash_value ^ ( i >> THREE_QUARTERS )) &
			~HIGH_BITS;
			*/
		}
		/* and the extra null value, so we match if working by length */
		update_hash(&hash_value, datum);
		/*
		hash_value = ( hash_value << ONE_EIGHTH ) + *datum;
		if (( i = hash_value & HIGH_BITS ) != 0 )
		hash_value =
		( hash_value ^ ( i >> THREE_QUARTERS )) &
		~HIGH_BITS;
		*/
	}

	/* printf("Hash value of %s//%s is %d\n", datum, datum_end, hash_value); */
	return hash_value;
}

void pvm_internal_init_directory(struct pvm_object_storage * os)
{
	struct data_area_4_directory      *da = (struct data_area_4_directory *)os->da;

	//da->elSize = 256;
	//da->capacity = 16;
	//da->nEntries = 0;

	//da->container = pvm_create_binary_object( da->elSize * da->capacity , 0 );

	errno_t rc = hdir_init(da, 0);
	//bakulev if (rc) panic("can't create directory"); // TODO do not panic? must return errno?
}
