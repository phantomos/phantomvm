#include "object_create.h"
// for common object definitions
#include "object.h"
#include "object_flags.h"
// for pvm_internal_classes
#include "internal.h"
// for data area definitions of classes
#include "internal_da.h"
// for pvm_root
#include "root.h"
// for da_po_ptr
#include "object_tools.h"
// for opcode_sys_0
#include "opcode_ids.h"
// for pvm_internal_init
#include "init_object.h"
// for printf 
#include <stdio.h>

#define panic(value) //bakulev
#define assert(value) //bakulev

#if 1
static inline void verify_p(pvm_object_storage_t *p)
{
	/*debug_catch_object("V", p);

	if (p && !pvm_object_is_allocated_light(p))
	{
	dumpo((addr_t)p);
	//pvm_object_is_allocated_assert(p);
	panic("freed object accessed");
	}*/
}

static inline void verify_o(pvm_object_t o)
{
	if (o.data)
	{
		verify_p(o.data);
		verify_p(o.interface);
	}
}
#else
#define verify_p()
#define verify_o()
#endif

/**
*
* Class is : checks if object's class is tclass or it's parent.
*
** /
/*
int pvm_object_class_is( struct pvm_object object, struct pvm_object tclass )
{
struct pvm_object_storage *tested = object.data->_class.data;
struct pvm_object_storage *nullc = pvm_get_null_class().data;

while( !pvm_is_null( tclass ) )
{
if( tested == tclass.data )
return 1;

if( tclass.data == nullc )
break;

tclass = pvm_object_da( tclass, class )->class_parent;
}
return 0;
}
*/

int pvm_object_class_exactly_is(struct pvm_object object, struct pvm_object tclass)
{
	struct pvm_object_storage *tested = object.data->_class.data;
	//struct pvm_object_storage *nullc = pvm_get_null_class().data;

	if ((!pvm_is_null(tclass)) && (tested == tclass.data))
		return 1;

	return 0;
}

// Really need this?
int pvm_object_class_is_or_parent(struct pvm_object object, struct pvm_object tclass)
{
	struct pvm_object_storage *tested = object.data->_class.data;
	struct pvm_object_storage *nullc = pvm_get_null_class().data;

	while (!pvm_is_null(tclass))
	{
		if (tested == tclass.data)
			return 1;

		if (tclass.data == nullc)
			break;

		tclass = pvm_object_da(tclass, class)->class_parent;
	}
	return 0;
}

int pvm_object_class_is_or_child(struct pvm_object object, struct pvm_object tclass)
{
	struct pvm_object oclass = object.data->_class;
	//struct pvm_object_storage *tested = object.data->_class.data;
	struct pvm_object_storage *nullc = pvm_get_null_class().data;

	if (pvm_is_null(tclass)) return 0;

	while (1)
	{
		if (oclass.data == tclass.data)
			return 1;

		if (oclass.data == nullc)
			break;

		oclass = pvm_object_da(oclass, class)->class_parent;
	}
	return 0;
}

/**
*
* Fields access for array.
*
**/

struct pvm_object  pvm_get_array_ofield(struct pvm_object_storage *o, unsigned int slot)
{
	verify_p(o);
	if (
		!(PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL & (o->_flags)) ||
		!(PHANTOM_OBJECT_STORAGE_FLAG_IS_RESIZEABLE & (o->_flags))
		)
		pvm_exec_panic("attempt to do an array op to non-array");

	struct data_area_4_array *da = (struct data_area_4_array *)&(o->da);

	if (slot >= da->used_slots)
		pvm_exec_panic("load: array index out of bounds");

	return pvm_get_ofield(da->page, slot);
}

// TODO need semaphores here
void pvm_set_array_ofield(struct pvm_object_storage *o, unsigned int slot, struct pvm_object value)
{
	verify_p(o);
	verify_o(value);
	if (
		!(PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL & (o->_flags)) ||
		!(PHANTOM_OBJECT_STORAGE_FLAG_IS_RESIZEABLE & (o->_flags))
		)
		pvm_exec_panic("attempt to do an array op to non-array");

	if (PHANTOM_OBJECT_STORAGE_FLAG_IS_IMMUTABLE &  (o->_flags))
		pvm_exec_panic("attempt to set_array_ofield for immutable");


	struct data_area_4_array *da = (struct data_area_4_array *)&(o->da);

	// need resize?
	if (pvm_is_null(da->page) || slot >= da->page_size)
	{
		const int step = 1024;
		int new_page_size = slot < (step / 2) ? (slot * 2) : (slot + step);

		if (new_page_size < 16) new_page_size = 16;

		if ((!pvm_is_null(da->page)) && da->page_size > 0)
			//da->page = pvm_object_storage::create_page( new_page_size, da->page.data()->da_po_ptr(), da->page_size );
			da->page = pvm_create_page_object(new_page_size, (void *)&(da->page.data->da), da->page_size);
		else
			da->page = pvm_create_page_object(new_page_size, 0, 0);

		da->page_size = new_page_size;
	}

	if (slot >= da->used_slots)
	{
		// set to null all new slots except for indexed
		//for( int i = da->used_slots; i < index; i++ )
		//da->page.save(i, pvm_object());
		da->used_slots = slot + 1;
	}

	pvm_set_ofield(da->page, slot, value);
}

// TODO need semaphores here
void pvm_append_array(struct pvm_object_storage *array, struct pvm_object value_to_append)
{
	struct data_area_4_array *da = (struct data_area_4_array *)&(array->da);

	pvm_set_array_ofield(array, da->used_slots, value_to_append);
}

int get_array_size(struct pvm_object_storage *array)
{
	struct data_area_4_array *da = (struct data_area_4_array *)&(array->da);
	return da->used_slots;
}

void pvm_pop_array(struct pvm_object_storage *array, struct pvm_object value_to_pop)
{
	struct data_area_4_array *da = (struct data_area_4_array *)&(array->da);

	verify_p(array);
	if (
		!(PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL & (array->_flags)) ||
		!(PHANTOM_OBJECT_STORAGE_FLAG_IS_RESIZEABLE & (array->_flags))
		)
		pvm_exec_panic("attempt to do an array op to non-array");

	if (PHANTOM_OBJECT_STORAGE_FLAG_IS_IMMUTABLE &  (array->_flags))
		pvm_exec_panic("attempt to pop_array for immutable");

	//swap with last and decrement used_slots
	struct pvm_object *p = da_po_ptr((da->page.data)->da);
	unsigned int slot;
	for (slot = 0; slot < da->used_slots; slot++)
	{
		if ((p[slot]).data == value_to_pop.data)  //please don't leak refcnt
		{
			if (slot != da->used_slots - 1) {
				p[slot] = p[da->used_slots - 1];
			}
			da->used_slots--;
			return;
		}
	}

	pvm_exec_panic("attempt to remove non existing element from array");
}



/**
*
* Fields access for noninternal ones.
*
**/

// TODO BUG XXX - races possible, see below
struct pvm_object
	pvm_get_field(struct pvm_object_storage *o, unsigned int slot)
{
	verify_p(o);
	if (PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL & (o->_flags))
	{
		if (PHANTOM_OBJECT_STORAGE_FLAG_IS_RESIZEABLE & (o->_flags))
		{
			return pvm_get_array_ofield(o, slot);
		}
		pvm_exec_panic("attempt to load from internal");
	}

	if (slot >= da_po_limit(o))
	{
		pvm_exec_panic("save: slot index out of bounds");
	}

	verify_o(da_po_ptr(o->da)[slot]);
	return da_po_ptr(o->da)[slot];
}

// TODO BUG XXX - races possible, read obj, then other thread writes
// to slot (derements refctr and kills object), then we attempt to
// use it (even increment refctr) -> death. Need atomic (to slot write? to refcnt dec?)
// refcnt incr here
struct pvm_object
	pvm_get_ofield(struct pvm_object op, unsigned int slot)
{
	verify_o(op);
	if (PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL & ((op.data)->_flags))
	{
		if (PHANTOM_OBJECT_STORAGE_FLAG_IS_RESIZEABLE & ((op.data)->_flags))
		{
			return pvm_get_array_ofield(op.data, slot);
		}
		pvm_exec_panic("attempt to load from internal");
	}

	if (slot >= da_po_limit(op.data))
	{
		pvm_exec_panic("load: slot index out of bounds");
	}

	verify_o(da_po_ptr((op.data)->da)[slot]);
	return da_po_ptr((op.data)->da)[slot];
}


void
pvm_set_field(struct pvm_object_storage *o, unsigned int slot, struct pvm_object value)
{
	verify_p(o);
	verify_o(value);
	if (PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL & (o->_flags))
	{
		if (PHANTOM_OBJECT_STORAGE_FLAG_IS_RESIZEABLE & (o->_flags))
		{
			pvm_set_array_ofield(o, slot, value);
			return;
		}
		pvm_exec_panic("attempt to save to internal");
	}

	if (PHANTOM_OBJECT_STORAGE_FLAG_IS_IMMUTABLE &  (o->_flags))
		pvm_exec_panic("attempt to set_field for immutable");

	if (slot >= da_po_limit(o))
	{
		pvm_exec_panic("load: slot index out of bounds");
	}

	if (da_po_ptr(o->da)[slot].data)     ref_dec_o(da_po_ptr(o->da)[slot]);  //decr old value
	da_po_ptr(o->da)[slot] = value;
}

void
pvm_set_ofield(struct pvm_object op, unsigned int slot, struct pvm_object value)
{
	verify_o(op);
	verify_o(value);
	if (PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL & ((op.data)->_flags))
	{
		if (PHANTOM_OBJECT_STORAGE_FLAG_IS_RESIZEABLE & ((op.data)->_flags))
		{
			pvm_set_array_ofield(op.data, slot, value);
			return;
		}
		pvm_exec_panic("attempt to save to internal");
	}

	if (PHANTOM_OBJECT_STORAGE_FLAG_IS_IMMUTABLE &  (op.data->_flags))
		pvm_exec_panic("attempt to set_ofield for immutable");


	if (slot >= da_po_limit(op.data))
	{
		pvm_exec_panic("slot index out of bounds");
	}

	if (da_po_ptr((op.data)->da)[slot].data) ref_dec_o(da_po_ptr((op.data)->da)[slot]);  //decr old value
	da_po_ptr((op.data)->da)[slot] = value;
}


/**
*
* Create general object.
* TODO: catch for special object creation and throw?
*
**/


static struct pvm_object
pvm_object_create_fixed(struct pvm_object object_class)
{
	return pvm_object_create_dynamic(object_class, -1);
}

struct pvm_object
	pvm_object_create_dynamic(struct pvm_object object_class, int das)
{
	struct data_area_4_class *cda = (struct data_area_4_class *)(&(object_class.data->da));
	if (object_class.data->_da_size != 76)
		printf("asdf");
	if (das < 0)
		das = cda->object_data_area_size;

	unsigned int flags = cda->object_flags;

	struct pvm_object_storage * out = pvm_object_alloc(das, flags, 0);
	out->_class = object_class;
	//out->_da_size = das; // alloc does it
	//out->_flags = flags; // alloc does it

	struct pvm_object ret;
	ret.data = out;
	ret.interface = cda->object_default_interface.data;

	return ret;
}

// The following methods are from backtrace.c

// returns ord or -1
int pvm_get_method_ordinal(pvm_object_t tclass, pvm_object_t mname)
{
	struct data_area_4_class *cda = pvm_object_da(tclass, class);
	pvm_object_t mnames = cda->method_names;

	if (pvm_is_null(mnames))
		return -1;

	if (pvm_is_null(mname))
		return -1;

	if (!pvm_object_class_exactly_is(mname, pvm_get_string_class()))
		return -1;


	int nitems = get_array_size(mnames.data);
	int i;

	for (i = 0; i < nitems; i++)
	{
		pvm_object_t curr_mname = pvm_get_ofield(mnames, i);

		int diff = pvm_strcmp(curr_mname, mname);
		if (diff == 0)
			return i;
	}

	return -1;
}

pvm_object_t pvm_get_class(pvm_object_t o)
{
	if (pvm_is_null(o))
		return o;

	return o.data->_class;
}

/**
*
* The most general way to create object. Must be used if no other requirements exist.
*
**/

struct pvm_object     pvm_create_object(struct pvm_object type)
{
	struct data_area_4_class *cda = pvm_data_area(type, class);

	struct pvm_object ret = pvm_object_create_fixed(type);

	if (cda->object_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL)
	{
		// init internal object

		int rec = pvm_iclass_by_class(type.data);

		pvm_internal_classes[rec].init(ret.data);
	}

	return ret;
}






/**
*
* Create special null object.
*
**/

//__inline__
struct pvm_object     pvm_create_null_object()
{
	return pvm_root.null_object;  // already created once forever
}

struct pvm_object     pvm_create_int_object(int _value)
{
	struct pvm_object	out = pvm_object_create_fixed(pvm_get_int_class());
	((struct data_area_4_int*)&(out.data->da))->value = _value;
	return out;
}

struct pvm_object     pvm_create_long_object(int64_t _value)
{
	struct pvm_object	out = pvm_object_create_fixed(pvm_get_long_class());
	((struct data_area_4_long*)&(out.data->da))->value = _value;
	return out;
}

struct pvm_object     pvm_create_float_object(float _value)
{
	struct pvm_object	out = pvm_object_create_fixed(pvm_get_float_class());
	((struct data_area_4_float*)&(out.data->da))->value = _value;
	return out;
}

struct pvm_object     pvm_create_double_object(double _value)
{
	struct pvm_object	out = pvm_object_create_fixed(pvm_get_double_class());
	((struct data_area_4_double*)&(out.data->da))->value = _value;
	return out;
}

struct pvm_object     pvm_create_string_object_binary(const char *value, int n_bytes)
{
	int das = sizeof(struct data_area_4_string) + n_bytes;
	struct pvm_object string_class = pvm_get_string_class();

	struct pvm_object	_data = pvm_object_create_dynamic(string_class, das);

	struct data_area_4_string* data_area = (struct data_area_4_string*)&(_data.data->da);

	pvm_internal_init_string(_data.data);

	if (value)
		memcpy(data_area->data, value, data_area->length = n_bytes);

	return _data;

}

struct pvm_object     pvm_create_string_object(const char *value)
{
	return pvm_create_string_object_binary(value, strlen(value));
}

struct pvm_object     pvm_create_string_object_binary_cat(
	const char *value1, int n_bytes1,
	const char *value2, int n_bytes2
)
{
	int das = sizeof(struct data_area_4_string) + n_bytes1 + n_bytes2;
	struct pvm_object string_class = pvm_get_string_class();

	struct pvm_object	_data = pvm_object_create_dynamic(string_class, das);

	struct data_area_4_string* data_area = (struct data_area_4_string*)&(_data.data->da);

	pvm_internal_init_string(_data.data);

	if (value1)
	{
		memcpy(data_area->data, value1, data_area->length = n_bytes1);
		if (value2)
		{
			memcpy(data_area->data + n_bytes1, value2, n_bytes2);
			data_area->length += n_bytes2;
		}
	}

	return _data;

}

int pvm_strcmp(pvm_object_t s1, pvm_object_t s2)
{
	int l1 = pvm_get_str_len(s1);
	int l2 = pvm_get_str_len(s2);

	char *d1 = pvm_get_str_data(s1);
	char *d2 = pvm_get_str_data(s2);

	if (l1 > l2) return 1;
	if (l2 > l1) return -1;

	return strncmp(d1, d2, l1);
}

//struct pvm_object     pvm_create_array_object();

struct pvm_object
	pvm_create_page_object(int n_slots, struct pvm_object *init, int init_slots)
{
	int das = n_slots * sizeof(struct pvm_object);

	struct pvm_object _data = pvm_object_create_dynamic(pvm_get_page_class(), das);

	struct pvm_object * data_area = (struct pvm_object *)&(_data.data->da);

	// NB! Bug! Here is the way to set object pointers to some garbage value
	//if( init_value )	memcpy( data_area, init_value, das );
	//else            	memset( data_area, 0, das );

	assert(init_slots < n_slots);

	int i;
	for (i = 0; i < init_slots; i++)
		data_area[i] = *init++;

	for (; i < n_slots; i++)
		data_area[i] = pvm_get_null_object();

	return _data;
}


struct pvm_object     pvm_create_call_frame_object()
{
	struct pvm_object _data = pvm_object_create_fixed(pvm_get_call_frame_class());

	pvm_internal_init_call_frame(_data.data);
	return _data;
}

// interface is a quite usual phantom object.
// (just has a specific flag - for performance only!)
// we just need an internal interface creation code
// to be able to start Phantom from scratch

static struct pvm_object create_interface_worker(int n_methods)
{
	//if(debug_init) printf("create interface\n");

	int das = n_methods * sizeof(struct pvm_object);

	pvm_object_t ret = pvm_object_create_dynamic(pvm_get_interface_class(), das);
	return ret;
}

struct pvm_object     pvm_create_interface_object(int n_methods, struct pvm_object parent_class)
{
	struct pvm_object	ret = create_interface_worker(n_methods);
	struct pvm_object * data_area = (struct pvm_object *)ret.data->da;

	if (pvm_is_null(parent_class))
		pvm_exec_panic("create interface: parent is null");

	struct pvm_object_storage *base_i = ((struct data_area_4_class*)parent_class.data->da)->object_default_interface.data;

	int base_icount = da_po_limit(base_i);

	if (base_icount > n_methods)
	{
		// root classes have N_SYS_METHODS slots in interface, don't cry about that
		if (n_methods > N_SYS_METHODS)
			printf(" create interface: child has less methods (%d) than parent (%d)\n", n_methods, base_icount);
		base_icount = n_methods;
	}

	int i = 0;
	// copy methods from parent
	for (; i < base_icount; i++)
	{
		pvm_object_t baseMethod = (da_po_ptr(base_i->da))[i];
		ref_inc_o(baseMethod);
		data_area[i] = baseMethod;
	}

	// fill others with nulls
	for (; i < n_methods; i++)
		data_area[i] = pvm_get_null_object(); // null

	return ret;
}

struct pvm_object pvm_create_class_object(struct pvm_object name, struct pvm_object iface, int da_size)
{
	struct pvm_object _data = pvm_object_create_fixed(pvm_get_class_class());

	struct data_area_4_class *      da = (struct data_area_4_class *)_data.data->da;

	da->object_data_area_size = da_size;
	da->object_flags = 0;
	da->object_default_interface = iface;
	da->sys_table_id = -1;

	da->class_name = name;
	da->class_parent = pvm_get_null_class();

	return _data;
}

struct pvm_object     pvm_create_binary_object(int size, void *init)
{
	struct pvm_object ret = pvm_object_create_dynamic(pvm_get_binary_class(), size + sizeof(struct data_area_4_binary));

	struct data_area_4_binary *da = (struct data_area_4_binary *)ret.data->da;
	da->data_size = size;
	if (init != NULL) memcpy(da->data, init, size);
	return ret;
}

#if COMPILE_WEAKREF

struct pvm_object     pvm_create_weakref_object(struct pvm_object owned)
{
	if (owned.data->_satellites.data != 0)
		return owned.data->_satellites;

	struct pvm_object ret = pvm_object_create_fixed(pvm_get_weakref_class());
	struct data_area_4_weakref *da = (struct data_area_4_weakref *)ret.data->da;

	// Interlocked to make sure no races can happen
	// (ref ass'ment seems to be non-atomic)

#if WEAKREF_SPIN
	wire_page_for_addr(&da->lock);
	int ie = hal_save_cli();
	hal_spin_lock(&da->lock);
#else
	hal_mutex_lock(&da->mutex);
#endif

	// No ref inc!
	da->object = owned;
	owned.data->_satellites = ret;

#if WEAKREF_SPIN
	hal_spin_unlock(&da->lock);
	if (ie) hal_sti();
	unwire_page_for_addr(&da->lock);
#else
	hal_mutex_unlock(&da->mutex);
#endif

	return ret;
}

struct pvm_object pvm_weakref_get_object(struct pvm_object wr)
{
	struct data_area_4_weakref *da = pvm_object_da(wr, weakref);
	struct pvm_object out;

	// still crashes :(

	// HACK HACK HACK BUG - wiring target too. TODO need wire size parameter for page cross situations!
	wire_page_for_addr(&(da->object));
	wire_page_for_addr(da->object.data);

	// All we do is return new reference to our object,
	// incrementing refcount before

#if WEAKREF_SPIN
	wire_page_for_addr(&da->lock);
	int ie = hal_save_cli();
	hal_spin_lock(&da->lock);
#else
	hal_mutex_lock(&da->mutex);
#endif

	// TODO should we check refcount before and return null if zero?
	if (0 == da->object.data->_ah.refCount)
		printf("zero object in pvm_weakref_get_object\n");

	out = ref_inc_o(da->object);

#if WEAKREF_SPIN
	hal_spin_unlock(&da->lock);
	if (ie) hal_sti();
	unwire_page_for_addr(&da->lock);
#else
	hal_mutex_unlock(&da->mutex);
#endif

	unwire_page_for_addr(da->object.data);
	unwire_page_for_addr(&(da->object));

	return out;
}

#endif

struct pvm_object     pvm_create_window_object(struct pvm_object owned)
{
	pvm_object_t ret = pvm_object_create_fixed(pvm_get_window_class());
	struct data_area_4_window *da = (struct data_area_4_window *)ret.data->da;

	(void)da;

	return ret;
}

struct pvm_object     pvm_create_directory_object(void)
{
	pvm_object_t ret = pvm_object_create_fixed(pvm_get_directory_class());
	return ret;
}

struct pvm_object     pvm_create_connection_object(void)
{
	pvm_object_t ret = pvm_object_create_fixed(pvm_get_connection_class());
	return ret;
}

// -----------------------------------------------------------------------
// Specials
// -----------------------------------------------------------------------

struct pvm_object     pvm_create_code_object(int size, void *code)
{
	struct pvm_object ret = pvm_object_create_dynamic(pvm_get_code_class(), size + sizeof(struct data_area_4_code));

	struct data_area_4_code *da = (struct data_area_4_code *)ret.data->da;
	da->code_size = size;
	memcpy(da->code, code, size);
	return ret;
}

struct pvm_object     pvm_create_thread_object(struct pvm_object start_cf)
{
	struct pvm_object ret = pvm_object_create_fixed(pvm_get_thread_class());
	struct data_area_4_thread *da = (struct data_area_4_thread *)ret.data->da;

	da->call_frame = start_cf;
	da->stack_depth = 1;

	pvm_exec_load_fast_acc(da);

	// add to system threads list
	pvm_append_array(pvm_root.threads_list.data, ret);
	ref_inc_o(ret);

	// not for each and every one
	//phantom_activate_thread(ret);

	return ret;
}

void   pvm_release_thread_object(struct pvm_object thread)
{
	// the thread was decr once... while executing class loader code... TODO: should be rewritten!

	// remove from system threads list.
	pvm_pop_array(pvm_root.threads_list.data, thread);

	ref_dec_o(thread);  //free now
}

/**
*
* Create special syscall-only interface for internal classes.
*
**/

static struct pvm_object pvm_create_syscall_code(int sys_num);

// creates interface such that each method
// has just syscall (with a number corresponding to method no)
// and return
void pvm_fill_syscall_interface(struct pvm_object iface, int syscall_count)
{
	struct pvm_object *da = (struct pvm_object *)iface.data->da;

	int i;
	for (i = 0; i < syscall_count; i++)
		da[i] = pvm_create_syscall_code(i);
}

// syscall code segment generator
static struct pvm_object pvm_create_syscall_code(int sys_num)
{

	if (sys_num <= 15)
	{
		char code[2];
		code[0] = opcode_sys_0 + (unsigned char)sys_num;
		code[1] = opcode_ret;
		return pvm_create_code_object(sizeof(code), code);
	}
	else
	{
		char code[3];
		code[0] = opcode_sys_8bit;
		code[1] = (unsigned char)sys_num;
		code[2] = opcode_ret;
		return pvm_create_code_object(sizeof(code), code);
	}

}

struct pvm_object
	pvm_copy_object(struct pvm_object in_object)
{
	// TODO ERROR throw!
	if (in_object.data->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL)
		panic("internal object copy?!");

	//ref_inc_o( in_object.data->_class );  //increment if class is refcounted
	struct pvm_object in_class = in_object.data->_class;
	int da_size = in_object.data->_da_size;

	struct pvm_object out = pvm_object_create_dynamic(in_class, da_size);

	int i = da_size / sizeof(struct pvm_object);
	for (; i >= 0; i--)
	{
		if (da_po_ptr((in_object.data)->da)[i].data)
			ref_inc_o(da_po_ptr((in_object.data)->da)[i]);
	}

	memcpy(out.data->da, in_object.data->da, da_size);
	// TODO: check for special cases - copy c'tor?

	return out;
}