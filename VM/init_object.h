﻿#pragma once

// for general object definitions
#include "object.h"

typedef void(*init_func_t)(struct pvm_object_storage * os);
typedef void(*o_restart_func_t)(pvm_object_t o);

// -------- Init and restart function (methods) for NULL class. --------

void pvm_internal_init_void(struct pvm_object_storage * os);

// -------- Init and restart function (methods) for CLASS class. --------

void pvm_internal_init_class(struct pvm_object_storage * os);

// -------- Init and restart function (methods) for INTERFACE class. --------

void pvm_internal_init_interface(struct pvm_object_storage * os);

// -------- Init and restart function (methods) for CODE class. --------

void pvm_internal_init_code(struct pvm_object_storage * os);

// -------- Init and restart function (methods) for INT class. --------

void pvm_internal_init_int(struct pvm_object_storage * os);

// -------- Init and restart function (methods) for STRING class. --------

void pvm_internal_init_string(struct pvm_object_storage * os);

// -------- Init and restart function (methods) for ARRAY class. --------

void pvm_internal_init_array(struct pvm_object_storage * os);

// -------- Init and restart function (methods) for PAGE class. --------

void pvm_internal_init_page(struct pvm_object_storage * os);

// -------- Init and restart function (methods) for THREAD class. --------

void pvm_internal_init_thread(struct pvm_object_storage * os);

// -------- Init and restart function (methods) for CALL_FRAME class. --------

void pvm_internal_init_call_frame(struct pvm_object_storage * os);

// -------- Init and restart function (methods) for ISTACK class. --------

void pvm_internal_init_call_istack(struct pvm_object_storage * os);

// -------- Init and restart function (methods) for OSTACK class. --------

void pvm_internal_init_call_ostack(struct pvm_object_storage * os);

// -------- Init and restart function (methods) for ESTACK class. --------

void pvm_internal_init_call_estack(struct pvm_object_storage * os);

// -------- Init and restart function (methods) for LONG class. --------

void pvm_internal_init_long(struct pvm_object_storage * os);

// -------- Init and restart function (methods) for FLOAT class. --------

void pvm_internal_init_float(struct pvm_object_storage * os);

// -------- Init and restart function (methods) for DOUBLE class. --------

void pvm_internal_init_double(struct pvm_object_storage * os);

// -------- Init and restart function (methods) for BOOT class. --------

void pvm_internal_init_boot(struct pvm_object_storage * os);

// -------- Init and restart function (methods) for TTY class. --------

void pvm_internal_init_tty(struct pvm_object_storage * os);
void pvm_restart_tty(pvm_object_t o);

// -------- Init and restart function (methods) for MUTEX class. --------

void pvm_internal_init_mutex(struct pvm_object_storage * os);

// -------- Init and restart function (methods) for COND class. --------

void pvm_internal_init_cond(struct pvm_object_storage * os);

// -------- Init and restart function (methods) for BINARY class. --------

void pvm_internal_init_binary(struct pvm_object_storage * os);

// -------- Init and restart function (methods) for BITMAP class. --------

void pvm_internal_init_bitmap(struct pvm_object_storage * os);

// -------- Init and restart function (methods) for WORLD class. --------

void pvm_internal_init_world(struct pvm_object_storage * os);

// -------- Init and restart function (methods) for CLOSURE class. --------

void pvm_internal_init_closure(struct pvm_object_storage * os);

// -------- Init and restart function (methods) for WINDOW class. --------

void pvm_internal_init_window(struct pvm_object_storage * os);
void pvm_restart_window(pvm_object_t o);

// -------- Init and restart function (methods) for DIRECTORY class. --------

void pvm_restart_directory(pvm_object_t o);

// -------- Init and restart function (methods) for CONNECTION class. --------

void pvm_internal_init_connection(struct pvm_object_storage * os);
void pvm_restart_connection(pvm_object_t o);

// -------- Init and restart function (methods) for SEMA class. --------

void pvm_internal_init_sema(struct pvm_object_storage * os);

