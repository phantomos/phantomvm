﻿#include "internal.h"
// include internal classes data area definitions for correct sizeof()
#include "internal_da.h"
// for object flags
#include "object_flags.h"
// string.h for strlen
#include <string.h>
// for pvm_internal_init_directory
#include "directory.h"

struct internal_class pvm_internal_classes[] =
{
	{
		".internal.void", // name of null class
		PVM_ROOT_OBJECT_NULL_CLASS, // internal number of null class
		syscall_table_4_void, &n_syscall_table_4_void, // table of syscalls funcs implementations 
		pvm_internal_init_void, // constructor func, if 0 then no func
		0, // gc iterator func, if 0 then no func
		0, // finalizer func, if 0 then no func
		0, // restart func, if 0 then no func
		0, // data area size, if 0 then size is dynamic
		PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_IMMUTABLE |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL,
		{ 0,0 } // empty object values { data = 0, interface = 0}
	},
	{
		".internal.class", // name of class class
		PVM_ROOT_OBJECT_CLASS_CLASS, // internal number of class class
		syscall_table_4_class, &n_syscall_table_4_class, // table of syscalls funcs implementations 
		pvm_internal_init_class, // constructor func, if 0 then no func
		pvm_gc_iter_class, // gc iterator func, if 0 then no func
		0, // finalizer func, if 0 then no func
		0, // restart func, if 0 then no func
		sizeof(struct data_area_4_class), // data area size, if 0 then size is dynamic
		PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_CLASS,
		{ 0,0 } // empty object values { data = 0, interface = 0}
	},
	{
		".internal.interface", // name of interface class
		PVM_ROOT_OBJECT_INTERFACE_CLASS, // internal number of interface class
		syscall_table_4_interface, &n_syscall_table_4_interface, // table of syscalls funcs implementations 
		pvm_internal_init_interface, // constructor func, if 0 then no func
		pvm_gc_iter_interface, // gc iterator func, if 0 then no func
		0, // finalizer func, if 0 then no func
		0, // restart func, if 0 then no func
		0, // data area size, if 0 then size is dynamic
		// no internal flag! TODO Immutable? |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERFACE,
		{ 0,0 } // empty object values { data = 0, interface = 0}
	},
	{
		".internal.code", // name of code class
		PVM_ROOT_OBJECT_CODE_CLASS, // internal number of code class
		syscall_table_4_code, &n_syscall_table_4_code, // table of syscalls funcs implementations 
		pvm_internal_init_code, // constructor func, if 0 then no func
		0, // gc iterator func, if 0 then no func
		0, // finalizer func, if 0 then no func
		0, // restart func, if 0 then no func
		sizeof(struct data_area_4_code), // data area size, if 0 then size is dynamic
		PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_CODE |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE,
		{ 0,0 } // empty object values { data = 0, interface = 0}
	},
	{
		".internal.int", // name of int class
		PVM_ROOT_OBJECT_INT_CLASS, // internal number of int class
		syscall_table_4_int, &n_syscall_table_4_int, // table of syscalls funcs implementations 
		pvm_internal_init_int, // constructor func, if 0 then no func
		0, // gc iterator func, if 0 then no func
		0, // finalizer func, if 0 then no func
		0, // restart func, if 0 then no func
		sizeof(struct data_area_4_int), // data area size, if 0 then size is dynamic
		PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_INT |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_IMMUTABLE,
		{ 0,0 } // empty object values { data = 0, interface = 0}
	},
	{
		".internal.string", // name of string class
		PVM_ROOT_OBJECT_STRING_CLASS, // internal number of string class
		syscall_table_4_string, &n_syscall_table_4_string, // table of syscalls funcs implementations 
		pvm_internal_init_string, // constructor func, if 0 then no func
		0, // gc iterator func, if 0 then no func
		0, // finalizer func, if 0 then no func
		0, // restart func, if 0 then no func
		sizeof(struct data_area_4_string), // data area size, if 0 then size is dynamic
		PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_STRING |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_IMMUTABLE,
		{ 0,0 } // empty object values { data = 0, interface = 0}
	},
	{
		".internal.container.array", // name of array class
		PVM_ROOT_OBJECT_ARRAY_CLASS, // internal number of array class
		syscall_table_4_array, &n_syscall_table_4_array, // table of syscalls funcs implementations 
		pvm_internal_init_array, // constructor func, if 0 then no func
		pvm_gc_iter_array, // gc iterator func, if 0 then no func
		0, // finalizer func, if 0 then no func
		0, // restart func, if 0 then no func
		sizeof(struct data_area_4_array), // data area size, if 0 then size is dynamic
		PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_DECOMPOSEABLE |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_RESIZEABLE,
		{ 0,0 } // empty object values { data = 0, interface = 0}
	},
	{
		".internal.container.page", // name of page class
		PVM_ROOT_OBJECT_PAGE_CLASS, // internal number of page class
		syscall_table_4_page, &n_syscall_table_4_page, // table of syscalls funcs implementations 
		pvm_internal_init_page, // constructor func, if 0 then no func
		pvm_gc_iter_page, // gc iterator func, if 0 then no func
		0, // finalizer func, if 0 then no func
		0, // restart func, if 0 then no func
		0, // data area size, if 0 then size is dynamic
		// PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL |
		0,
		{ 0,0 } // empty object values { data = 0, interface = 0}
	},
	{
		".internal.thread", // name of thread class
		PVM_ROOT_OBJECT_THREAD_CLASS, // internal number of thread class
		syscall_table_4_thread, &n_syscall_table_4_thread, // table of syscalls funcs implementations 
		pvm_internal_init_thread, // constructor func, if 0 then no func
		pvm_gc_iter_thread, // gc iterator func, if 0 then no func
		0, // finalizer func, if 0 then no func
		0, // restart func, if 0 then no func
		sizeof(struct data_area_4_thread), // data area size, if 0 then size is dynamic
		PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_THREAD,
		{ 0,0 } // empty object values { data = 0, interface = 0}
	},
	{
		".internal.call_frame", // name of call_frame class
		PVM_ROOT_OBJECT_CALL_FRAME_CLASS, // internal number of call_frame class
		syscall_table_4_call_frame, &n_syscall_table_4_call_frame, // table of syscalls funcs implementations 
		pvm_internal_init_call_frame, // constructor func, if 0 then no func
		pvm_gc_iter_call_frame, // gc iterator func, if 0 then no func
		0, // finalizer func, if 0 then no func
		0, // restart func, if 0 then no func
		sizeof(struct data_area_4_call_frame), // data area size, if 0 then size is dynamic
		PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_CALL_FRAME,
		{ 0,0 } // empty object values { data = 0, interface = 0}
	},
	{
		".internal.istack", // name of istack class
		PVM_ROOT_OBJECT_ISTACK_CLASS, // internal number of istack class
		syscall_table_4_istack, &n_syscall_table_4_istack, // table of syscalls funcs implementations 
		pvm_internal_init_call_istack, // constructor func, if 0 then no func
		pvm_gc_iter_call_istack, // gc iterator func, if 0 then no func
		0, // finalizer func, if 0 then no func
		0, // restart func, if 0 then no func
		sizeof(struct data_area_4_integer_stack), // data area size, if 0 then size is dynamic
		PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_STACK_FRAME,
		{ 0,0 } // empty object values { data = 0, interface = 0}
	},
	{
		".internal.ostack", // name of ostack class
		PVM_ROOT_OBJECT_OSTACK_CLASS, // internal number of ostack class
		syscall_table_4_ostack, &n_syscall_table_4_ostack, // table of syscalls funcs implementations 
		pvm_internal_init_call_ostack, // constructor func, if 0 then no func
		pvm_gc_iter_call_ostack, // gc iterator func, if 0 then no func
		0, // finalizer func, if 0 then no func
		0, // restart func, if 0 then no func
		sizeof(struct data_area_4_object_stack), // data area size, if 0 then size is dynamic
		PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_STACK_FRAME,
		{ 0,0 } // empty object values { data = 0, interface = 0}
	},
	{
		".internal.estack", // name of estack class
		PVM_ROOT_OBJECT_ESTACK_CLASS, // internal number of estack class
		syscall_table_4_estack, &n_syscall_table_4_estack, // table of syscalls funcs implementations 
		pvm_internal_init_call_estack, // constructor func, if 0 then no func
		pvm_gc_iter_call_estack, // gc iterator func, if 0 then no func
		0, // finalizer func, if 0 then no func
		0, // restart func, if 0 then no func
		sizeof(struct data_area_4_exception_stack), // data area size, if 0 then size is dynamic
		PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_STACK_FRAME,
		{ 0,0 } // empty object values { data = 0, interface = 0}
	},
	{
		".internal.long", // name of long class
		PVM_ROOT_OBJECT_LONG_CLASS, // internal number of long class
		syscall_table_4_long, &n_syscall_table_4_long, // table of syscalls funcs implementations 
		pvm_internal_init_long, // constructor func, if 0 then no func
		0, // gc iterator func, if 0 then no func
		0, // finalizer func, if 0 then no func
		0, // restart func, if 0 then no func
		sizeof(struct data_area_4_long), // data area size, if 0 then size is dynamic
		PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE |
		// removed PHANTOM_OBJECT_STORAGE_FLAG_IS_INT |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_IMMUTABLE,
		{ 0,0 } // empty object values { data = 0, interface = 0}
	},
	{
		".internal.float", // name of float class
		PVM_ROOT_OBJECT_FLOAT_CLASS, // internal number of float class
		syscall_table_4_float, &n_syscall_table_4_float, // table of syscalls funcs implementations 
		pvm_internal_init_float, // constructor func, if 0 then no func
		0, // gc iterator func, if 0 then no func
		0, // finalizer func, if 0 then no func
		0, // restart func, if 0 then no func
		sizeof(struct data_area_4_float), // data area size, if 0 then size is dynamic
		PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE |
		// removed PHANTOM_OBJECT_STORAGE_FLAG_IS_INT |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_IMMUTABLE,
		{ 0,0 } // empty object values { data = 0, interface = 0}
	},
	{
		".internal.double", // name of double class
		PVM_ROOT_OBJECT_DOUBLE_CLASS, // internal number of double class
		syscall_table_4_double, &n_syscall_table_4_double, // table of syscalls funcs implementations 
		pvm_internal_init_double, // constructor func, if 0 then no func
		0, // gc iterator func, if 0 then no func
		0, // finalizer func, if 0 then no func
		0, // restart func, if 0 then no func
		sizeof(struct data_area_4_double), // data area size, if 0 then size is dynamic
		PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE |
		// removed PHANTOM_OBJECT_STORAGE_FLAG_IS_INT |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_IMMUTABLE,
		{ 0,0 } // empty object values { data = 0, interface = 0}
	},
	{
		".internal.boot", // name of boot class
		PVM_ROOT_OBJECT_BOOT_CLASS, // internal number of boot class
		syscall_table_4_boot, &n_syscall_table_4_boot, // table of syscalls funcs implementations 
		pvm_internal_init_boot, // constructor func, if 0 then no func
		0, // gc iterator func, if 0 then no func
		0, // finalizer func, if 0 then no func
		0, // restart func, if 0 then no func
		sizeof(struct data_area_4_boot), // data area size, if 0 then size is dynamic
		PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE,
		{ 0,0 } // empty object values { data = 0, interface = 0}
	},
	{
		".internal.io.tty", // name of tty class
		PVM_ROOT_OBJECT_TTY_CLASS, // internal number of tty class
		syscall_table_4_tty, &n_syscall_table_4_tty, // table of syscalls funcs implementations 
		pvm_internal_init_tty, // constructor func, if 0 then no func
		0, // gc iterator func, if 0 then no func
		pvm_gc_finalizer_tty, // finalizer func, if 0 then no func
		pvm_restart_tty, // restart func, if 0 then no func
		sizeof(struct data_area_4_tty), // data area size, if 0 then size is dynamic
		PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE,
		{ 0,0 } // empty object values { data = 0, interface = 0}
	},
	{
		".internal.mutex", // name of mutex class
		PVM_ROOT_OBJECT_MUTEX_CLASS, // internal number of mutex class
		syscall_table_4_mutex, &n_syscall_table_4_mutex, // table of syscalls funcs implementations 
		pvm_internal_init_mutex, // constructor func, if 0 then no func
		0, // gc iterator func, if 0 then no func
		0, // finalizer func, if 0 then no func
		0, // restart func, if 0 then no func
		sizeof(struct data_area_4_mutex), // data area size, if 0 then size is dynamic
		PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE,
		{ 0,0 } // empty object values { data = 0, interface = 0}
	},
	{
		".internal.cond", // name of cond class
		PVM_ROOT_OBJECT_COND_CLASS, // internal number of cond class
		syscall_table_4_cond, &n_syscall_table_4_cond, // table of syscalls funcs implementations 
		pvm_internal_init_cond, // constructor func, if 0 then no func
		0, // gc iterator func, if 0 then no func
		0, // finalizer func, if 0 then no func
		0, // restart func, if 0 then no func
		sizeof(struct data_area_4_cond), // data area size, if 0 then size is dynamic
		PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE,
		{ 0,0 } // empty object values { data = 0, interface = 0}
	},
	{
		".internal.binary", // name of binary class
		PVM_ROOT_OBJECT_BINARY_CLASS, // internal number of binary class
		syscall_table_4_binary, &n_syscall_table_4_binary, // table of syscalls funcs implementations 
		pvm_internal_init_binary, // constructor func, if 0 then no func
		0, // gc iterator func, if 0 then no func
		0, // finalizer func, if 0 then no func
		0, // restart func, if 0 then no func
		sizeof(struct data_area_4_binary), // data area size, if 0 then size is dynamic
		PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE,
		{ 0,0 } // empty object values { data = 0, interface = 0}
	},
	{
		".internal.bitmap", // name of bitmap class
		PVM_ROOT_OBJECT_BITMAP_CLASS, // internal number of bitmap class
		syscall_table_4_bitmap, &n_syscall_table_4_bitmap, // table of syscalls funcs implementations 
		pvm_internal_init_bitmap, // constructor func, if 0 then no func
		pvm_gc_iter_call_bitmap, // gc iterator func, if 0 then no func
		0, // finalizer func, if 0 then no func
		0, // restart func, if 0 then no func
		sizeof(struct data_area_4_bitmap), // data area size, if 0 then size is dynamic
		PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL,
		{ 0,0 } // empty object values { data = 0, interface = 0}
	},
	{
		".internal.world", // name of world class
		PVM_ROOT_OBJECT_WORLD_CLASS, // internal number of world class
		syscall_table_4_world, &n_syscall_table_4_world, // table of syscalls funcs implementations 
		pvm_internal_init_world, // constructor func, if 0 then no func
		0, // gc iterator func, if 0 then no func
		0, // finalizer func, if 0 then no func
		0, // restart func, if 0 then no func
		sizeof(struct data_area_4_world), // data area size, if 0 then size is dynamic
		PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE,
		{ 0,0 } // empty object values { data = 0, interface = 0}
	},
	{
		".internal.closure", // name of closure class
		PVM_ROOT_OBJECT_CLOSURE_CLASS, // internal number of closure class
		syscall_table_4_closure, &n_syscall_table_4_closure, // table of syscalls funcs implementations 
		pvm_internal_init_closure, // constructor func, if 0 then no func
		pvm_gc_iter_call_closure, // gc iterator func, if 0 then no func
		0, // finalizer func, if 0 then no func
		0, // restart func, if 0 then no func
		sizeof(struct data_area_4_closure), // data area size, if 0 then size is dynamic
		PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL,
		{ 0,0 } // empty object values { data = 0, interface = 0}
	},
	{
		".internal.window", // name of window class
		PVM_ROOT_OBJECT_WINDOW_CLASS, // internal number of window class
		syscall_table_4_window, &n_syscall_table_4_window, // table of syscalls funcs implementations 
		pvm_internal_init_window, // constructor func, if 0 then no func
		pvm_gc_iter_call_window, // gc iterator func, if 0 then no func
		pvm_gc_finalizer_window, // finalizer func, if 0 then no func
		pvm_restart_window, // restart func, if 0 then no func
		sizeof(struct data_area_4_window), // data area size, if 0 then size is dynamic
		PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL,
		{ 0,0 } // empty object values { data = 0, interface = 0}
	},
	{
		".internal.directory", // name of directory class
		PVM_ROOT_OBJECT_DIRECTORY_CLASS, // internal number of directory class
		syscall_table_4_directory, &n_syscall_table_4_directory, // table of syscalls funcs implementations 
		pvm_internal_init_directory, // constructor func, if 0 then no func
		pvm_gc_iter_call_directory, // gc iterator func, if 0 then no func
		pvm_gc_finalizer_directory, // finalizer func, if 0 then no func
		pvm_restart_directory, // restart func, if 0 then no func
		sizeof(struct data_area_4_directory), // data area size, if 0 then size is dynamic
		// TODO add DIR flag? |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL,
		{ 0,0 } // empty object values { data = 0, interface = 0}
	},
	{
		".internal.connection", // name of connection class
		PVM_ROOT_OBJECT_CONNECTION_CLASS, // internal number of connection class
		syscall_table_4_connection, &n_syscall_table_4_connection, // table of syscalls funcs implementations 
		pvm_internal_init_connection, // constructor func, if 0 then no func
		pvm_gc_iter_call_connection, // gc iterator func, if 0 then no func
		pvm_gc_finalizer_connection, // finalizer func, if 0 then no func
		pvm_restart_connection, // restart func, if 0 then no func
		sizeof(struct data_area_4_connection), // data area size, if 0 then size is dynamic
		PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_FINALIZER,
		{ 0,0 } // empty object values { data = 0, interface = 0}
	},
	{
		".internal.sema", // name of sema class
		PVM_ROOT_OBJECT_SEMA_CLASS, // internal number of sema class
		syscall_table_4_sema, &n_syscall_table_4_sema, // table of syscalls funcs implementations 
		pvm_internal_init_sema, // constructor func, if 0 then no func
		0, // gc iterator func, if 0 then no func
		0, // finalizer func, if 0 then no func
		0, // restart func, if 0 then no func
		sizeof(struct data_area_4_sema), // data area size, if 0 then size is dynamic
		PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL |
		PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE,
		{ 0,0 } // empty object values { data = 0, interface = 0}
	}
};

int pvm_n_internal_classes = sizeof(pvm_internal_classes) / sizeof(struct internal_class);

int     pvm_iclass_by_root_index(int index)
{
	int i;
	for (i = 0; i < pvm_n_internal_classes; i++)
	{
		if (pvm_internal_classes[i].root_index == index)
			return i;
	}
	//panic("no iclass with root index = %d", index);
	/*NORETURN*/
}

int     pvm_iclass_by_class(struct pvm_object_storage *cs)
{
	int i;
	for (i = 0; i < pvm_n_internal_classes; i++)
	{
		if (pvm_internal_classes[i].class_object.data == cs)
			return i;
	}
	//panic("no iclass for 0x%x", cs);
	/*NORETURN*/
}

int EQ_STRING_P2C(const struct pvm_object o1, const char *v2)
{
	return (((unsigned)((struct data_area_4_string *)&(o1.data->da))->length) == strlen(v2))
		&& (0 == strncmp((const char *)(((struct data_area_4_string *)&(o1.data->da))->data), v2, (int)(((struct data_area_4_string *)&(o1.data->da))->length)));
}

struct pvm_object pvm_lookup_internal_class(struct pvm_object name)
{
	//if (EQ_STRING_P2C(name, ".internal.object"))
	//if ((((unsigned)pvm_get_str_len(name)) == strlen((const char *)".internal.object"))
	//	&& (0 == strncmp((const char *)pvm_get_str_data(name), (const char *)".internal.object", pvm_get_str_len(name))))
	if ((((unsigned)((struct data_area_4_string *)&(name.data->da))->length) == strlen((const char *)".internal.object"))
		&& (0 == strncmp((const char *)(((struct data_area_4_string *)&(name.data->da))->data), (const char *)".internal.object", (int)(((struct data_area_4_string *)&(name.data->da))->length))))
		
		return pvm_internal_classes[PVM_ROOT_OBJECT_NULL_CLASS].class_object;

	int i;
	for (i = 0; i < pvm_n_internal_classes; i++)
	{
		if (EQ_STRING_P2C(name, pvm_internal_classes[i].name))
			return pvm_internal_classes[i].class_object;
	}

	struct pvm_object retNull = { 0, 0 };
	//retNull.data = 0;
	return retNull;
}
