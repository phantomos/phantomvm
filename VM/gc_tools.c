#include "gc_tools.h"
#include "object.h"
#include "object_flags.h"
// for da_po_ptr
#include "object_tools.h"
// for INT_MAX
#include <limits.h>

//bakulev
#define PVM_OBJECT_AH_ALLOCATOR_FLAG_IN_BUFFER 0x08
#define PVM_OBJECT_AH_ALLOCATOR_FLAG_FREE 0x00
#define PVM_OBJECT_AH_ALLOCATOR_FLAG_ALLOCATED 0x01
#define PVM_OBJECT_AH_ALLOCATOR_FLAG_WENT_DOWN 0x04

#define	assert(e)	((e) ? (void)0 : printf( __FILE__ ":%u, %s: assertion '" #e "' failed" , __LINE__, __func__ ))

// -----------------------------------------------------------------------
// Refcount processor.
// Takes object which is found to be nonreferenced (has PVM_OBJECT_AH_ALLOCATOR_FLAG_REFZERO flag)
// and processes all its children. Those with only one ref will become marked with
// PVM_OBJECT_AH_ALLOCATOR_FLAG_REFZERO flag too.
// -----------------------------------------------------------------------

void refzero_process_children(pvm_object_storage_t *p)
{
	assert(p->_ah.alloc_flags != PVM_OBJECT_AH_ALLOCATOR_FLAG_FREE);
	assert(p->_ah.refCount == 0);

	do_refzero_process_children(p);

	//bakulev if (p->_ah.alloc_flags & PVM_OBJECT_AH_ALLOCATOR_FLAG_IN_BUFFER)
	//bakulev 	cycle_root_buffer_rm_candidate(p);

	p->_ah.alloc_flags = PVM_OBJECT_AH_ALLOCATOR_FLAG_FREE;

	debug_catch_object("del", p);
	//bakulev DEBUG_PRINT("x");
}

static void do_refzero_process_children(pvm_object_storage_t *p)
{
	// Fast skip if no children - done!
	//if( o->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE )
	//    return;

	// plain non internal objects -
	if (!(p->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL))
	{
		unsigned i;

		for (i = 0; i < da_po_limit(p); i++)
		{
			ref_dec_o(da_po_ptr(p->da)[i]);
		}
		//don't touch classes yet
		//ref_dec_o( p->_class );  // Why? a kind of mismatch in the compiler, in opcode_os_save8

		return;
	}

	// We're here if object is internal.


	// TEMP - skip classes and interfaces too. we must not reach them, in fact... how do we?
	if (
		(p->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_CLASS) ||
		(p->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERFACE) ||
		(p->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_CODE)
		)
		//panic("");
		return;


	// Now find and call class-specific function: pvm_gc_iter_*

	//bakulev gc_iterator_func_t  func = pvm_internal_classes[pvm_object_da(p->_class, class)->sys_table_id].iter;
	//bakulev func(refzero_add_from_internal, p, 0);


	//don't touch classes yet
	//ref_dec_o( p->_class );  // Why? Internal class objects are saturated for now.
}


static void refzero_add_from_internal(pvm_object_t o, void *arg)
{
	(void)arg;
	if (o.data == 0) // Don't try to process null objects
		return;

	ref_dec_o(o);
}




/*-----------------------------------------------------------------------------------------*/

#define RECURSE_REF_DEC 1

// used by   ref_dec_p()
static void ref_dec_proccess_zero(pvm_object_storage_t *p)
{
	assert(p->_ah.alloc_flags != PVM_OBJECT_AH_ALLOCATOR_FLAG_FREE);
	assert(p->_ah.refCount == 0);
	assert(!(p->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE));

#if RECURSE_REF_DEC
	// FIXME must not be so in final OS, stack overflow possiblity!
	// TODO use some local pool too, instead of recursion
	refzero_process_children(p);
#else
	// postpone for delayed inspection (bug or feature?)
	DEBUG_PRINT("(X)");
	p->_ah.alloc_flags |= PVM_OBJECT_AH_ALLOCATOR_FLAG_REFZERO; //beware of  PVM_OBJECT_AH_ALLOCATOR_FLAG_IN_BUFFER
	p->_ah.alloc_flags &= ~PVM_OBJECT_AH_ALLOCATOR_FLAG_ALLOCATED; //beware of  PVM_OBJECT_AH_ALLOCATOR_FLAG_IN_BUFFER
	pvm_collapse_free(p);
#endif
}



void debug_catch_object(const char *msg, pvm_object_storage_t *p)
{
#if 0
	// Can be used to trace some specific object's access
	//if( p != (void *)0x7acbe56c )
	//if( p != (void *)0x7acbd0e8 )
	//if( 0 != strncmp(msg, "gc", 2) || !debug_memory_leaks )
	//if( !(p->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERFACE) )
	return;
	printf("touch %s %p, refcnt = %d, size = %d da_size = %d ", msg, p, p->_ah.refCount, p->_ah.exact_size, p->_da_size);

	print_object_flags(p);
	//dumpo(p);
	//getchar();
	printf("\n"); // for GDB to break here
#endif
}

//static inline
void do_ref_dec_p(pvm_object_storage_t *p)
{
	if (p == 0) return;
	debug_catch_object("--", p);

	assert(p->_ah.object_start_marker == PVM_OBJECT_START_MARKER);
	assert(p->_ah.alloc_flags & PVM_OBJECT_AH_ALLOCATOR_FLAG_ALLOCATED);
	assert(p->_ah.refCount > 0);

	//if(p->_ah.refCount <= 0) {
	/*if( !(p->_ah.alloc_flags & PVM_OBJECT_AH_ALLOCATOR_FLAG_ALLOCATED) ) {
	//DEBUG_PRINT("Y");
	printf(" %d", p->_ah.refCount );
	printf(" @ 0x%X", p); getchar();
	}*/

	if (p->_ah.refCount < INT_MAX) // Do we really need this check? Sure, we see many decrements for saturated objects!
	{
		if (0 == (--(p->_ah.refCount)))
		{
			if (p->_flags & PHANTOM_OBJECT_STORAGE_FLAG_HAS_WEAKREF)
			{
				// This one has weak reference(s) pointing to it

				// If clearing weakrefs had problems, just bail out,
				// leave it all to long big smart GC
				//if( gc_clear_weakrefs(p) )                    goto nokill;
				gc_clear_weakrefs(p);

				if (0 != p->_ah.refCount)
					goto nonzero;
			}


			// Fast way if no children
			if (p->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE)
			{
				if (p->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_FINALIZER)
				{
					// based on the assumption that finalizer is only valid for some internal childfree objects - is it correct?
					//bakulev gc_finalizer_func_t  func = pvm_internal_classes[pvm_object_da(p->_class, class)->sys_table_id].finalizer;
					//bakulev if (func != 0) func(p);
				}

				p->_ah.alloc_flags = PVM_OBJECT_AH_ALLOCATOR_FLAG_FREE;
				debug_catch_object("del", p);
				//bakulev DEBUG_PRINT("-");
				pvm_collapse_free(p);
			}
			else
				ref_dec_proccess_zero(p);
			//bakulev STAT_INC_CNT(OBJECT_FREE);
		}
		// if we decrement refcount and stil above zero - mark an object as potential cycle root;
		// and internal objects can't be a cycle root (sic!)
		else
		{
		nonzero:;
			if (!(p->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL))
			{
				if (!(p->_ah.alloc_flags & PVM_OBJECT_AH_ALLOCATOR_FLAG_IN_BUFFER))
				{
					//bakulev cycle_root_buffer_add_candidate(p);
					p->_ah.alloc_flags |= PVM_OBJECT_AH_ALLOCATOR_FLAG_IN_BUFFER;
				}
				p->_ah.alloc_flags |= PVM_OBJECT_AH_ALLOCATOR_FLAG_WENT_DOWN;  // set down flag
			}
		}
		//nokill:;
	}
}

void ref_dec_p(pvm_object_storage_t *p)
{
#if VM_DEFERRED_REFDEC
	deferred_refdec(p);
#else
	do_ref_dec_p(p);
#endif
}

//static inline
void ref_inc_p(pvm_object_storage_t *p)
{
	if (p == 0) return;
	debug_catch_object("++", p);

	assert(p->_ah.object_start_marker == PVM_OBJECT_START_MARKER);
	assert(p->_ah.alloc_flags & PVM_OBJECT_AH_ALLOCATOR_FLAG_ALLOCATED);
	assert(p->_ah.refCount > 0);

	//if( p->_ah.refCount <= 0 )
	//{
	//    panic("p->_ah.refCount <= 0: 0x%X", p);
	//}

	if (p->_ah.refCount < INT_MAX)
	{
		(p->_ah.refCount)++;

		if (p->_ah.alloc_flags & PVM_OBJECT_AH_ALLOCATOR_FLAG_IN_BUFFER)
			p->_ah.alloc_flags &= ~PVM_OBJECT_AH_ALLOCATOR_FLAG_WENT_DOWN;  //clear down flag
	}
}

// Make sure this object won't be deleted with refcount dec
// used on sys global objects
void ref_saturate_p(pvm_object_storage_t *p)
{
	debug_catch_object("!!", p);

	//bakulev STAT_INC_CNT(OBJECT_SATURATE);

	// Saturated object can't be a loop collection candidate. Can it?
	/* //bakulev if (p->_ah.alloc_flags & PVM_OBJECT_AH_ALLOCATOR_FLAG_IN_BUFFER) {
		cycle_root_buffer_rm_candidate(p);

		p->_ah.alloc_flags &= ~PVM_OBJECT_AH_ALLOCATOR_FLAG_IN_BUFFER;
		p->_ah.alloc_flags &= ~PVM_OBJECT_AH_ALLOCATOR_FLAG_WENT_DOWN;
	}*/

	assert(p->_ah.object_start_marker == PVM_OBJECT_START_MARKER);
	assert(p->_ah.alloc_flags == PVM_OBJECT_AH_ALLOCATOR_FLAG_ALLOCATED);
	assert(p->_ah.refCount > 0);

	p->_ah.refCount = INT_MAX;
}

//external calls:
void ref_saturate_o(pvm_object_t o)
{
	if (!(o.data)) return;
	ref_saturate_p(o.data);
}

void ref_dec_o(pvm_object_t o)
{
	// Interface is never refcounted! (But why?)
	ref_dec_p(o.data);
}

pvm_object_t  ref_inc_o(pvm_object_t o)
{
	ref_inc_p(o.data);
	return o;
}

static void gc_clear_weakrefs(pvm_object_storage_t *p)
{
#if COMPILE_WEAKREF
	// This impl assumes object _satellites field is used for
	// holding weakref backpointer only


	struct pvm_object w = p->_satellites;

	si_weakref_9_resetMyObject(w);
#endif
}


