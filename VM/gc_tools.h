#pragma once

#include "object.h"

void refzero_process_children(pvm_object_storage_t *p);

static void do_refzero_process_children(pvm_object_storage_t *p);

static void refzero_add_from_internal(pvm_object_t o, void *arg);

static void ref_dec_proccess_zero(pvm_object_storage_t *p);

void debug_catch_object(const char *msg, pvm_object_storage_t *p);

void do_ref_dec_p(pvm_object_storage_t *p);

void ref_dec_p(pvm_object_storage_t *p);

void ref_inc_p(pvm_object_storage_t *p);

// Make sure this object won't be deleted with refcount dec
// used on sys global objects
void ref_saturate_p(pvm_object_storage_t *p);

void ref_saturate_o(pvm_object_t o);

void ref_dec_o(pvm_object_t o);

pvm_object_t  ref_inc_o(pvm_object_t o);

static void gc_clear_weakrefs(pvm_object_storage_t *p);


