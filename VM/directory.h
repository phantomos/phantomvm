#pragma once

#include "object.h"
// for uint32_t
#include <stdint.h>
// for errno_t
//#include <corecrt.h>
typedef int errno_t; // extracted from corecrt.h

/**
*
* BUG! TODO no mutex, TODO general short-term sync primitive for persistent mem
*
* General design:
*
* 3 arrays, array of strings (keys), array of obvects (values), bit array of flags - single or multiple value in dir entry.
* Hash func points to array slot.
* If we have just one entry for given key, just hold it and its key in corresponding arrays. Have 0 in multiple flag array.
* If we have more than one entry for given key, hold 2nd level arrays (for values and keys) in corresponding arrays. Have 1 in multiple flag array.
* In 2nd level arrays key and value positions are correspond too. Null in key slot menas that slot is unused.
*
**/

typedef struct hashdir {
uint32_t                           capacity;       // size of 1nd level arrays
uint32_t                           nEntries;       // number of actual entries stored

struct pvm_object   		keys;      	// Where we actually hold keys
struct pvm_object   		values;      	// Where we actually hold values
uint8_t 				*flags;      	// Is this keys/values slot pointing to 2nd level array

//bakulev hal_spinlock_t                      lock;

} hashdir_t;

static int hdir_cmp_keys(const char *ikey, size_t ikey_len, pvm_object_t okey);

// TODO add parameter for find and remove mode
errno_t hdir_find(hashdir_t *dir, const char *ikey, size_t i_key_len, pvm_object_t *out, int delete_found);

//! Return EEXIST if dup
errno_t hdir_add(hashdir_t *dir, const char *ikey, size_t i_key_len, pvm_object_t add);

static int hdir_cmp_keys(const char *ikey, size_t ikey_len, pvm_object_t okey);

//! Return EEXIST if dup
static errno_t hdir_init(hashdir_t *dir, size_t initial_size);

//! General hash function - if second arg iz zero, accepts
//! zero terminated string in first arg.
unsigned int calc_hash(const char *datum, const char *datum_end);

void pvm_internal_init_directory(struct pvm_object_storage * os);
