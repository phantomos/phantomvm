#pragma once

#include "object.h"

int pvm_load_class_from_module(const char *class_name, struct pvm_object   *out);

int load_class_from_file(const char *cn, struct pvm_object *out);

static int do_load_class_from_file(const char *fn, struct pvm_object *out);

static void pvm_load_type(struct pvm_code_handler *h, struct type_loader_handler *th);

static void pvm_dump_type(struct type_loader_handler *th);

static void pvm_load_method(struct method_loader_handler *mh, const unsigned char *data, int in_size);

int pvm_load_class_from_memory(const void *data, int fsize, struct pvm_object *out);
