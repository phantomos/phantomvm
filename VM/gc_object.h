﻿#pragma once

// for general object definitions
#include "object.h"

typedef void (*gc_iterator_call_t)( struct pvm_object o, void *arg );
typedef void (*gc_iterator_func_t)( gc_iterator_call_t func, struct pvm_object_storage * os, void *arg );
typedef void (*gc_finalizer_func_t)( struct pvm_object_storage * os );

// -------- Init and restart function (methods) for NULL class. --------


// -------- Init and restart function (methods) for CLASS class. --------

void pvm_gc_iter_class(gc_iterator_call_t func, struct pvm_object_storage * os, void *arg);

// -------- Init and restart function (methods) for INTERFACE class. --------

void pvm_gc_iter_interface(gc_iterator_call_t func, struct pvm_object_storage * os, void *arg);

// -------- Init and restart function (methods) for CODE class. --------


// -------- Init and restart function (methods) for INT class. --------


// -------- Init and restart function (methods) for STRING class. --------


// -------- Init and restart function (methods) for ARRAY class. --------

void pvm_gc_iter_array(gc_iterator_call_t func, struct pvm_object_storage * os, void *arg);

// -------- Init and restart function (methods) for PAGE class. --------

void pvm_gc_iter_page(gc_iterator_call_t func, struct pvm_object_storage * os, void *arg);

// -------- Init and restart function (methods) for THREAD class. --------

void pvm_gc_iter_thread(gc_iterator_call_t func, struct pvm_object_storage * os, void *arg);

// -------- Init and restart function (methods) for CALL_FRAME class. --------

void pvm_gc_iter_call_frame(gc_iterator_call_t func, struct pvm_object_storage * os, void *arg);

// -------- Init and restart function (methods) for ISTACK class. --------

void pvm_gc_iter_call_istack(gc_iterator_call_t func, struct pvm_object_storage * os, void *arg);

// -------- Init and restart function (methods) for OSTACK class. --------

void pvm_gc_iter_call_ostack(gc_iterator_call_t func, struct pvm_object_storage * os, void *arg);

// -------- Init and restart function (methods) for ESTACK class. --------

void pvm_gc_iter_call_estack(gc_iterator_call_t func, struct pvm_object_storage * os, void *arg);

// -------- Init and restart function (methods) for LONG class. --------


// -------- Init and restart function (methods) for FLOAT class. --------


// -------- Init and restart function (methods) for DOUBLE class. --------


// -------- Init and restart function (methods) for BOOT class. --------


// -------- Init and restart function (methods) for TTY class. --------

void pvm_gc_finalizer_tty(struct pvm_object_storage * os);

// -------- Init and restart function (methods) for MUTEX class. --------


// -------- Init and restart function (methods) for COND class. --------


// -------- Init and restart function (methods) for BINARY class. --------


// -------- Init and restart function (methods) for BITMAP class. --------

void pvm_gc_iter_call_bitmap(gc_iterator_call_t func, struct pvm_object_storage * os, void *arg);

// -------- Init and restart function (methods) for WORLD class. --------


// -------- Init and restart function (methods) for CLOSURE class. --------

void pvm_gc_iter_call_closure(gc_iterator_call_t func, struct pvm_object_storage * os, void *arg);

// -------- Init and restart function (methods) for WINDOW class. --------

void pvm_gc_iter_call_window(gc_iterator_call_t func, struct pvm_object_storage * os, void *arg);
void pvm_gc_finalizer_window(struct pvm_object_storage * os);

// -------- Init and restart function (methods) for DIRECTORY class. --------

void pvm_gc_iter_call_directory(gc_iterator_call_t func, struct pvm_object_storage * os, void *arg);
void pvm_gc_finalizer_directory(struct pvm_object_storage * os);

// -------- Init and restart function (methods) for CONNECTION class. --------

void pvm_gc_iter_call_connection(gc_iterator_call_t func, struct pvm_object_storage * os, void *arg);
void pvm_gc_finalizer_connection(struct pvm_object_storage * os);

// -------- Init and restart function (methods) for SEMA class. --------


