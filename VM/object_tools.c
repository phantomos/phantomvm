#include "object_tools.h"

// for PHANTOM_OBJECT_STORAGE_FLAG_IS_STRING
#include "object_flags.h"
// for pvm_get_thread_class()
#include "root.h"
// for pvm_create_string_object()
#include "object_create.h"
// for pvm_exec_run_method()
#include "exec.h"
// for printf
#include <stdio.h>

#define panic(value) //bakulev

/**
*
* Debug.
*
**/

void pvm_puts(struct pvm_object o)
{
	if (o.data->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_STRING)
	{
		struct data_area_4_string *da = (struct data_area_4_string *)&(o.data->da);
		int len = da->length;
		unsigned const char *sp = da->data;
		/* TODO BUG! From unicode! */
		while (len--)
		{
			putchar(*sp++);
		}
	}
	else
		printf("?");
}

void pvm_object_print(struct pvm_object o)
{
	if (o.data->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_INT)
	{
		printf("%d", pvm_get_int(o));
	}
	else if (o.data->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_STRING)
	{
		pvm_puts(o);
	}
	else
	{
		pvm_object_t r = pvm_exec_run_method(o, 5, 0, 0); // ToString
		pvm_puts(r);
		ref_dec_o(r);
	}
}

void print_object_flags(struct pvm_object_storage *o)
{
	if (o->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_FINALIZER)       printf("FINALIZER ");
	if (o->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE)       printf("CHILDFREE ");
	if (o->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_THREAD)          printf("THREAD ");
	if (o->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_STACK_FRAME)     printf("STACK_FRAME ");
	if (o->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_CALL_FRAME)      printf("CALL_FRAME ");
	if (o->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL)        printf("INTERNAL ");
	if (o->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_RESIZEABLE)      printf("RESIZEABLE ");
	if (o->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_STRING)          printf("STRING ");
	if (o->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_INT)             printf("INT ");
	if (o->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_DECOMPOSEABLE)   printf("DECOMPOSEABLE ");
	if (o->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_CLASS)           printf("CLASS ");
	if (o->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERFACE)       printf("INTERFACE ");
	if (o->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_CODE)            printf("CODE ");
}

// For debugging - to call from GDB - arg is object storage!

void dumpo(size_t addr)
{
	struct pvm_object_storage *o = (struct pvm_object_storage *)addr;

	printf("Flags: '");
	print_object_flags(o);
	printf("'\n");
	printf("Da size: %ld\n", (long)(o->_da_size));


	if (o->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_STRING)
	{
		printf("String: '");

		struct data_area_4_string *da = (struct data_area_4_string *)&(o->da);
		int len = da->length;
		unsigned const char *sp = da->data;
		while (len--)
		{
			putchar(*sp++);
		}
		printf("'\n");
	}
	if (o->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_CLASS)
	{
		struct data_area_4_class *da = (struct data_area_4_class *)&(o->da);
		printf("Is class: '"); pvm_object_print(da->class_name); printf("'\n");
	}
	else
	{
		// Don't dump class class
		printf("Class: { "); dumpo((size_t)(o->_class.data)); printf("}\n");
		//pvm_object_print( o->_class );
	}
}

void pvm_object_dump(struct pvm_object o)
{
	dumpo((size_t)o.data);
}

struct pvm_object
	pvm_get_class_name(struct pvm_object o)
{
	struct pvm_object c = o.data->_class;
	if (c.data->_flags & PHANTOM_OBJECT_STORAGE_FLAG_IS_CLASS)
	{
		struct data_area_4_class *da = (struct data_area_4_class *)&(c.data->da);
		ref_inc_o(da->class_name);
		return da->class_name;
	}
	else
		return pvm_create_string_object("(class corrupt?)");
}


void pvm_check_is_thread(struct pvm_object new_thread)
{
	struct pvm_object_storage	* d = new_thread.data;

	if (d->_flags != (PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL | PHANTOM_OBJECT_STORAGE_FLAG_IS_THREAD))
		panic("Thread object has no INTERNAL flag");

	if (!pvm_object_class_exactly_is(new_thread, pvm_get_thread_class()))
		panic("Thread object class is not .internal.thread");
}