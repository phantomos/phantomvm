#pragma once

// for generic object definition
#include "object.h"
// for data_area_4_##type
#include "internal_da.h"

/** Extract (typed) object data area pointer from object pointer. */
#define pvm_object_da( o, type ) ((struct data_area_4_##type *)&(o.data->da))
/** Extract (typed) object data area pointer from object pointer. */
#define pvm_data_area( o, type ) ((struct data_area_4_##type *)&(o.data->da))

/** Num of slots in normal (noninternal) object. */
#define da_po_limit(o)	 (((o)->_da_size)/sizeof(struct pvm_object))
/** Slots access for noninternal object. */
#define da_po_ptr(da)  ((struct pvm_object *)&(da))

pvm_object_t pvm_storage_to_object(pvm_object_storage_t *st);

/*bakulev
static inline pvm_object_t pvm_da_to_object(void *da)
{
	const int off = __offsetof(pvm_object_storage_t, da);
	pvm_object_storage_t *st = da - off;

	return pvm_storage_to_object(st);
}
*/

void pvm_fill_syscall_interface(struct pvm_object iface, int syscall_count);

// TODO: make sure it is not an lvalue for security reasons!
#define pvm_get_int( o )  ( (int) (((struct data_area_4_int *)&(o.data->da))->value))

// TODO: make sure it is not an lvalue for security reasons!
#define pvm_get_long( o )  ( (int64_t) (((struct data_area_4_long *)&(o.data->da))->value))

// TODO: make sure it is not an lvalue for security reasons!
#define pvm_get_float( o )  ( (float) (((struct data_area_4_float *)&(o.data->da))->value))

// TODO: make sure it is not an lvalue for security reasons!
#define pvm_get_double( o )  ( (double) (((struct data_area_4_double *)&(o.data->da))->value))

#define pvm_get_str_len( o )  ( (int) (((struct data_area_4_string *)&(o.data->da))->length))
#define pvm_get_str_data( o )  ( (char *) (((struct data_area_4_string *)&(o.data->da))->data))

int pvm_strcmp(pvm_object_t s1, pvm_object_t s2);

/**
*
* Debug.
*
**/

void pvm_puts(struct pvm_object o);

void pvm_object_print(struct pvm_object o);

void print_object_flags(struct pvm_object_storage *o);

void dumpo(size_t addr);

void pvm_object_dump(struct pvm_object o);

struct pvm_object
	pvm_get_class_name(struct pvm_object o);

void pvm_check_is_thread(struct pvm_object new_thread);