﻿#pragma once

#include "object.h"
// for int64_t
#include <stdint.h>

#define PVM_INTEGER_STACK_SIZE 64

#define PVM_OBJECT_STACK_SIZE 64

#define PVM_EXCEPTION_STACK_SIZE 64

struct pvm_stack_da_common
{
	/**
	*
	* Root (first created) page of the stack.
	* rootda is shortcut pointer to root's data area (struct data_area_4_XXX_stack).
	*
	*/
	struct pvm_object           	root;

	/**
	*
	* Current (last created) page of the stack.
	* This field is used in root stack page only.
	*
	* See curr_da below - it is a shortcut to curr object's data area.
	*
	* See also '#define set_me(to)' in stacks.c.
	*
	*/
	struct pvm_object           	curr;

	/** Pointer to previous (older) stack page. */
	struct pvm_object  			prev;

	/** Pointer to next (newer) stack page. */
	struct pvm_object  			next;

	/** number of cells used. */
	unsigned int    			free_cell_ptr;

	/** Page array has this many elements. */
	unsigned int                        __sSize;
};


struct data_area_4_class
{
		unsigned int	object_flags;			// object of this class will have such flags
		unsigned int	object_data_area_size;	// object of this class will have data area of this size
		struct pvm_object	object_default_interface; // default one
		
		unsigned int    sys_table_id; // See above - index into the kernel's syscall tables table
		
		struct pvm_object	class_name;
		struct pvm_object	class_parent;
		
		struct pvm_object	static_vars; // array of static variables
		
		struct pvm_object	ip2line_maps; // array of maps: ip->line number
		struct pvm_object	method_names; // array of method names
		struct pvm_object	field_names; // array of field names
		
		struct pvm_object	const_pool; // array of object constants
};

struct data_area_4_code
{
		// Dynamic!!! size in internal.c originally is 0
		unsigned int		code_size; // bytes
		unsigned char		code[];
};

struct data_area_4_int
{
		int	value;
};

struct data_area_4_string
{
		int	length; // bytes! (chars are unicode?)
		unsigned char	data[];
};

struct data_area_4_array
{
		struct pvm_object	page;
		int	page_size; // current page size
		int	used_slots; // how many slots are used now
};


struct pvm_code_handler
{
	const unsigned char *   	code;
	unsigned int            	IP;   /* Instruction Pointer */
	unsigned int            	IP_max;
};

struct data_area_4_thread
{
		struct pvm_code_handler	code;	// Loaded by load_fast_acc from frame
		
		//unsigned long	thread_id;	// Too hard to implement and nobody needs
		struct pvm_object	call_frame;	// current
		
		// some owner pointer?
		struct pvm_object	owner;
		struct pvm_object	environment;
		
		//bakulev hal_spinlock_t	spin;	// used on manipulations with sleep_flag
		
		#if OLD_VM_SLEEP
		volatile int	sleep_flag;	// Is true if thread is put asleep in userland
		#endif
		//bakulev timedcall_t	timer;	// Who will wake us
		//net_timer_event	timer;	// Who will wake us
		pvm_object_t	sleep_chain;	// More threads sleeping on the same event, meaningless for running thread
		//bakulev VM_SPIN_TYPE *	spin_to_unlock;	// This spin will be unlocked after putting thread asleep
		
		int	tid;	// Actual kernel thread id - reloaded on each kernel restart
		
		// fast access copies
		struct pvm_object	_this_object;	// Loaded by load_fast_acc from call_frame
		
		struct data_area_4_integer_stack *	_istack;	// Loaded by load_fast_acc from call_frame
		struct data_area_4_object_stack *	_ostack;	// Loaded by load_fast_acc from call_frame
		struct data_area_4_exception_stack *	_estack;	// Loaded by load_fast_acc from call_frame
		
		// misc data
		int	stack_depth;	// number of frames
		//long	memory_size;	// memory allocated - deallocated by this thread
};

struct data_area_4_call_frame
{
		struct pvm_object	istack;	// integer (fast calc) stack
		struct pvm_object	ostack;	// main object stack
		struct pvm_object	estack;	// exception catchers
		//struct pvm_object	cs;	// code segment, ref just for gc - OK without
		unsigned int	IP_max;	// size of code in bytes
		unsigned char *	code;	// (byte)code itself
		unsigned int	IP;
		struct pvm_object	this_object;
		struct pvm_object	prev;	// where to return!
		int	ordinal;	// num of method we run
};

struct data_area_4_integer_stack
{
		struct pvm_stack_da_common	common;
		struct data_area_4_integer_stack *	curr_da;
		int	stack[PVM_INTEGER_STACK_SIZE];
};

struct data_area_4_object_stack
{
		struct pvm_stack_da_common	common;
		struct data_area_4_object_stack *	curr_da;
		struct pvm_object	stack[PVM_OBJECT_STACK_SIZE];
};

struct pvm_exception_handler
{
	struct pvm_object  		object;
	unsigned int    		jump;
};

struct data_area_4_exception_stack
{
		struct pvm_stack_da_common	common;
		struct data_area_4_exception_stack *	curr_da;
		struct pvm_exception_handler	stack[PVM_EXCEPTION_STACK_SIZE];
};

struct data_area_4_long
{
		int64_t	value;
};

struct data_area_4_float
{
		float	value;
};

struct data_area_4_double
{
		double	value;
};

struct data_area_4_boot
{
			//Empty ?
			int i;
};

struct data_area_4_tty
{
	int i; //bakulev
	/*bakulev
#if NEW_WINDOWS
	window_handle_t                     w;
	rgba_t                              pixel[PVM_MAX_TTY_PIXELS];
#else
	drv_video_window_t                  w;
	// this field extends w and works as it's last field.
	rgba_t                              pixel[PVM_MAX_TTY_PIXELS];
#endif
	int                                 font_height; // Font is hardcoded yet - BUG - but we cant have ptr to kernel from object
	int                                 font_width;
	int                                 xsize, ysize; // in chars
	int                                 x, y; // in pixels
	rgba_t                              fg, bg; // colors
		
	char                                title[PVM_MAX_TTY_TITLE+1];
	*/
};

struct data_area_4_mutex
{
	//int             poor_mans_pagefault_compatible_spinlock;
	//bakulev VM_SPIN_TYPE      poor_mans_pagefault_compatible_spinlock;
		
	struct data_area_4_thread *owner_thread;
		
	// Up to MAX_MUTEX_THREADS are stored here
	//pvm_object_t	waiting_threads[MAX_MUTEX_THREADS];
	// And the rest is here
	pvm_object_t        waiting_threads_array;
		
	int                 nwaiting;
};

struct data_area_4_cond
{
	//bakulev VM_SPIN_TYPE        poor_mans_pagefault_compatible_spinlock;
		
	struct data_area_4_thread *owner_thread;
		
	pvm_object_t        waiting_threads_array;
	int                 nwaiting;
};

struct data_area_4_binary
{
	int	data_size;	// GCC refuses to have only unsized array in a struct
	unsigned char	data[];
};

struct data_area_4_bitmap
{
	struct pvm_object	image;	// .internal.binary
	int	xsize;
	int	ysize;
};

struct data_area_4_world
{
	int	placeholder[8];	// for any case
};

struct data_area_4_closure
{
	/** Which object to call. */
	struct pvm_object   object;
	/** Which method to call. */
	int                 ordinal;
};

struct data_area_4_window
{
	int i; //bakulev
	/*bakulev
#if NEW_WINDOWS
	window_handle_t                     w;
	rgba_t                              pixel[PVM_MAX_TTY_PIXELS];
#else
	drv_video_window_t                  w;
	// this field extends w and works as it's last field. 
	rgba_t                              pixel[PVM_MAX_TTY_PIXELS];
#endif
		
	struct pvm_object   		connector;      // Used for callbacks - events
		
	int                                 x, y; // in pixels
	rgba_t                              fg, bg; // colors
		
	//struct pvm_object                   event_handler; // connection!
	char                                title[PVM_MAX_TTY_TITLE+1];
	*/
};

struct data_area_4_directory
{
/* old, unused?
	u_int32_t                           elSize;         // size of one dir entry, bytes, defaults to 256
		
	u_int32_t                           capacity;       // size of binary container in entries
	u_int32_t                           nEntries;       // number of actual entries
		
	struct pvm_object   		container;      // Where we actually hold it
*/
		
	uint32_t                           capacity;       // size of 1nd level arrays
	uint32_t                           nEntries;       // number of actual entries stored
		
	struct pvm_object   		keys;      	// Where we actually hold keys
	struct pvm_object   		values;      	// Where we actually hold values
	uint8_t 				*flags;      	// Is this keys/values slot pointing to 2nd level array
		
	//bakulev hal_spinlock_t                      lock;
};

struct data_area_4_connection
{
	struct data_area_4_thread *         owner;		// Just this one can use
	struct pvm_connection_ops *         kernel;         // Stuff in kernel that serves us
		
	pvm_object_t                        callback;
	int                                 callback_method;
	int                                 n_active_callbacks;
		
	// Persistent kernel state, p_kernel_state_object is binary
	size_t                              p_kernel_state_size;
	pvm_object_t                        p_kernel_state_object;
	void *                              p_kernel_state;
		
	pvm_object_t 			(*blocking_syscall_worker)( pvm_object_t this, struct data_area_4_thread *tc, int nmethod, pvm_object_t arg );
		
	// Volatile kernel state
	size_t                              v_kernel_state_size;
	void *                              v_kernel_state;
		
	char                                name[1024];     // Used to reconnect on restart
};

struct data_area_4_sema
{
	//bakulev VM_SPIN_TYPE        poor_mans_pagefault_compatible_spinlock;
		
	struct data_area_4_thread *owner_thread;
		
	pvm_object_t        waiting_threads_array;
	int                 nwaiting;
		
	int                 sem_value;
};

