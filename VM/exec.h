#pragma once

/*
#include <vm/root.h>
#include <vm/internal_da.h>
#include <vm/internal.h>
#include <vm/object_flags.h>
#include <vm/exception.h>
#include <vm/alloc.h>

#include <vm/exec.h>
#include <vm/code.h>
#include <vm/reflect.h>

#include <vm/stacks.h>
#include <vm/syscall.h>

#include "ids/opcode_ids.h"

#include <kernel/snap_sync.h>
#include <kernel/debug.h>

#include <exceptions.h>
*/

#include "object.h"
// for syscall_func_t
#include "syscall.h"
// for errno_t
//#include <corecrt.h>
typedef int errno_t; // extracted from corecrt.h

typedef struct dynamic_method_info
{
	pvm_object_t        new_this;
	pvm_object_t        target_class;
	pvm_object_t        method_name;

	int                 method_ordinal;
	int                 n_param;
} dynamic_method_info_t;

/**
* Lookup class. TODO reimplement! Can block!
*/
struct pvm_object pvm_exec_lookup_class_by_name(struct pvm_object name);

struct pvm_object
	pvm_exec_run_method(
		struct pvm_object this_object,
		int method,
		int n_args,
		struct pvm_object args[]
	);

struct pvm_object_storage * pvm_exec_find_method(struct pvm_object o, unsigned int method_index);

static void do_pvm_exec(pvm_object_t current_thread);

void pvm_exec(pvm_object_t current_thread);

static errno_t find_dynamic_method(dynamic_method_info_t *mi);

static syscall_func_t pvm_exec_find_syscall(struct pvm_object _class, unsigned int syscall_index);

void pvm_exec_set_cs(struct data_area_4_call_frame* cfda, struct pvm_object_storage * code);
