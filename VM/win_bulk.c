#include "win_bulk.h"
#include <stdio.h> // for fopen

int load_code(void **out_code, unsigned int *out_size, const char *fn)
{
	FILE * f = (FILE *)fopen(fn, "rb");

	if (f == NULL)
	{
		//if(debug_print) printf("Can't open %s\n", fn );
		return 1;
	}

	fseek(f, 0, SEEK_END);
	long fsize = ftell(f);
	//printf("fsize %d\n", fsize );

	unsigned char *code = (unsigned char *)malloc(fsize);
	if (code == NULL)
	{
		printf("Can't alloc mem\n");
		return 1;
	}

	fseek(f, 0, SEEK_SET);
	int ret = fread(code, 1, fsize, f);
	if (fsize != ret)
	{
		printf("Can't read code: ret = %d\n", ret);
		free(code);
		return 1;
	}

	fclose(f);

	*out_size = (unsigned)fsize;
	*out_code = code;

	return 0;
}

/*static int do_load_class_from_file(const char *fn, struct pvm_object *out)
{
	void *code;
	unsigned int size;
	int rc = load_code(&code, &size, fn);

	if (rc)
		return rc;

	//struct pvm_object out;
	rc = pvm_load_class_from_memory(code, size, out);

	free(code);
	return rc;
}*/