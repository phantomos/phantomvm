INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (0, N'nulll     ', N'.internal.void', 1, N'Default superclass for any class.', N'PVM_ROOT_OBJECT_NULL_CLASS', N'syscall_table_4_void', N'pvm_internal_init_void', NULL, NULL, NULL, NULL, NULL, N'PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE
PHANTOM_OBJECT_STORAGE_FLAG_IS_IMMUTABLE
PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (1, N'class     ', N'.internal.class', 1, N'Class of a class (and of itself).
Pointer to class class object (used to create classes)', N'PVM_ROOT_OBJECT_CLASS_CLASS', N'syscall_table_4_class', N'pvm_internal_init_class', N'pvm_gc_iter_class', NULL, NULL, N'struct data_area_4_class', N'unsigned int	object_flags;			// object of this class will have such flags
unsigned int	object_data_area_size;	// object of this class will have data area of this size
struct pvm_object	object_default_interface; // default one

unsigned int    sys_table_id; // See above - index into the kernel''s syscall tables table

struct pvm_object	class_name;
struct pvm_object	class_parent;

struct pvm_object	static_vars; // array of static variables

struct pvm_object	ip2line_maps; // array of maps: ip->line number
struct pvm_object	method_names; // array of method names
struct pvm_object	field_names; // array of field names

struct pvm_object	const_pool; // array of object constants', N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL
PHANTOM_OBJECT_STORAGE_FLAG_IS_CLASS')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (2, N'interface ', N'.internal.interface', 1, N'// interface is a quite usual phantom object.
// (just has a specific flag - for performance only!)
// we just need an internal interface creation code
// to be able to start Phantom from scratch', N'PVM_ROOT_OBJECT_INTERFACE_CLASS', N'syscall_table_4_interface', N'pvm_internal_init_interface', N'pvm_gc_iter_interface', NULL, NULL, NULL, NULL, N'// no internal flag! TODO Immutable?
PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERFACE')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (3, N'code      ', N'.internal.code', 1, N'Pointer to code class', N'PVM_ROOT_OBJECT_CODE_CLASS', N'syscall_table_4_code', N'pvm_internal_init_code', NULL, NULL, NULL, NULL, NULL, N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL
PHANTOM_OBJECT_STORAGE_FLAG_IS_CODE
PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (4, N'int       ', N'.internal.int', 1, N'Pointer to integer class', N'PVM_ROOT_OBJECT_INT_CLASS', N'syscall_table_4_int', N'pvm_internal_init_int', NULL, NULL, NULL, N'struct data_area_4_int', N'int	value;', N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL
PHANTOM_OBJECT_STORAGE_FLAG_IS_INT
PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE
PHANTOM_OBJECT_STORAGE_FLAG_IS_IMMUTABLE')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (5, N'string    ', N'.internal.string', 1, NULL, N'PVM_ROOT_OBJECT_STRING_CLASS', N'syscall_table_4_string', N'pvm_internal_init_string', NULL, NULL, NULL, N'struct data_area_4_string', N'int	length; // bytes! (chars are unicode?)
unsigned char	data[];', N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL
PHANTOM_OBJECT_STORAGE_FLAG_IS_STRING
PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE
PHANTOM_OBJECT_STORAGE_FLAG_IS_IMMUTABLE')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (6, N'array     ', N'.internal.container.array', 1, NULL, N'PVM_ROOT_OBJECT_ARRAY_CLASS', N'syscall_table_4_array', N'pvm_internal_init_array', N'pvm_gc_iter_array', NULL, NULL, N'struct data_area_4_array', N'struct pvm_object	page;
int	page_size; // current page size
int	used_slots; // how many slots are used now', N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL
PHANTOM_OBJECT_STORAGE_FLAG_IS_DECOMPOSEABLE
PHANTOM_OBJECT_STORAGE_FLAG_IS_RESIZEABLE')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (7, N'page      ', N'.internal.container.page', 1, NULL, N'PVM_ROOT_OBJECT_PAGE_CLASS', N'syscall_table_4_page', N'pvm_internal_init_page', N'pvm_gc_iter_page', NULL, NULL, NULL, NULL, N'0
// PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (8, N'thread    ', N'.internal.thread', 1, NULL, N'PVM_ROOT_OBJECT_THREAD_CLASS', N'syscall_table_4_thread', N'pvm_internal_init_thread', N'pvm_gc_iter_thread', NULL, NULL, N'struct data_area_4_thread', N'struct pvm_code_handler	code;	// Loaded by load_fast_acc from frame

//unsigned long	thread_id;	// Too hard to implement and nobody needs
struct pvm_object	call_frame;	// current

// some owner pointer?
struct pvm_object	owner;
struct pvm_object	environment;

hal_spinlock_t	spin;	// used on manipulations with sleep_flag

#if OLD_VM_SLEEP
volatile int	sleep_flag;	// Is true if thread is put asleep in userland
#endif
//timedcall_t	timer;	// Who will wake us
net_timer_event	timer;	// Who will wake us
pvm_object_t	sleep_chain;	// More threads sleeping on the same event, meaningless for running thread
VM_SPIN_TYPE *	spin_to_unlock;	// This spin will be unlocked after putting thread asleep

int	tid;	// Actual kernel thread id - reloaded on each kernel restart

// fast access copies
struct pvm_object	_this_object;	// Loaded by load_fast_acc from call_frame

struct data_area_4_integer_stack *	_istack;	// Loaded by load_fast_acc from call_frame
struct data_area_4_object_stack *	_ostack;	// Loaded by load_fast_acc from call_frame
struct data_area_4_exception_stack *	_estack;	// Loaded by load_fast_acc from call_frame

// misc data
int	stack_depth;	// number of frames
//long	memory_size;	// memory allocated - deallocated by this thread', N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL
PHANTOM_OBJECT_STORAGE_FLAG_IS_THREAD')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (9, N'call_frame', N'.internal.call_frame', 1, NULL, N'PVM_ROOT_OBJECT_CALL_FRAME_CLASS', N'syscall_table_4_call_frame', N'pvm_internal_init_call_frame', N'pvm_gc_iter_call_frame', NULL, NULL, N'struct data_area_4_call_frame', N'struct pvm_object	istack;	// integer (fast calc) stack
struct pvm_object	ostack;	// main object stack
struct pvm_object	estack;	// exception catchers
//struct pvm_object	cs;	// code segment, ref just for gc - OK without
unsigned int	IP_max;	// size of code in bytes
unsigned char *	code;	// (byte)code itself
unsigned int	IP;
struct pvm_object	this_object;
struct pvm_object	prev;	// where to return!
int	ordinal;	// num of method we run', N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL
PHANTOM_OBJECT_STORAGE_FLAG_IS_CALL_FRAME')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (10, N'istack    ', N'.internal.istack', 1, N'Integer stack', N'PVM_ROOT_OBJECT_ISTACK_CLASS', N'syscall_table_4_istack', N'pvm_internal_init_call_istack', N'pvm_gc_iter_call_istack', NULL, NULL, N'struct data_area_4_integer_stack', N'struct pvm_stack_da_common	common;
struct data_area_4_integer_stack *	curr_da;
int	stack[PVM_INTEGER_STACK_SIZE];', N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL
PHANTOM_OBJECT_STORAGE_FLAG_IS_STACK_FRAME')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (11, N'ostack    ', N'.internal.ostack', 1, N'Object stack', N'PVM_ROOT_OBJECT_OSTACK_CLASS', N'syscall_table_4_ostack', N'pvm_internal_init_call_ostack', N'pvm_gc_iter_call_ostack', NULL, NULL, N'struct data_area_4_object_stack', N'struct pvm_stack_da_common	common;
struct data_area_4_object_stack *	curr_da;
struct pvm_object	stack[PVM_OBJECT_STACK_SIZE];', N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL
PHANTOM_OBJECT_STORAGE_FLAG_IS_STACK_FRAME')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (12, N'estack    ', N'.internal.estack', 1, N'Exception stack', N'PVM_ROOT_OBJECT_ESTACK_CLASS', N'syscall_table_4_estack', N'pvm_internal_init_call_estack', N'pvm_gc_iter_call_estack', NULL, NULL, N'struct data_area_4_exception_stack', N'struct pvm_stack_da_common	common;
struct data_area_4_exception_stack *	curr_da;
struct pvm_exception_handler	stack[PVM_EXCEPTION_STACK_SIZE];', N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL
PHANTOM_OBJECT_STORAGE_FLAG_IS_STACK_FRAME')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (13, N'long      ', N'.internal.long', 1, N'// Pointer to long (64 bit) class', N'PVM_ROOT_OBJECT_LONG_CLASS', N'syscall_table_4_long', N'pvm_internal_init_long', NULL, NULL, NULL, N'struct data_area_4_long', N'int64_t	value;', N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL
PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE
PHANTOM_OBJECT_STORAGE_FLAG_IS_IMMUTABLE
// removed PHANTOM_OBJECT_STORAGE_FLAG_IS_INT')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (14, N'float     ', N'.internal.float', 1, N'// Pointer to float class', N'PVM_ROOT_OBJECT_FLOAT_CLASS', N'syscall_table_4_float', N'pvm_internal_init_float', NULL, NULL, NULL, N'struct data_area_4_float', N'float	value;', N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL
PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE
PHANTOM_OBJECT_STORAGE_FLAG_IS_IMMUTABLE
// removed PHANTOM_OBJECT_STORAGE_FLAG_IS_INT')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (15, N'double    ', N'.internal.double', 1, N'// Pointer to double (64 bit) class', N'PVM_ROOT_OBJECT_DOUBLE_CLASS', N'syscall_table_4_double', N'pvm_internal_init_double', NULL, NULL, NULL, N'struct data_area_4_double', N'double	value;', N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL
PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE
PHANTOM_OBJECT_STORAGE_FLAG_IS_IMMUTABLE
// removed PHANTOM_OBJECT_STORAGE_FLAG_IS_INT')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (16, N'boot      ', N'.internal.boot', 1, N'// Pointer to botstrap class - used on a new fresh system creation
// to suck in the rest of the system', N'PVM_ROOT_OBJECT_BOOT_CLASS', N'syscall_table_4_boot', N'pvm_internal_init_boot', NULL, NULL, NULL, N'struct data_area_4_boot', N'// Empty?', N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL
PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (17, N'tty       ', N'.internal.io.tty', 1, NULL, N'PVM_ROOT_OBJECT_TTY_CLASS', N'syscall_table_4_tty', N'pvm_internal_init_tty', NULL, N'pvm_gc_finalizer_tty', N'pvm_restart_tty', N'struct data_area_4_tty', N'#if NEW_WINDOWS
    window_handle_t                     w;
    rgba_t                              pixel[PVM_MAX_TTY_PIXELS];
#else
    drv_video_window_t                  w;
    /** this field extends w and works as it''s last field. */
    rgba_t                              pixel[PVM_MAX_TTY_PIXELS];
#endif
    int                                 font_height; // Font is hardcoded yet - BUG - but we cant have ptr to kernel from object
    int                                 font_width;
    int                                 xsize, ysize; // in chars
    int                                 x, y; // in pixels
    rgba_t                              fg, bg; // colors

    char                                title[PVM_MAX_TTY_TITLE+1];', N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL
PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (18, N'loader    ', N'internal.loader', 0, NULL, N'PVM_ROOT_OBJECT_CLASS_LOADER', N'syscall_table_4_loader', N'pvm_internal_init_loader', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (19, N'driver    ', N'.internal.io.driver', 0, NULL, N'PVM_ROOT_OBJECT_DRIVER_CLASS', N'syscall_table_4_driver', N'pvm_internal_init_driver', N'pvm_gc_iter_driver', NULL, NULL, N'struct data_area_4_driver', N'// Not implemented.', N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (20, N'mutex     ', N'.internal.mutex', 1, NULL, N'PVM_ROOT_OBJECT_MUTEX_CLASS', N'syscall_table_4_mutex', N'pvm_internal_init_mutex', NULL, NULL, NULL, N'struct data_area_4_mutex', N'//int                 poor_mans_pagefault_compatible_spinlock;
    VM_SPIN_TYPE      poor_mans_pagefault_compatible_spinlock;

    struct data_area_4_thread *owner_thread;

    // Up to MAX_MUTEX_THREADS are stored here
    //pvm_object_t	waiting_threads[MAX_MUTEX_THREADS];
    // And the rest is here
    pvm_object_t        waiting_threads_array;

    int                 nwaiting;', N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL
PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (21, N'cond      ', N'.internal.cond', 1, NULL, N'PVM_ROOT_OBJECT_COND_CLASS', N'syscall_table_4_cond', N'pvm_internal_init_cond', NULL, NULL, NULL, N'struct data_area_4_cond', N'VM_SPIN_TYPE        poor_mans_pagefault_compatible_spinlock;

    struct data_area_4_thread *owner_thread;

    pvm_object_t        waiting_threads_array;
    int                 nwaiting;', N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL
PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (22, N'binary    ', N'.internal.binary', 1, N'// TODO problem - dynamically sized!', N'PVM_ROOT_OBJECT_BINARY_CLASS', N'syscall_table_4_binary', N'pvm_internal_init_binary', NULL, NULL, NULL, N'struct data_area_4_binary', N'int	data_size;	// GCC refuses to have only unsized array in a struct
unsigned char	data[];', N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL
PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (23, N'bitmap    ', N'.internal.bitmap', 1, NULL, N'PVM_ROOT_OBJECT_BITMAP_CLASS', N'syscall_table_4_bitmap', N'pvm_internal_init_bitmap', N'pvm_gc_iter_call_bitmap', NULL, NULL, N'struct data_area_4_bitmap', N'struct pvm_object	image;	// .internal.binary
int	xsize;
int	ysize;', N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (24, N'world     ', N'.internal.world', 1, NULL, N'PVM_ROOT_OBJECT_WORLD_CLASS', N'syscall_table_4_world', N'pvm_internal_init_world', NULL, NULL, NULL, N'struct data_area_4_world', N'int	placeholder[8];	// for any case', N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL
PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (25, N'closure   ', N'.internal.closure', 1, NULL, N'PVM_ROOT_OBJECT_CLOSURE_CLASS', N'syscall_table_4_closure', N'pvm_internal_init_closure', N'pvm_gc_iter_call_closure', NULL, NULL, N'struct data_area_4_closure', N'/** Which object to call. */
    struct pvm_object   object;
    /** Which method to call. */
    int                 ordinal;', N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (26, N'udp       ', N'.internal.udp', 0, NULL, N'PVM_ROOT_OBJECT_UDP_CLASS', N'syscall_table_4_udp', N'pvm_internal_init_udp', NULL, N'pvm_gc_finalizer_udp', NULL, N'struct data_area_4_udp', N'// Not implemented.', N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL
PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE
PHANTOM_OBJECT_STORAGE_FLAG_IS_FINALIZER')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (27, N'tcp       ', N'.internal.tcp', 0, NULL, N'PVM_ROOT_OBJECT_TCP_CLASS', N'syscall_table_4_tcp', N'pvm_internal_init_tcp', NULL, N'pvm_gc_finalizer_tcp', NULL, N'struct data_area_4_tcp', N'// Not implemented.', N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL
PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE
PHANTOM_OBJECT_STORAGE_FLAG_IS_FINALIZER')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (28, N'weakref   ', N'.internal.weakref', 0, NULL, N'PVM_ROOT_OBJECT_WEAKREF_CLASS', N'syscall_table_4_weakref', N'pvm_internal_init_weakref', N'pvm_gc_iter_call_weakref', NULL, NULL, N'struct data_area_4_weakref', N'// Not implemented.', N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (29, N'window    ', N'.internal.window', 1, NULL, N'PVM_ROOT_OBJECT_WINDOW_CLASS', N'syscall_table_4_window', N'pvm_internal_init_window', N'pvm_gc_iter_call_window', N'pvm_gc_finalizer_window', N'pvm_restart_window', N'struct data_area_4_window', N'#if NEW_WINDOWS
    window_handle_t                     w;
    rgba_t                              pixel[PVM_MAX_TTY_PIXELS];
#else
    drv_video_window_t                  w;
    // this field extends w and works as it''s last field. 
    rgba_t                              pixel[PVM_MAX_TTY_PIXELS];
#endif

    struct pvm_object   		connector;      // Used for callbacks - events

    int                                 x, y; // in pixels
    rgba_t                              fg, bg; // colors

    //struct pvm_object                   event_handler; // connection!
    char                                title[PVM_MAX_TTY_TITLE+1];', N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (30, N'directory ', N'.internal.directory', 1, NULL, N'PVM_ROOT_OBJECT_DIRECTORY_CLASS', N'syscall_table_4_directory', N'pvm_internal_init_directory', N'pvm_gc_iter_call_directory', N'pvm_gc_finalizer_directory', N'pvm_restart_directory', N'struct data_area_4_directory', N'/* old, unused?
    u_int32_t                           elSize;         // size of one dir entry, bytes, defaults to 256

    u_int32_t                           capacity;       // size of binary container in entries
    u_int32_t                           nEntries;       // number of actual entries

    struct pvm_object   		container;      // Where we actually hold it
*/

    u_int32_t                           capacity;       // size of 1nd level arrays
    u_int32_t                           nEntries;       // number of actual entries stored

    struct pvm_object   		keys;      	// Where we actually hold keys
    struct pvm_object   		values;      	// Where we actually hold values
    u_int8_t 				*flags;      	// Is this keys/values slot pointing to 2nd level array

    hal_spinlock_t                      lock;', N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL
// TODO add DIR flag?')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (31, N'connection', N'.internal.connection', 1, NULL, N'PVM_ROOT_OBJECT_CONNECTION_CLASS', N'syscall_table_4_connection', N'pvm_internal_init_connection', N'pvm_gc_iter_call_connection', N'pvm_gc_finalizer_connection', N'pvm_restart_connection', N'struct data_area_4_connection', N'    struct data_area_4_thread *         owner;		// Just this one can use
    struct pvm_connection_ops *         kernel;         // Stuff in kernel that serves us

    pvm_object_t                        callback;
    int                                 callback_method;
    int                                 n_active_callbacks;

    // Persistent kernel state, p_kernel_state_object is binary
    size_t                              p_kernel_state_size;
    pvm_object_t                        p_kernel_state_object;
    void *                              p_kernel_state;

    pvm_object_t 			(*blocking_syscall_worker)( pvm_object_t this, struct data_area_4_thread *tc, int nmethod, pvm_object_t arg );

    // Volatile kernel state
    size_t                              v_kernel_state_size;
    void *                              v_kernel_state;

    char                                name[1024];     // Used to reconnect on restart', N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL
PHANTOM_OBJECT_STORAGE_FLAG_IS_FINALIZER')
INSERT INTO [dbo].[InternalClasses] ([Id], [Name], [NameClass], [isEnabled], [Comment], [NameDefine], [SyscalTable], [InitFunction], [GcIter], [Finalizer], [Restart], [DataAreaName], [DataAreaFields], [Flags]) VALUES (32, N'sema      ', N'.internal.sema', 1, NULL, N'PVM_ROOT_OBJECT_SEMA_CLASS', N'syscall_table_4_sema', N'pvm_internal_init_sema', NULL, NULL, NULL, N'struct data_area_4_sema', N'VM_SPIN_TYPE        poor_mans_pagefault_compatible_spinlock;

    struct data_area_4_thread *owner_thread;

    pvm_object_t        waiting_threads_array;
    int                 nwaiting;

    int                 sem_value;', N'PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL
PHANTOM_OBJECT_STORAGE_FLAG_IS_CHILDFREE')
