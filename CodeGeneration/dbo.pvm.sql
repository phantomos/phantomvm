﻿CREATE TABLE [dbo].[Table]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Name] VARCHAR(50) NULL, 
    [NameClass] NCHAR(10) NULL, 
    [isEnabled] BIT NULL, 
    [Comment] NTEXT NULL, 
    [NameDefine] NCHAR(10) NULL
)
