SET IDENTITY_INSERT [dbo].[SyscallFunctions] ON
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (1, N'invalid_syscall', N'    DEBUG_INFO;
printf("invalid syscal for object: "); dumpo( (addr_t)(o.data) );//pvm_object_print( o ); printf("\n");
//printf("invalid value''s class: "); pvm_object_print( o.data->_class); printf("\n");
    SYSCALL_THROW_STRING( "invalid syscall called" );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (2, N'si_void_0_construct', N'    (void)oo;
    //(void)tc;

    DEBUG_INFO;
    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (3, N'si_void_1_destruct', N'    (void)o;
    //(void)tc;

    DEBUG_INFO;
    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (4, N'si_void_2_class', N'    DEBUG_INFO;
    //ref_inc_o( this_obj.data->_class );  //increment if class is refcounted
    SYSCALL_RETURN(this_obj.data->_class);
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (5, N'si_void_3_clone', N'    (void)o;
    DEBUG_INFO;
    SYSCALL_THROW_STRING( "void clone called" );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (6, N'si_void_4_equals', N'    DEBUG_INFO;

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

    struct pvm_object him = POP_ARG;

    int ret = (me.data == him.data);

    SYS_FREE_O(him);

    SYSCALL_RETURN(pvm_create_int_object( ret ) );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (7, N'si_void_5_tostring', N'    (void)o;
    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_string_object( "(void)" ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (8, N'si_void_6_toXML', N'    (void)o;
    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_string_object( "<void>" ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (9, N'si_void_7_fromXML', N'    (void)o;
    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_string_object( "(void)" ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (10, N'si_void_8_def_op_1', N'    (void)o;
    DEBUG_INFO;
    SYSCALL_THROW_STRING( "void default op 1 called" );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (11, N'si_void_9_def_op_2', N'    (void)o;
    DEBUG_INFO;
    SYSCALL_THROW_STRING( "void default op 2 called" );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (12, N'si_void_15_hashcode', N'    DEBUG_INFO;
    size_t os = me.data->_da_size;
    void *oa = me.data->da;

    //SYSCALL_RETURN(pvm_create_int_object( ((addr_t)me.data)^0x3685A634^((addr_t)&si_void_15_hashcode) ));
    SYSCALL_RETURN(pvm_create_int_object( calc_hash( oa, oa+os) ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (13, N'si_int_3_clone', N'    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_int_object( pvm_get_int(me) ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (14, N'si_int_4_equals', N'    DEBUG_INFO;

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

    struct pvm_object him = POP_ARG;

    int same_class = me.data->_class.data == him.data->_class.data;
    int same_value = pvm_get_int(me) == pvm_get_int(him);

    SYS_FREE_O(him);

    SYSCALL_RETURN(pvm_create_int_object( same_class && same_value));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (15, N'si_int_5_tostring', N'    DEBUG_INFO;
    char buf[32];
    snprintf( buf, sizeof(buf), "%d", pvm_get_int(me) );
    SYSCALL_RETURN(pvm_create_string_object( buf ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (16, N'si_int_6_toXML', N'    DEBUG_INFO;
    char buf[32];
    snprintf( buf, 31, "%d", pvm_get_int(me) );
	//SYSCALL_RETURN(pvm_create_string_object( "<void>" ));
    SYSCALL_THROW_STRING( "int toXML called" );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (17, N'si_long_3_clone', N'    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_long_object( pvm_get_long(me) ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (18, N'si_long_4_equals', N'    DEBUG_INFO;

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

    struct pvm_object him = POP_ARG;

    int same_class = me.data->_class.data == him.data->_class.data;
    int same_value = pvm_get_long(me) == pvm_get_long(him);

    SYS_FREE_O(him);

    SYSCALL_RETURN(pvm_create_int_object( same_class && same_value));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (19, N'si_long_5_tostring', N'    DEBUG_INFO;
    char buf[100];
    snprintf( buf, sizeof(buf), "%Ld", pvm_get_long(me) ); // TODO right size?
    SYSCALL_RETURN(pvm_create_string_object( buf ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (20, N'si_long_6_toXML', N'    DEBUG_INFO;
    char buf[100];
    snprintf( buf, sizeof(buf), "%Ld", pvm_get_long(me) );
	//SYSCALL_RETURN(pvm_create_string_object( "<void>" ));
    SYSCALL_THROW_STRING( "int toXML called" );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (21, N'si_float_3_clone', N'    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_float_object( pvm_get_float(me) ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (22, N'si_float_4_equals', N'    DEBUG_INFO;

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

    struct pvm_object him = POP_ARG;

    int same_class = me.data->_class.data == him.data->_class.data;
    int same_value = pvm_get_float(me) == pvm_get_float(him);

    SYS_FREE_O(him);

    SYSCALL_RETURN(pvm_create_int_object( same_class && same_value));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (23, N'si_float_5_tostring', N'    DEBUG_INFO;
    char buf[100];
    snprintf( buf, sizeof(buf), "%f", pvm_get_float(me) ); // TODO right size?
    SYSCALL_RETURN(pvm_create_string_object( buf ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (24, N'si_float_6_toXML', N'    DEBUG_INFO;
    char buf[100];
    snprintf( buf, sizeof(buf), "<float>%f</float>", pvm_get_float(me) );
	SYSCALL_RETURN(pvm_create_string_object( buf ));
    //SYSCALL_THROW_STRING( "int toXML called" );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (25, N'si_double_3_clone', N'    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_double_object( pvm_get_double(me) ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (26, N'si_double_4_equals', N'    DEBUG_INFO;

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

    struct pvm_object him = POP_ARG;

    int same_class = me.data->_class.data == him.data->_class.data;
    int same_value = pvm_get_double(me) == pvm_get_double(him);

    SYS_FREE_O(him);

    SYSCALL_RETURN(pvm_create_int_object( same_class && same_value));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (27, N'si_double_5_tostring', N'    DEBUG_INFO;
    char buf[100];
    snprintf( buf, sizeof(buf), "%f", pvm_get_double(me) ); // TODO right size?
    SYSCALL_RETURN(pvm_create_string_object( buf ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (28, N'si_double_6_toXML', N'    DEBUG_INFO;
    char buf[100];
    snprintf( buf, sizeof(buf), "<double>%f</double>", pvm_get_double(me) );
	SYSCALL_RETURN(pvm_create_string_object( buf ));
    //SYSCALL_THROW_STRING( "int toXML called" );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (29, N'si_string_3_clone', N'    DEBUG_INFO;
    ASSERT_STRING(me);
    struct data_area_4_string *meda = pvm_object_da( me, string );
    SYSCALL_RETURN(pvm_create_string_object_binary( (char *)meda->data, meda->length ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (30, N'si_string_4_equals', N'    DEBUG_INFO;

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

    struct pvm_object him = POP_ARG;

    int ret = 0;
    if( !pvm_is_null(him) )
    {
        ASSERT_STRING(him);

        struct data_area_4_string *meda = pvm_object_da( me, string );
        struct data_area_4_string *himda = pvm_object_da( him, string );

        ret =
            me.data->_class.data == him.data->_class.data &&
            meda->length == himda->length &&
            0 == strncmp( (const char*)meda->data, (const char*)himda->data, meda->length )
            ;
    }
    SYS_FREE_O(him);

    // BUG - can compare just same classes
    SYSCALL_RETURN(pvm_create_int_object( ret ) );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (31, N'si_string_5_tostring', N'    DEBUG_INFO;
    SYSCALL_RETURN(me);
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (32, N'si_string_8_substring', N'    DEBUG_INFO;
    ASSERT_STRING(me);
    struct data_area_4_string *meda = pvm_object_da( me, string );

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 2);

    int parmlen = POP_INT();
    int index = POP_INT();


    if( index < 0 || index >= meda->length )
        SYSCALL_THROW_STRING( "string.substring index is out of bounds" );

    int len = meda->length - index;
    if( parmlen < len ) len = parmlen;

    if( len < 0 )
        SYSCALL_THROW_STRING( "string.substring length is negative" );


    //printf("substr inx %x len %d parmlen %d\n", index, len, parmlen);

    SYSCALL_RETURN(pvm_create_string_object_binary( (char *)meda->data + index, len ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (33, N'si_string_9_charat', N'    DEBUG_INFO;
    ASSERT_STRING(me);
    struct data_area_4_string *meda = pvm_object_da( me, string );

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

    int index = POP_INT();


    int len = meda->length;

    if(index > len-1 )
        SYSCALL_THROW_STRING( "string.charAt index is out of bounds" );

    SYSCALL_RETURN(pvm_create_int_object( meda->data[index]  ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (34, N'si_string_10_concat', N'    DEBUG_INFO;

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

    struct pvm_object him = POP_ARG;
    ASSERT_STRING(him);

    struct data_area_4_string *meda = pvm_object_da( me, string );
    struct data_area_4_string *himda = pvm_object_da( him, string );

    pvm_object_t ret = pvm_create_string_object_binary_cat(
    		(char *)meda->data, meda->length,
                (char *)himda->data, himda->length );

    SYS_FREE_O(him);

    SYSCALL_RETURN( ret );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (35, N'si_string_11_length', N'    DEBUG_INFO;
    ASSERT_STRING(me);
    struct data_area_4_string *meda = pvm_object_da( me, string );

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 0);

    SYSCALL_RETURN(pvm_create_int_object( meda->length ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (36, N'si_string_12_find', N'    DEBUG_INFO;

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

    struct pvm_object him = POP_ARG;
    ASSERT_STRING(him);

    struct data_area_4_string *meda = pvm_object_da( me, string );
    struct data_area_4_string *himda = pvm_object_da( him, string );

    unsigned char * ret = (unsigned char *)strnstrn(
    		(char *)meda->data, meda->length,
                (char *)himda->data, himda->length );

    SYS_FREE_O(him);

    int pos = -1;

    if( ret != 0 )
        pos = ret - (meda->data);

    SYSCALL_RETURN(pvm_create_int_object( pos ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (37, N'si_thread_5_tostring', N'    (void)me;
    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_string_object( "thread" ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (38, N'si_thread_8_start', N'    (void)me;
    DEBUG_INFO;
    //phantom_activate_thread(me);
    //SYSCALL_RETURN(pvm_create_string_object( "thread" ));
    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (39, N'si_thread_10_pause', N'    DEBUG_INFO;
    struct data_area_4_thread *meda = pvm_object_da( me, thread );

    if(meda != tc)
    	SYSCALL_THROW_STRING("Thread can pause itself only");

#if OLD_VM_SLEEP
    SYSCALL_PUT_THIS_THREAD_ASLEEP(0);
#else
    SYSCALL_THROW_STRING("Not this way");
#endif

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (40, N'si_thread_11_continue', N'    DEBUG_INFO;
#if OLD_VM_SLEEP
    struct data_area_4_thread *meda = pvm_object_da( me, thread );

    //hal_spin_lock(&meda->spin);
    if( !meda->sleep_flag )
    	SYSCALL_THROW_STRING("Thread is not sleeping in continue");
    //hal_spin_unlock(&meda->spin);

    SYSCALL_WAKE_THREAD_UP(meda);
#else
    SYSCALL_THROW_STRING("Not this way");
#endif

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (41, N'si_thread_14_getOsInterface', N'    (void)me;
    DEBUG_INFO;
    struct pvm_object_storage *root = get_root_object_storage();
    struct pvm_object o = pvm_get_field( root, PVM_ROOT_OBJECT_OS_ENTRY );
    SYSCALL_RETURN( ref_inc_o( o ) );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (42, N'si_thread_13_getUser', N'    (void)me;
    DEBUG_INFO;
    struct data_area_4_thread *meda = pvm_object_da( me, thread );

    SYSCALL_RETURN(meda->owner);
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (43, N'si_thread_12_getEnvironment', N'    (void)me;
    DEBUG_INFO;
    struct data_area_4_thread *meda = pvm_object_da( me, thread );

    if( pvm_is_null(meda->environment) )
    {
        struct pvm_object env = pvm_create_string_object(".phantom.environment");
        struct pvm_object cl = pvm_exec_lookup_class_by_name( env );
        meda->environment = pvm_create_object(cl);
        ref_dec_o(env);
        //ref_dec_o(cl);  // object keep class ref
    }

    SYSCALL_RETURN(meda->environment);
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (44, N'si_call_frame_5_tostring', N'    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_string_object( "call_frame" ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (45, N'si_istack_5_tostring', N'    (void)me;
    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_string_object( "istack" ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (46, N'si_ostack_5_tostring', N'    (void)me;
    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_string_object( "ostack" ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (47, N'si_estack_5_tostring', N'    (void)me;
    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_string_object( "estack" ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (48, N'si_class_class_5_tostring', N'    (void)me;
    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_string_object( "class" ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (49, N'si_class_class_8_new_class', N'    (void)me;
    DEBUG_INFO;

    int n_param = POP_ISTACK;

    CHECK_PARAM_COUNT(n_param, 3);

    struct pvm_object class_name = POP_ARG;
    int n_object_slots = POP_INT();
    struct pvm_object iface = POP_ARG;

    ASSERT_STRING(class_name);

    struct pvm_object new_class = pvm_create_class_object(class_name, iface, sizeof(struct pvm_object) * n_object_slots);

    //SYS_FREE_O(class_name);  //linked in class object
    //SYS_FREE_O(iface);  //linked in class object

    SYSCALL_RETURN( new_class );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (50, N'si_class_10_set_static', N'    struct data_area_4_class *meda = pvm_object_da( me, class );

    DEBUG_INFO;

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 2);

    struct pvm_object static_val = POP_ARG;
    int n_slot = POP_INT();

    pvm_set_ofield( meda->static_vars, n_slot, static_val );

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (51, N'si_class_11_get_static', N'    struct data_area_4_class *meda = pvm_object_da( me, class );
    DEBUG_INFO;

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 2);

    int n_slot = POP_INT();

    pvm_object_t ret = pvm_get_ofield( meda->static_vars, n_slot );
    ref_inc_o( ret );
    SYSCALL_RETURN( ret );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (52, N'si_class_14_instanceof', N'    //struct data_area_4_class *meda = pvm_object_da( me, class );
    DEBUG_INFO;

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

    struct pvm_object instance = POP_ARG;
#if VM_INSTOF_RECURSIVE
    int is = pvm_object_class_is_or_child( instance, me );
#else
    int is = pvm_object_class_exactly_is( instance, me );
#endif // VM_INSTOF_RECURSIVE
    SYS_FREE_O(instance);

    SYSCALL_RETURN(pvm_create_int_object( is ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (53, N'si_interface_5_tostring', N'    (void)me;
    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_string_object( "interface" ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (54, N'si_code_5_tostring', N'    (void)me;
    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_string_object( "code" ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (55, N'si_page_5_tostring', N'    (void)me;
    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_string_object( "page" ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (56, N'si_bootstrap_5_tostring', N'    (void)me;
    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_string_object( "bootstrap" ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (57, N'si_bootstrap_8_load_class', N'    (void)me;
    DEBUG_INFO;

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

    const int bufs = 1024;
    char buf[bufs+1];

    {
    struct pvm_object name = POP_ARG;
    ASSERT_STRING(name);

    struct data_area_4_string *nameda = pvm_object_da( name, string );


    int len = nameda->length > bufs ? bufs : nameda->length;
    memcpy( buf, nameda->data, len );
    buf[len] = ''\0'';

    SYS_FREE_O(name);
    }

    // BUG! Need some diagnostics from loader here

    struct pvm_object new_class;

    if( pvm_load_class_from_module(buf, &new_class))
    {
        const char *msg = " - class load error";
        if( strlen(buf) >= bufs - 2 - strlen(msg) )
        {
            SYSCALL_THROW_STRING( msg+3 );
        }
        else
        {
            strcat( buf, msg );
            SYSCALL_THROW_STRING( buf );
        }
    }
    else
    {
    	SYSCALL_RETURN(new_class);
    }
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (58, N'si_bootstrap_9_load_code', N'    DEBUG_INFO;

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

    struct pvm_object name = POP_ARG;
    ASSERT_STRING(name);

#if 0
    const int bufs = 1024;
    char buf[bufs+1];


    struct data_area_4_string *nameda = pvm_object_da( name, string );

    int len = nameda->length > bufs ? bufs : nameda->length;
    memcpy( buf, nameda->data, len );
    buf[len] = ''\0'';
    SYS_FREE_O(name);

    code_seg cs = load_code(buf);

    SYSCALL_RETURN(pvm_object_storage::create_code( cs.get_code_size(), cs.get_code() ));
#else
    SYS_FREE_O(name);
    SYSCALL_THROW_STRING( "load code not implemented" );
#endif
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (59, N'si_bootstrap_16_print', N'    (void)me;
    DEBUG_INFO;

    int n_param = POP_ISTACK;

    while( n_param-- )
        {
    	struct pvm_object o = POP_ARG;
        pvm_object_print( o );
        SYS_FREE_O( o );
        }

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (60, N'si_bootstrap_17_register_class_loader', N'    (void)me;
    DEBUG_INFO;

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

    struct pvm_object loader = POP_ARG;

    pvm_root.class_loader = loader;
    pvm_object_storage_t *root = get_root_object_storage();
    pvm_set_field( root, PVM_ROOT_OBJECT_CLASS_LOADER, pvm_root.class_loader );

    // Don''t need do SYS_FREE_O(loader) since we store it

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (61, N'si_bootstrap_18_thread', N'    DEBUG_INFO;

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

    struct pvm_object object = POP_ARG;

    // Don''t need do SYS_FREE_O(object) since we store it as ''this''

#if 1
    // TODO check object class to be runnable or subclass

    {
    struct pvm_object new_cf = pvm_create_call_frame_object();
    struct data_area_4_call_frame* cfda = pvm_object_da( new_cf, call_frame );

    pvm_ostack_push( pvm_object_da(cfda->ostack, object_stack), me );
    pvm_istack_push( pvm_object_da(cfda->istack, integer_stack), 1); // pass him real number of parameters

    struct pvm_object_storage *code = pvm_exec_find_method( object, 8 );
    pvm_exec_set_cs( cfda, code );
    cfda->this_object = object;

    struct pvm_object thread = pvm_create_thread_object( new_cf );

    //printf("here?\n");

    phantom_activate_thread(thread);
    }
#endif


    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (62, N'si_bootstrap_19_create_binary', N'    (void)me;
    DEBUG_INFO;

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

    int nbytes = POP_INT();

    SYSCALL_RETURN( pvm_create_binary_object(nbytes, NULL) );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (63, N'si_bootstrap_20_set_screen_background', N'    (void)me;
    DEBUG_INFO;

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

    struct pvm_object _bmp = POP_ARG;

#if !BACK_WIN
    if( drv_video_bmpblt(_bmp,0,0,0) )
    	SYSCALL_THROW_STRING( "not a bitmap" );


    drv_video_window_repaint_all();
#else
    // TODO black screen :(

    if( !pvm_object_class_exactly_is( _bmp, pvm_get_bitmap_class() ) )
    	SYSCALL_THROW_STRING( "not a bitmap" );

    struct data_area_4_bitmap *bmp = pvm_object_da( _bmp, bitmap );
    struct data_area_4_binary *bin = pvm_object_da( bmp->image, binary );


#if !VIDEO_NEW_BG_WIN
    if(back_win == 0)
    	back_win = drv_video_window_create( scr_get_xsize(), scr_get_ysize(), 0, 0, COLOR_BLACK, "Background", WFLAG_WIN_DECORATED );

    back_win->flags &= ~WFLAG_WIN_DECORATED;
    back_win->flags |= WFLAG_WIN_NOFOCUS;

    w_to_bottom(back_win);

    //drv_video_bitblt( (void *)bin->data, 0, 0, bmp->xsize, bmp->ysize, (zbuf_t)zpos );
#else
    window_handle_t back_win = w_get_bg_window();
#endif

    bitmap2bitmap(
    		back_win->w_pixel, back_win->xsize, back_win->ysize, 0, 0,
                     (void *)bin->data, bmp->xsize, bmp->ysize, 0, 0,
                     bmp->xsize, bmp->ysize
                    );

    //drv_video_winblt(back_win);
    w_update( back_win );
    //scr_repaint_all();
#endif
    // Remove it if will store bmp!
    SYS_FREE_O(_bmp);

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (64, N'si_bootstrap_21_sleep', N'    (void)me;
    DEBUG_INFO;

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

    int msec = POP_INT();
#if OLD_VM_SLEEP
    phantom_wakeup_after_msec(msec,tc);

    //#warning to kill
    SHOW_ERROR0( 0, "si_bootstrap_21_sleep used" );

    if(phantom_is_a_real_kernel())
        SYSCALL_PUT_THIS_THREAD_ASLEEP(0);
#else
    (void) msec;
    SHOW_ERROR0( 0, "si_bootstrap_21_sleep used" );
#endif
    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (65, N'si_bootstrap_22_set_os_interface', N'    (void)me;
    DEBUG_INFO;

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

    pvm_root.os_entry = POP_ARG;
    ref_saturate_o(pvm_root.os_entry); // make sure refcount is disabled for this object
    struct pvm_object_storage *root = get_root_object_storage();
    pvm_set_field( root, PVM_ROOT_OBJECT_OS_ENTRY, pvm_root.os_entry );
    // No ref dec - we store it.

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (66, N'si_bootstrap_23_getenv', N'    (void)me;
    DEBUG_INFO;
    SYSCALL_RETURN( ref_inc_o( pvm_root.kernel_environment ) );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (67, N'si_array_5_tostring', N'    (void)me;
    DEBUG_INFO;
    // BUG? Recursively call tostring for all of them?
    SYSCALL_RETURN(pvm_create_string_object( "array" ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (68, N'si_array_8_get_iterator', N'    (void)me;
    DEBUG_INFO;

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 0);
    SYSCALL_THROW_STRING( "get iterator is not implemented yet" );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (69, N'si_array_9_get_subarray', N'    (void)me;
    DEBUG_INFO;

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 2);

    /*
    int len = POP_INT();
    int base = POP_INT();
    SYSCALL_RETURN();
    */

    SYSCALL_THROW_STRING( "get subarray is not implemented yet" );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (70, N'si_array_10_get', N'    DEBUG_INFO;

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

    unsigned int index = POP_INT();

    struct data_area_4_array *da = (struct data_area_4_array *)me.data->da;

    if( index >= da->used_slots )
        SYSCALL_THROW_STRING( "array get - index is out of bounds" );

    struct pvm_object o = pvm_get_ofield( da->page, index);
    SYSCALL_RETURN( ref_inc_o( o ) );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (71, N'si_array_11_set', N'    DEBUG_INFO;

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 2);

    int index = POP_INT();

    struct pvm_object value = POP_ARG;

    pvm_set_array_ofield( me.data, index, value );

    // we increment refcount and return object back.
    // it will possibly be dropped and refcount will decrement again then.
    SYSCALL_RETURN( ref_inc_o( value ) );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (72, N'si_array_12_size', N'    DEBUG_INFO;
    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 0);

    struct data_area_4_array *da = (struct data_area_4_array *)me.data->da;

    SYSCALL_RETURN(pvm_create_int_object( da->used_slots ) );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (73, N'si_binary_5_tostring', N'    (void)o;
    DEBUG_INFO;
    // TODO hexdump

    if(1)
    {
        struct data_area_4_binary *da = pvm_object_da( o, binary );
        int size = o.data->_da_size - sizeof( struct data_area_4_binary );

        hexdump( da->data, size, "", 0);
    }

    SYSCALL_RETURN(pvm_create_string_object( "(binary)" ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (74, N'si_binary_8_getbyte', N'    DEBUG_INFO;
    struct data_area_4_binary *da = pvm_object_da( me, binary );

    unsigned int index = POP_INT();

    int size = me.data->_da_size - sizeof( struct data_area_4_binary );

    //if( index < 0 || index >= size )
    if( index >= size )
        SYSCALL_THROW_STRING( "binary index out of bounds" );

    SYSCALL_RETURN(pvm_create_int_object( da->data[index] ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (75, N'si_binary_9_setbyte', N'    DEBUG_INFO;
    struct data_area_4_binary *da = pvm_object_da( me, binary );

    unsigned int byte = POP_INT();
    unsigned int index = POP_INT();

    int size = me.data->_da_size - sizeof( struct data_area_4_binary );

    //if( index < 0 || index >= size )
    if( index >= size )
        SYSCALL_THROW_STRING( "binary index out of bounds" );

    da->data[index] = byte;

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (76, N'si_binary_10_setrange', N'    DEBUG_INFO;
    struct data_area_4_binary *da = pvm_object_da( me, binary );

    unsigned int len = POP_INT();
    unsigned int frompos = POP_INT();
    unsigned int topos = POP_INT();

    // TODO assert his class!!
    struct pvm_object _src = POP_ARG;
    struct data_area_4_binary *src = pvm_object_da( _src, binary );


    int size = me.data->_da_size - sizeof( struct data_area_4_binary );

    //if( topos < 0 || topos+len > size )
    if( topos+len > size )
        SYSCALL_THROW_STRING( "binary copy dest index/len out of bounds" );

    int src_size = _src.data->_da_size - sizeof( struct data_area_4_binary );

    //if( frompos < 0 || frompos+len > src_size )
    if( frompos+len > src_size )
        SYSCALL_THROW_STRING( "binary copy src index/len out of bounds" );

    //da->data[index] = byte;
    memcpy( (da->data)+topos, (src->data)+frompos, len );

    SYS_FREE_O(_src);

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (77, N'si_closure_9_getordinal', N'    DEBUG_INFO;
    struct data_area_4_closure *da = pvm_object_da( me, closure );

    SYSCALL_RETURN(pvm_create_int_object( da->ordinal ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (78, N'si_closure_10_setordinal', N'    DEBUG_INFO;
    struct data_area_4_closure *da = pvm_object_da( me, closure );

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

    da->ordinal = POP_INT();

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (79, N'si_closure_11_setobject', N'    DEBUG_INFO;
    struct data_area_4_closure *da = pvm_object_da( me, closure );

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

    // We do not decrement its refcount, ''cause we store it.
    da->object = POP_ARG;

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (80, N'si_bitmap_5_tostring', N'    (void)o;
    DEBUG_INFO;
    // TODO hexdump
    SYSCALL_RETURN(pvm_create_string_object( "(bitmap)" ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (81, N'si_bitmap_8_fromstring', N'    DEBUG_INFO;
    struct data_area_4_bitmap *da = pvm_object_da( me, bitmap );

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

//printf("Load from string\n");

    pvm_object_t _s = POP_ARG;

    if( drv_video_string2bmp( da, pvm_object_da( _s, string)->data ) )
    	SYSCALL_THROW_STRING("can not parse graphics data");

    SYS_FREE_O(_s);

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (82, N'si_bitmap_9_paintto', N'    DEBUG_INFO;
    struct data_area_4_bitmap *da = pvm_object_da( me, bitmap );

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

    int y = POP_INT();
    int x = POP_INT();
    struct pvm_object _tty = POP_ARG;

    // TODO check class!
    struct data_area_4_tty *tty = pvm_object_da( _tty, tty );
    struct data_area_4_binary *pixels = pvm_object_da( da->image, binary );

    bitmap2bitmap(
    		tty->pixel, tty->w.xsize, tty->w.ysize, x, y,
    		(rgba_t *)pixels, da->xsize, da->ysize, 0, 0,
    		da->xsize, da->ysize
    );
    //drv_video_winblt( &(tty->w), tty->w.x, tty->w.y);
    w_update( &(tty->w) );

    SYS_FREE_O(_tty);

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (83, N'si_world_5_tostring', N'    (void)o;
    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_string_object( "(world)" ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (84, N'si_world_8_getMyThread', N'    (void)o;
    DEBUG_INFO;

    // TODO spinlock!
    if(thread_iface == 0 )
    {
        struct data_area_4_class *cda = pvm_object_da( pvm_get_thread_class(), class );
        thread_iface = cda->object_default_interface.data;
    }

    struct pvm_object out;

    out.data =
        (pvm_object_storage_t *)
        (tc - DA_OFFSET()); // TODO XXX HACK!
    out.interface = thread_iface;

    SYSCALL_RETURN( ref_inc_o( out ) );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (85, N'si_weakref_5_tostring', N'    (void)o;
    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_string_object( "(weakref)" ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (86, N'si_weakref_8_getMyObject', N'    DEBUG_INFO;
#if 0
    struct data_area_4_weakref *da = pvm_object_da( o, weakref );

    // All we do is return new reference to our object,
    // incrementing refcount before
    int ie = hal_save_cli();
    hal_spin_lock( &da->lock );

    struct pvm_object out = ref_inc_o( da->object );

    hal_spin_unlock( &da->lock );
    if( ie ) hal_sti();
#else
    pvm_object_t out = pvm_weakref_get_object( o );
#endif
    SYSCALL_RETURN( out );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (87, N'si_directory_4_equals', N'    (void)o;
    DEBUG_INFO;
    SYSCALL_THROW_STRING( "dir.equals: not implemented" );
    //SYSCALL_RETURN(pvm_create_string_object( "(directory)" ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (88, N'si_directory_5_tostring', N'    (void)o;
    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_string_object( "(directory)" ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (89, N'si_directory_8_put', N'    struct data_area_4_directory *da = pvm_object_da( o, directory );
    DEBUG_INFO;

    struct pvm_object val = POP_ARG;
    struct pvm_object key = POP_ARG;
    ASSERT_STRING(key);

    errno_t rc = hdir_add( da, pvm_get_str_data(key), pvm_get_str_len(key), val );

    SYS_FREE_O(key); // dir code creates it''s own binary object
    if( rc ) SYS_FREE_O( val ); // we didn''t put it there

    SYSCALL_RETURN(pvm_create_int_object( rc ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (90, N'si_directory_9_get', N'    struct data_area_4_directory *da = pvm_object_da( o, directory );
    DEBUG_INFO;

    struct pvm_object key = POP_ARG;
    ASSERT_STRING(key);

    pvm_object_t out;
    errno_t rc = hdir_find( da, pvm_get_str_data(key), pvm_get_str_len(key), &out, 0 );
    if( rc )
        SYSCALL_RETURN_NOTHING;
    else
        SYSCALL_RETURN(out);
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (91, N'si_directory_10_remove', N'    struct data_area_4_directory *da = pvm_object_da( o, directory );
    DEBUG_INFO;

    struct pvm_object key = POP_ARG;
    ASSERT_STRING(key);

    pvm_object_t out; // unused
    errno_t rc = hdir_find( da, pvm_get_str_data(key), pvm_get_str_len(key), &out, 1 );
    SYSCALL_RETURN(pvm_create_int_object( rc ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (92, N'si_directory_11_size', N'    struct data_area_4_directory *da = pvm_object_da( o, directory );
    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_int_object( da->nEntries ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (93, N'si_directory_12_iterate', N'    (void)o;
    DEBUG_INFO;
    // TODO implement dir iterator

    SYSCALL_THROW_STRING( "dir.iterate: not implemented" );
    SYSCALL_RETURN_NOTHING;
    //return pvm_create_null_object();
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (94, N'si_connection_5_tostring', N'    (void)o;
    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_string_object( "(connection)" ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (95, N'si_connection_8_connect', N'    DEBUG_INFO;
    struct data_area_4_connection *da = pvm_object_da( o, connection );

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);


    pvm_object_t _s = POP_ARG;

    if(!IS_PHANTOM_STRING(_s))
    {
        SYS_FREE_O(_s);
        SYSCALL_THROW_STRING( "connection.connect: not a string arg" );
    }

    int slen = pvm_get_str_len(_s);

    if( slen+1 > sizeof( da->name ) )
    {
        SYS_FREE_O(_s);
        SYSCALL_THROW_STRING( "string arg too long" );
    }

    strncpy( da->name, pvm_get_str_data(_s), slen );
    SYS_FREE_O(_s);

    printf(".internal.connection: Connect to ''%s''\n", da->name );

    int ret = pvm_connect_object(o,tc);

    SYSCALL_RETURN(pvm_create_int_object( ret ) );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (96, N'si_connection_9_disconnect', N'    DEBUG_INFO;
    //struct data_area_4_connection *da = pvm_object_da( o, connection );

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 0);

    int ret = pvm_disconnect_object(o,tc);

    SYSCALL_RETURN(pvm_create_int_object( ret ) );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (97, N'si_connection_10_check', N'    DEBUG_INFO;
    struct data_area_4_connection *da = pvm_object_da( o, connection );

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

    int op_index = POP_INT();

#if !PVM_CONNECTION_WAKE
    (void) op_index;
    (void) da;
    int ret = ENXIO;
#else
    int ret = 0;

    if( da->kernel == 0 || da->kernel->check_operation || da->kernel->req_wake )
    {
        ret = ENXIO;
    }
    else
    {
        ret = da->kernel->check_operation( op_index, da, tc );
        if( ret )
        {
            // TODO races?
            ret = da->kernel->req_wake( op_index, da, tc );
            if( ret == 0 )
                SYSCALL_PUT_THIS_THREAD_ASLEEP();
        }
    }
#endif
    SYSCALL_RETURN(pvm_create_int_object( ret ) );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (98, N'si_connection_11_do', N'    DEBUG_INFO;
    struct data_area_4_connection *da = pvm_object_da( o, connection );

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 2);

    int op_index = POP_INT();
    pvm_object_t arg = POP_ARG;

    int ret = 0;

    if( (da->kernel == 0) || (da->kernel->do_operation == 0) )
    {
        ret = ENXIO;
    }
    else
    {
        ret = da->kernel->do_operation( op_index, da, tc, arg );
    }

    SYS_FREE_O(arg);
    SYSCALL_RETURN(pvm_create_int_object( ret ) );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (99, N'si_connection_12_set_callback', N'    DEBUG_INFO;
    struct data_area_4_connection *da = pvm_object_da( o, connection );

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 2);

    int nmethod = POP_INT();
    pvm_object_t callback_object = POP_ARG;

    int ret = !pvm_isnull( da->callback );

    // No sync - assume caller does it before getting real callbacks
    da->callback = callback_object;
    da->callback_method = nmethod;

    SYSCALL_RETURN(pvm_create_int_object( ret ) );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (100, N'si_connection_13_blocking', N'    DEBUG_INFO;
    struct data_area_4_connection *da = pvm_object_da( o, connection );

    /*
    int n_param = POP_ISTACK;
    pvm_istack_push( tc->_istack, n_param-1 ); // we''ll take one

    if( n_param < 1 )
        SYSCALL_THROW(pvm_create_string_object( "blocking: need at least 1 parameter" )); \

    int nmethod = POP_INT();
    */

    pvm_object_t (*syscall_worker)( pvm_object_t , struct data_area_4_thread *, int nmethod, pvm_object_t arg ) = da->blocking_syscall_worker;

    //SHOW_FLOW( 1, "blocking call to nmethod = %d", nmethod);
    return vm_syscall_block( o, tc, syscall_worker );
    // vm_syscall_block pushes retcode itself
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (101, N'si_tcp_tostring_5', N'    (void) me;
    DEBUG_INFO;
    SYSCALL_RETURN( pvm_create_string_object( "tcp socket" ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (102, N'si_tcp_connect_16', N'    (void) me;
    //struct data_area_4_tcp      *da = pvm_data_area( me, tcp );

    DEBUG_INFO;
    int n_param = POP_ISTACK;

    CHECK_PARAM_COUNT(n_param, 2);

    //SYSCALL_PUT_THIS_THREAD_ASLEEP()

    SYSCALL_THROW_STRING( "not implemented" );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (103, N'si_tcp_disconnect_17', N'    (void) me;
    //struct data_area_4_tcp      *da = pvm_data_area( me, tcp );

    DEBUG_INFO;
    int n_param = POP_ISTACK;

    CHECK_PARAM_COUNT(n_param, 2);


    SYSCALL_THROW_STRING( "not implemented" );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (104, N'si_tcp_waitsend_18', N'    (void) me;
    //struct data_area_4_tcp      *da = pvm_data_area( me, tcp );

    DEBUG_INFO;
    int n_param = POP_ISTACK;

    CHECK_PARAM_COUNT(n_param, 0);

//    if(!si_tcp_ready_to_send(da))
//        SYSCALL_PUT_THIS_THREAD_ASLEEP();

    SYSCALL_THROW_STRING( "not implemented" );
    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (105, N'si_tcp_send_19', N'    (void) me;
    //struct data_area_4_tcp      *da = pvm_data_area( me, tcp );

    DEBUG_INFO;
    int n_param = POP_ISTACK;

    CHECK_PARAM_COUNT(n_param, 2);


    SYSCALL_THROW_STRING( "not implemented" );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (106, N'si_tcp_waitrecv_20', N'    (void) me;
    //struct data_area_4_tcp      *da = pvm_data_area( me, tcp );

    DEBUG_INFO;
    int n_param = POP_ISTACK;

    CHECK_PARAM_COUNT(n_param, 2);

//    if(!si_tcp_ready_to_recv(da))
//        SYSCALL_PUT_THIS_THREAD_ASLEEP();

    SYSCALL_THROW_STRING( "not implemented" );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (107, N'si_tcp_recv_21', N'    (void) me;
    //struct data_area_4_tcp      *da = pvm_data_area( me, tcp );

    DEBUG_INFO;
    int n_param = POP_ISTACK;

    CHECK_PARAM_COUNT(n_param, 2);


    SYSCALL_THROW_STRING( "not implemented" );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (108, N'si_tcp_waitaccept_22', N'    (void) me;
    //struct data_area_4_tcp      *da = pvm_data_area( me, tcp );

    DEBUG_INFO;
    int n_param = POP_ISTACK;

    CHECK_PARAM_COUNT(n_param, 1);
    int backlog = POP_INT();
    (void) backlog;

    //SYSCALL_PUT_THIS_THREAD_ASLEEP()

    SYSCALL_THROW_STRING( "not implemented" );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (109, N'si_tcp_accept_23', N'    (void) me;
    //struct data_area_4_tcp      *da = pvm_data_area( me, tcp );

    DEBUG_INFO;
    int n_param = POP_ISTACK;

    CHECK_PARAM_COUNT(n_param, 2);


    SYSCALL_THROW_STRING( "not implemented" );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (110, N'si_mutex_5_tostring', N'    (void)o;
    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_string_object( "mutex" ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (111, N'si_mutex_8_lock', N'    DEBUG_INFO;
    vm_mutex_lock( me, tc );
    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (112, N'si_mutex_9_unlock', N'    DEBUG_INFO;
    //struct data_area_4_mutex *da = pvm_object_da( me, mutex );
    //(void)da;

    // No locking in syscalls!!
    //pthread_mutex_unlock(&(da->mutex));

    errno_t rc = vm_mutex_unlock( me, tc );
    switch(rc)
    {
    case EINVAL:
        printf("mutex unlock - not owner");
        SYSCALL_THROW_STRING( "mutex unlock - not owner" );
        // unreached
        break;

    default: break;
    }

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (113, N'si_mutex_10_trylock', N'    DEBUG_INFO;
    //struct data_area_4_mutex *da = pvm_object_da( me, mutex );

    // No locking in syscalls!!
    //SYSCALL_RETURN(pvm_create_int_object( pthread_mutex_trylock(&(da->mutex)) ));

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (114, N'si_cond_5_tostring', N'    (void)o;
    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_string_object( ".internal.cond" ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (115, N'si_cond_8_wait', N'    (void)me;
    DEBUG_INFO;
    struct data_area_4_cond *da = pvm_object_da( me, cond );
    (void)da;

    // No locking in syscalls!!
    //pthread_cond_wait(&(da->cond));

    //SYSCALL_PUT_THIS_THREAD_ASLEEP();
    SYSCALL_THROW_STRING( "wait not impl" );

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (116, N'si_cond_9_twait', N'    (void)me;
    DEBUG_INFO;
    struct data_area_4_cond *da = pvm_object_da( me, cond );
    (void)da;

    SYSCALL_THROW_STRING( "timed wait not impl" );

    // No locking in syscalls!!
    //pthread_cond_timedwait(&(da->cond));

    //SYSCALL_PUT_THIS_THREAD_ASLEEP();


    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (117, N'si_cond_10_broadcast', N'    DEBUG_INFO;
    struct data_area_4_cond *da = pvm_object_da( me, cond );
    (void)da;

    // No locking in syscalls!!
    //pthread_cond_broadcast(&(da->cond));

    //SYSCALL_WAKE_THREAD_UP(thread)

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (118, N'si_cond_11_signal', N'    DEBUG_INFO;
    struct data_area_4_cond *da = pvm_object_da( me, cond );
    (void)da;

    // No locking in syscalls!!
    //pthread_cond_signal(&(da->cond));

    //SYSCALL_WAKE_THREAD_UP(thread)

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (119, N'si_sema_5_tostring', N'    (void)o;
    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_string_object( ".internal.sema" ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (120, N'si_sema_8_acquire', N'#if OLD_VM_SLEEP
    DEBUG_INFO;
    struct data_area_4_sema *da = pvm_object_da( me, sema );
    VM_SPIN_LOCK(da->poor_mans_pagefault_compatible_spinlock);

    while( da->sem_value <= 0 )
    {
        // Sema is busy, fall asleep now
        pvm_object_t this_thread = pvm_da_to_object(tc);

        assert(!pvm_isnull(this_thread));
        assert(pvm_object_class_is( this_thread, pvm_get_thread_class() ) );

        pvm_set_ofield( da->waiting_threads_array, da->nwaiting++, this_thread );

//#warning have SYSCALL_PUT_THIS_THREAD_ASLEEP unlock the spinlock!
        //VM_SPIN_UNLOCK(da->poor_mans_pagefault_compatible_spinlock);
        SYSCALL_PUT_THIS_THREAD_ASLEEP(&da->poor_mans_pagefault_compatible_spinlock);
        VM_SPIN_LOCK(da->poor_mans_pagefault_compatible_spinlock);
    }

    da->sem_value--;
    da->owner_thread = tc;

    VM_SPIN_UNLOCK(da->poor_mans_pagefault_compatible_spinlock);
    SYSCALL_RETURN_NOTHING;
#else
    SYSCALL_THROW_STRING("Not this way");
#endif
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (121, N'si_sema_9_tacquire', N'    (void)me;
    DEBUG_INFO;
    struct data_area_4_sema *da = pvm_object_da( me, sema );
    (void)da;

    SYSCALL_THROW_STRING( "timed acquire not impl" );


    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (122, N'si_sema_10_zero', N'    DEBUG_INFO;
    struct data_area_4_sema *da = pvm_object_da( me, sema );

    if( da->sem_value > 0 )
        da->sem_value = 0;

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (123, N'si_sema_11_release', N'#if OLD_VM_SLEEP
    DEBUG_INFO;
    struct data_area_4_sema *da = pvm_object_da( me, sema );
    VM_SPIN_LOCK(da->poor_mans_pagefault_compatible_spinlock);

    da->sem_value++;

    if( da->nwaiting > 0 )
    {
        // Wakeup one
        // TODO takes last, must take first
        pvm_object_t next_thread = pvm_get_ofield( da->waiting_threads_array, --da->nwaiting );

        assert(!pvm_isnull(next_thread));
        assert(pvm_object_class_is( next_thread, pvm_get_thread_class() ) );

        da->owner_thread = pvm_object_da( next_thread, thread );
        SYSCALL_WAKE_THREAD_UP( da->owner_thread );
    }

    VM_SPIN_UNLOCK(da->poor_mans_pagefault_compatible_spinlock);
    SYSCALL_RETURN_NOTHING;
#else
    SYSCALL_THROW_STRING("Not this way");
#endif
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (124, N'tostring_5', N'    (void) me;
    DEBUG_INFO;
    SYSCALL_RETURN( pvm_create_string_object( "tty window" ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (125, N'putws_17', N'    DEBUG_INFO;

    struct data_area_4_tty      *da = pvm_data_area( me, tty );

    //printf("putws font %d,%d\n", da->font_width, da->font_height );


    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

    struct pvm_object _text = POP_ARG;
    ASSERT_STRING(_text);

    int len = pvm_get_str_len( _text );
    const char * data = (const char *)pvm_get_str_data(_text);

    char buf[BS+2];

    if( len > BS ) len = BS;
    strncpy( buf, data, len );
    //buf[len] = ''\n'';
    buf[len] = 0;

    SYS_FREE_O(_text);

    //printf("tty print: ''%s'' at %d,%d\n", buf, da->x, da->y );

    struct rgba_t fg = da->fg;
    struct rgba_t bg = da->bg;

    w_font_tty_string( &(da->w), tty_font, buf, fg, bg, &(da->x), &(da->y) );
    w_update( &(da->w) );

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (126, N'getwc_16', N'    (void) me;
    DEBUG_INFO;
    char c[1];

    // TODO XXX syscall blocks!
    c[0] = phantom_dev_keyboard_getc();

    SYSCALL_RETURN( pvm_create_string_object_binary( c, 1 ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (127, N'debug_18', N'    (void) me;
    DEBUG_INFO;

    //struct data_area_4_tty      *da = pvm_data_area( me, tty );


    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

    struct pvm_object o = POP_ARG;

    //pvm_object_print( o );
    printf("\n\nobj dump: ");
    dumpo((addr_t)(o.data));
    printf("\n\n");

    SYS_FREE_O(o);

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (128, N'gotoxy_19', N'    struct data_area_4_tty      *da = pvm_data_area( me, tty );

    DEBUG_INFO;
    int n_param = POP_ISTACK;

    CHECK_PARAM_COUNT(n_param, 2);

    int goy = POP_INT();
    int gox = POP_INT();

    da->x = da->font_width * gox;
    da->y = da->font_height * goy;

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (129, N'clear_20', N'    struct data_area_4_tty      *da = pvm_data_area( me, tty );

    DEBUG_INFO;

    da->x = da->y = 0;

    w_fill( &(da->w), da->bg );
    w_update( &(da->w) );

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (130, N'setcolor_21', N'    (void) me;
    //struct data_area_4_tty      *da = pvm_data_area( me, tty );

    DEBUG_INFO;
    int n_param = POP_ISTACK;

    CHECK_PARAM_COUNT(n_param, 1);

    int color = POP_INT();
    (void) color;
    //int attr = (short)color;

    // TODO colors from attrs
    //printf("setcolor  font %d,%d\n", da->font_width, da->font_height );

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (131, N'fill_22', N'    (void) me;
    DEBUG_INFO;
    SYSCALL_THROW_STRING( "not implemented" );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (132, N'putblock_23', N'    (void) me;
    DEBUG_INFO;
    SYSCALL_THROW_STRING( "not implemented" );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (133, N'tty_setWinPos_24', N'    DEBUG_INFO;
    struct data_area_4_tty      *da = pvm_data_area( me, tty );

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 2);

    int y = POP_INT();
    int x = POP_INT();

    w_move( &(da->w), x, y );

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (134, N'tty_setWinTitle_25', N'    DEBUG_INFO;
    struct data_area_4_tty      *da = pvm_data_area( me, tty );


    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

    struct pvm_object _text = POP_ARG;
    ASSERT_STRING(_text);

    int len = pvm_get_str_len( _text );
    const char * data = (const char *)pvm_get_str_data(_text);

    if( len > PVM_MAX_TTY_TITLE-1 ) len = PVM_MAX_TTY_TITLE-1 ;
    strlcpy( da->title, data, len+1 );
    //buf[len] = 0;

    SYS_FREE_O(_text);

    w_set_title( &(da->w), da->w.title );

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (135, N'si_window_5_tostring', N'    (void)o;
    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_string_object( "(window)" ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (136, N'win_getXSize', N'    struct data_area_4_window      *da = pvm_data_area( o, window );
    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_int_object( da->w.xsize ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (137, N'win_getYSize', N'    struct data_area_4_window      *da = pvm_data_area( o, window );
    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_int_object( da->w.ysize ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (138, N'win_getX', N'    struct data_area_4_window      *da = pvm_data_area( o, window );
    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_int_object( da->x ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (139, N'win_getY', N'    struct data_area_4_window      *da = pvm_data_area( o, window );
    DEBUG_INFO;
    SYSCALL_RETURN(pvm_create_int_object( da->y ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (140, N'win_clear_20', N'    struct data_area_4_window      *da = pvm_data_area( me, window );

    DEBUG_INFO;

    da->x = da->y = 0;

    w_fill( &(da->w), da->bg );
    w_update( &(da->w) );

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (141, N'win_fill_21', N'    struct data_area_4_window      *da = pvm_data_area( me, window );
    DEBUG_INFO;

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);
    int color = POP_INT();

    rgba_t c;
    INT32_TO_RGBA(c, color);
    w_fill( &(da->w), c );

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (142, N'win_setFGcolor_22', N'    struct data_area_4_window      *da = pvm_data_area( me, window );

    DEBUG_INFO;
    int n_param = POP_ISTACK;

    CHECK_PARAM_COUNT(n_param, 1);

    int color = POP_INT();
    INT32_TO_RGBA(da->fg, color);

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (143, N'win_setBGcolor_23', N'    struct data_area_4_window      *da = pvm_data_area( me, window );

    DEBUG_INFO;
    int n_param = POP_ISTACK;

    CHECK_PARAM_COUNT(n_param, 1);

    int color = POP_INT();
    INT32_TO_RGBA(da->bg, color);

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (144, N'win_putString_24', N'    DEBUG_INFO;

    struct data_area_4_tty      *da = pvm_data_area( me, tty );

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 3);

    struct pvm_object _text = POP_ARG;
    ASSERT_STRING(_text);

    int x = POP_INT();
    int y = POP_INT();


    int len = pvm_get_str_len( _text );
    const char * data = (const char *)pvm_get_str_data(_text);

#define BS 1024
    char buf[BS+2];

    if( len > BS ) len = BS;
    strncpy( buf, data, len );
    buf[len] = 0;

    SYS_FREE_O(_text);

    //printf("tty print: ''%s'' at %d,%d\n", buf, da->x, da->y );

    struct rgba_t fg = da->fg;
    struct rgba_t bg = da->bg;

    // TODO make a version of drv_video_font_tty_string that accepts non-zero terminated strings with len
    w_font_tty_string( &(da->w), tty_font, buf, fg, bg, &x, &y );
    w_update( &(da->w) );

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (145, N'win_putImage_25', N'    DEBUG_INFO;
    struct data_area_4_window      *da = pvm_data_area( me, window );

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 3);

    struct pvm_object _img = POP_ARG;
    int y = POP_INT();
    int x = POP_INT();

    // TODO check class!
    struct data_area_4_bitmap *_bmp = pvm_object_da( _img, bitmap );
    //struct data_area_4_tty *tty = pvm_object_da( _tty, tty );
    struct data_area_4_binary *pixels = pvm_object_da( _bmp->image, binary );

    bitmap2bitmap(
    		da->pixel, da->w.xsize, da->w.ysize, x, y,
    		(rgba_t *)pixels, _bmp->xsize, _bmp->ysize, 0, 0,
    		_bmp->xsize, _bmp->ysize
    );
    //drv_video_winblt( &(tty->w), tty->w.x, tty->w.y);
    // Sure?
    w_update( &(da->w) );

    SYS_FREE_O(_img);

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (146, N'win_setSize_26', N'    DEBUG_INFO;
    struct data_area_4_window      *da = pvm_data_area( me, window );

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 2);

    int y = POP_INT();
    int x = POP_INT();

    if(x*y > PVM_MAX_WIN_PIXELS)
        SYSCALL_THROW_STRING( "new win size > PVM_MAX_WIN_PIXELS" );

    w_resize( &(da->w), x, y );

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (147, N'win_setPos_27', N'    DEBUG_INFO;
    struct data_area_4_window      *da = pvm_data_area( me, window );

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 2);

    int y = POP_INT();
    int x = POP_INT();

    w_move( &(da->w), x, y );

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (148, N'win_drawLine_28', N'    DEBUG_INFO;
    struct data_area_4_window      *da = pvm_data_area( me, window );

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 4);

    int ys = POP_INT();
    int xs = POP_INT();
    int y = POP_INT();
    int x = POP_INT();

    w_draw_line( &(da->w), x, y, x+xs, y+ys, da->fg );


    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (149, N'win_drawBox_29', N'    DEBUG_INFO;
    struct data_area_4_window      *da = pvm_data_area( me, window );

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 4);

    int ys = POP_INT();
    int xs = POP_INT();
    int y = POP_INT();
    int x = POP_INT();

    w_draw_box( &(da->w), x, y, xs, ys, da->fg );

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (150, N'win_fillBox_30', N'    DEBUG_INFO;
    struct data_area_4_window      *da = pvm_data_area( me, window );

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 4);

    int ys = POP_INT();
    int xs = POP_INT();
    int y = POP_INT();
    int x = POP_INT();

    w_fill_box( &(da->w), x, y, xs, ys, da->fg );

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (151, N'win_fillEllipse_31', N'    DEBUG_INFO;
    struct data_area_4_window      *da = pvm_data_area( me, window );

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 4);

    int ys = POP_INT();
    int xs = POP_INT();
    int y = POP_INT();
    int x = POP_INT();

    w_fill_ellipse( &(da->w), x, y, xs, ys, da->fg );

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (152, N'win_setHandler_32', N'    DEBUG_INFO;
    struct data_area_4_window      *da = pvm_data_area( me, window );

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

    struct pvm_object handler = POP_ARG;

    // TODO check class!

//    da->event_handler = handler;

    {
    struct data_area_4_connection  *cda = (struct data_area_4_connection *)da->connector.data->da;
    // No sync - assume caller does it before getting real callbacks
    cda->callback = handler;
    cda->callback_method = 8; // TODO BUG FIXME
    }

    //SYS_FREE_O(_img);

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (153, N'win_setTitle_33', N'    DEBUG_INFO;
    struct data_area_4_window      *da = pvm_data_area( me, window );


    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 1);

    struct pvm_object _text = POP_ARG;
    ASSERT_STRING(_text);

    int len = pvm_get_str_len( _text );
    const char * data = (const char *)pvm_get_str_data(_text);

    if( len > PVM_MAX_TTY_TITLE-1 ) len = PVM_MAX_TTY_TITLE-1 ;
    strlcpy( da->title, data, len+1 );
    //buf[len] = 0;

    SYS_FREE_O(_text);

    w_set_title( &(da->w), da->w.title );

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (154, N'win_update_34', N'    DEBUG_INFO;
    struct data_area_4_window      *da = pvm_data_area( me, window );


    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 0);


    w_update( &(da->w) );

    SYSCALL_RETURN_NOTHING;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (155, N'win_scrollHor_35', N'    DEBUG_INFO;
    struct data_area_4_window      *da = pvm_data_area( me, window );

    int n_param = POP_ISTACK;
    CHECK_PARAM_COUNT(n_param, 5);

    int s = POP_INT();
    int ys = POP_INT();
    int xs = POP_INT();
    int y = POP_INT();
    int x = POP_INT();

    errno_t err = w_scroll_hor( &(da->w), x, y, xs, ys, s );

    //SYSCALL_RETURN_NOTHING;
    SYSCALL_RETURN(pvm_create_int_object( err ));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (156, N'pvm_internal_init_void', N'(void)os;')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (157, N'pvm_internal_init_class', N'	struct data_area_4_class *      da = (struct data_area_4_class *)os->da;

	da->object_data_area_size   	= 0;
	da->object_flags     		= 0;
	da->object_default_interface 	= pvm_get_null_class();
	da->sys_table_id             	= -1;

	da->class_name 			= pvm_get_null_object();
        da->class_parent		= pvm_get_null_class();

        da->static_vars                 = pvm_create_object( pvm_get_array_class() );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (158, N'pvm_gc_iter_class', N'	#define gc_fcall( f, arg, o )   f( o, arg )

	struct data_area_4_class *da = (struct data_area_4_class *)&(os->da);
	gc_fcall( func, arg, da->object_default_interface );
	gc_fcall( func, arg, da->class_name );
	gc_fcall( func, arg, da->class_parent );

        gc_fcall( func, arg, da->static_vars );

        gc_fcall( func, arg, da->ip2line_maps );
        gc_fcall( func, arg, da->method_names );
        gc_fcall( func, arg, da->field_names );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (159, N'pvm_internal_init_interface', N'	memset( os->da, 0, os->_da_size );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (160, N'pvm_gc_iter_interface', N'    (void)func;
    (void)os;
    (void)arg;
    // Empty
    // Must not be called - this is not really an internal object
    panic("Interface GC iterator called");
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (161, N'pvm_internal_init_code', N'	struct data_area_4_code *      da = (struct data_area_4_code *)os->da;

	da->code_size     			= 0;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (162, N'pvm_internal_init_int', N'    (void)os;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (163, N'pvm_internal_init_string', N'	struct data_area_4_string* data_area = (struct data_area_4_string*)&(os->da);

	memset( (void *)data_area, 0, os->_da_size );
	data_area->length = 0;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (164, N'pvm_internal_init_array', N'	struct data_area_4_array *      da = (struct data_area_4_array *)os->da;

	da->used_slots     			= 0;
	da->page_size                       = 16;
	da->page.data                       = 0;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (165, N'pvm_gc_iter_array', N'	#define gc_fcall( f, arg, o )   f( o, arg )

	struct data_area_4_array *da = (struct data_area_4_array *)&(os->da);
	if(da->page.data != 0)
		gc_fcall( func, arg, da->page );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (166, N'pvm_internal_init_page', N'	assert( ((os->_da_size) % sizeof(struct pvm_object)) == 0); // Natural num of

	int n_slots = (os->_da_size) / sizeof(struct pvm_object);
	struct pvm_object * data_area = (struct pvm_object *)&(os->da);

	int i;
	for( i = 0; i < n_slots; i++ )
		data_area[i] = pvm_get_null_object();
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (167, N'pvm_gc_iter_page', N'	int n_slots = (os->_da_size) / sizeof(struct pvm_object);
	struct pvm_object * data_area = (struct pvm_object *)&(os->da);

	int i;
	for( i = 0; i < n_slots; i++ )
	{
		func( data_area[i], arg );
	}
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (168, N'pvm_internal_init_thread', N'	struct data_area_4_thread *      da = (struct data_area_4_thread *)os->da;

	hal_spin_init(&da->spin);
#if OLD_VM_SLEEP
        da->sleep_flag                      = 0;
#endif
	//hal_cond_init(&(da->wakeup_cond), "VmThrdWake");

	da->call_frame   			= pvm_create_call_frame_object();
	da->stack_depth				= 1;

	da->owner.data = 0;
	da->environment.data = 0;

	da->code.code     			= 0;
	da->code.IP 			= 0;
	da->code.IP_max             	= 0;

	//da->_this_object 			= pvm_get_null_object();

	pvm_exec_load_fast_acc(da); // Just to fill shadows with something non-null
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (169, N'pvm_gc_iter_thread', N'	#define gc_fcall( f, arg, o )   f( o, arg )

	struct data_area_4_thread *da = (struct data_area_4_thread *)&(os->da);
	gc_fcall( func, arg, da->call_frame );
	gc_fcall( func, arg, da->owner );
	gc_fcall( func, arg, da->environment );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (170, N'pvm_internal_init_call_frame', N'	//struct data_area_4_call_frame *da = pvm_data_area( os, call_frame );
	struct data_area_4_call_frame *da = (struct data_area_4_call_frame *)&(os->da);

	da->IP_max = 0;
	da->IP = 0;
	da->code = 0;

	da->this_object = pvm_get_null_object();
	da->prev = pvm_get_null_object();

	da->istack = pvm_create_istack_object();
	da->ostack = pvm_create_ostack_object();
	da->estack = pvm_create_estack_object();
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (171, N'pvm_gc_iter_call_frame', N'	#define gc_fcall( f, arg, o )   f( o, arg )

	struct data_area_4_call_frame *da = (struct data_area_4_call_frame *)&(os->da);
	gc_fcall( func, arg, da->this_object );
	gc_fcall( func, arg, da->prev ); // FYI - shall never be followed in normal situation, must contain zero data ptr if being considered by refcount
	gc_fcall( func, arg, da->istack );
	gc_fcall( func, arg, da->ostack );
	gc_fcall( func, arg, da->estack );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (172, N'pvm_internal_init_istack', N'    struct data_area_4_integer_stack* sda = (struct data_area_4_integer_stack*) os->da;

    struct pvm_object ret;

    ret.data = os;
    ret.interface = 0; // Nobody needs it there anyway

    sda->common.root = ret;
    sda->common.curr = ret;
    sda->common.prev.data = 0;
    sda->common.next.data = 0;
    sda->common.free_cell_ptr = 0;
    sda->common.__sSize = PVM_INTEGER_STACK_SIZE;
    sda->curr_da = (void *)sda;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (173, N'pvm_gc_iter_istack', N'    #define gc_fcall( f, arg, o )   f( o, arg )

    struct data_area_4_object_stack *da = (struct data_area_4_object_stack *)&(os->da);

    // No objects in the integer stack, but please visit linked list ifself

    if ( da->common.next.data != 0 )
        gc_fcall( func, arg, da->common.next );  //we are starting from root, so following next is enough. Tail recursion
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (174, N'pvm_internal_init_ostack', N'    struct data_area_4_object_stack* sda = (struct data_area_4_object_stack*) os->da;

    struct pvm_object ret;

    ret.data = os;
    ret.interface = 0; // Nobody needs it there anyway

    sda->common.root = ret;  // will update later for nonroot pages
    sda->common.curr = ret;
    sda->common.prev.data = 0;
    sda->common.next.data = 0;
    sda->common.free_cell_ptr = 0;
    sda->common.__sSize = PVM_OBJECT_STACK_SIZE;
    sda->curr_da = (void *)sda;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (175, N'pvm_gc_iter_ostack', N'    #define gc_fcall( f, arg, o )   f( o, arg )

    struct data_area_4_object_stack *da = (struct data_area_4_object_stack *)&(os->da);

    int i, max = da->common.free_cell_ptr;
    for( i = 0; i < max; i++ )
    {
        gc_fcall( func, arg, da->stack[i] );
    }

    if ( da->common.next.data != 0 )
        gc_fcall( func, arg, da->common.next );  //we are starting from root, so following next is enough. Tail recursion
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (176, N'pvm_internal_init_estack', N'    struct data_area_4_exception_stack* sda = (struct data_area_4_exception_stack*) os->da;

    struct pvm_object ret;

    ret.data = os;
    ret.interface = 0; // Nobody needs it there anyway

    sda->common.root = ret;
    sda->common.curr = ret;
    sda->common.prev.data = 0;
    sda->common.next.data = 0;
    sda->common.free_cell_ptr = 0;
    sda->common.__sSize = PVM_EXCEPTION_STACK_SIZE;
    sda->curr_da = (void *)sda;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (177, N'pvm_gc_iter_estack', N'    struct data_area_4_exception_stack *da = (struct data_area_4_exception_stack *)&(os->da);

    int i, max = da->common.free_cell_ptr;
    for( i = 0; i < max; i++ )
    {
        gc_fcall( func, arg, da->stack[i].object );
    }

    if ( da->common.next.data != 0 )
        gc_fcall( func, arg, da->common.next );  //we are starting from root, so following next is enough. Tail recursion
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (178, N'pvm_internal_init_long', N'    (void)os;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (179, N'pvm_internal_init_float', N'    (void)os;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (180, N'pvm_internal_init_double', N'    (void)os;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (181, N'pvm_internal_init_boot', N'     (void)os;
     // Nohing!
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (182, N'pvm_internal_init_tty', N' ')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (183, N'pvm_gc_finalizer_tty', N' ')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (184, N'pvm_restart_tty', N' ')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (185, N'pvm_internal_init_loader', N' ')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (186, N'pvm_internal_init_driver', N' ')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (187, N'pvm_gc_iter_driver', N' ')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (188, N'pvm_internal_init_mutex', N'    struct data_area_4_mutex *      da = (struct data_area_4_mutex *)os->da;

    da->waiting_threads_array = pvm_create_object( pvm_get_array_class() );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (189, N'pvm_internal_init_cond', N'    struct data_area_4_cond *      da = (struct data_area_4_cond *)os->da;

    da->waiting_threads_array = pvm_create_object( pvm_get_array_class() );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (190, N'pvm_internal_init_binary', N'    (void)os;
    //struct data_area_4_binary *      da = (struct data_area_4_binary *)os->da;
    // empty
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (191, N'pvm_internal_init_bitmap', N'    (void)os;
    // Nothing
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (192, N'pvm_gc_iter_bitmap', N'    #define gc_fcall( f, arg, o )   f( o, arg )

	struct data_area_4_bitmap *da = (struct data_area_4_bitmap *)&(os->da);
	if(da->image.data != 0)
		gc_fcall( func, arg, da->image );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (193, N'pvm_internal_init_world', N'    (void)os;
    //struct data_area_4_binary *      da = (struct data_area_4_binary *)os->da;
    // empty
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (194, N'pvm_internal_init_closure', N'	struct data_area_4_closure *      da = (struct data_area_4_closure *)os->da;
	da->object.data = 0;
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (195, N'pvm_gc_iter_closure', N'    struct data_area_4_closure *      da = (struct data_area_4_closure *)os->da;
    //if(da->image.object != 0)
    gc_fcall( func, arg, da->object );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (196, N'pvm_internal_init_udp', N' ')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (197, N'pvm_gc_finalizer_udp', N' ')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (198, N'pvm_internal_init_tcp', N' ')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (199, N'pvm_gc_finalizer_tcp', N' ')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (200, N'pvm_internal_init_weakref', N'    struct data_area_4_weakref *      da = (struct data_area_4_weakref *)os->da;
    da->object.data = 0;
#if WEAKREF_SPIN
    hal_spin_init( &da->lock );
#else
    hal_mutex_init( &da->mutex, "WeakRef" );
#endif
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (201, N'pvm_gc_iter_weakref', N'    struct data_area_4_weakref *      da = (struct data_area_4_weakref *)os->da;

    (void) da;
    // No! We are weak ref and do not count our reference or make GC
    // know of it so that our reference does not prevent ref''d object
    // from being GC''ed
    //gc_fcall( func, arg, da->object );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (202, N'pvm_internal_init_window', N'    struct data_area_4_window      *da = (struct data_area_4_window *)os->da;

    strlcpy( da->title, "Window", sizeof(da->title) );

    //da->w.title = da->title;
    da->fg = COLOR_BLACK;
    da->bg = COLOR_WHITE;
    da->x = 0;
    da->y = 0;

    drv_video_window_init( &(da->w), PVM_DEF_TTY_XSIZE, PVM_DEF_TTY_YSIZE, 100, 100, da->bg, WFLAG_WIN_DECORATED, da->title );


    {
    pvm_object_t o;
    o.data = os;
    o.interface = pvm_get_default_interface(os).data;

    da->connector = pvm_create_connection_object();
    struct data_area_4_connection *cda = (struct data_area_4_connection *)da->connector.data->da;

    phantom_connect_object_internal(cda, 0, o, 0);

    // This object needs OS attention at restart
    // TODO do it by class flag in create fixed or earlier?
    pvm_add_object_to_restart_list( o );
    }
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (203, N'pvm_gc_iter_window', N'    #define gc_fcall( f, arg, o )   f( o, arg )

    struct data_area_4_window *da = (struct data_area_4_window *)os->da;

    gc_fcall( func, arg, da->connector );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (204, N'pvm_gc_finalizer_window', N'    // is it called?
    struct data_area_4_window      *da = (struct data_area_4_window *)os->da;
    drv_video_window_destroy(&(da->w));
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (205, N'pvm_restart_window', N'    pvm_add_object_to_restart_list( o ); // Again!

    struct data_area_4_window *da = pvm_object_da( o, window );

    printf("restart WIN\n");

    w_restart_init( &da->w );

    da->w.title = da->title; // must be correct in snap? don''t reset?

    /*
    queue_init(&(da->w.events));
    da->w.events_count = 0;

    iw_enter_allwq( &da->w );

    //event_q_put_win( 0, 0, UI_EVENT_WIN_REPAINT, &da->w );
    ev_q_put_win( 0, 0, UI_EVENT_WIN_REDECORATE, &da->w );
    */
    w_restart_attach( &da->w );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (206, N'pvm_internal_init_directory', N'    struct data_area_4_directory      *da = (struct data_area_4_directory *)os->da;

    //da->elSize = 256;
    //da->capacity = 16;
    //da->nEntries = 0;

    //da->container = pvm_create_binary_object( da->elSize * da->capacity , 0 );

    errno_t rc = hdir_init( da, 0 );
    if( rc ) panic("can''t create directory"); // TODO do not panic? must return errno?
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (207, N'pvm_gc_iter_directory', N'    #define gc_fcall( f, arg, o )   f( o, arg )

    struct data_area_4_directory      *da = (struct data_area_4_directory *)os->da;

    gc_fcall( func, arg, da->keys );
    gc_fcall( func, arg, da->values );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (208, N'pvm_gc_finalizer_directory', N'    //struct data_area_4_window      *da = (struct data_area_4_window *)os->da;
    assert(0);
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (209, N'pvm_restart_directory', N'    //struct data_area_4_directory *da = pvm_object_da( o, directory );
    assert(0);
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (210, N'pvm_internal_init_connection', N'    struct data_area_4_connection      *da = (struct data_area_4_connection *)os->da;

    da->kernel = 0;
    memset( da->name, 0, sizeof(da->name) );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (211, N'pvm_gc_iter_connection', N'    #define gc_fcall( f, arg, o )   f( o, arg )

    struct data_area_4_connection      *da = (struct data_area_4_connection *)os->da;

    pvm_object_t ot;
    ot.interface = 0;
    ot.data = (void *) (((addr_t)da->owner)-DA_OFFSET());

    gc_fcall( func, arg, ot );
    gc_fcall( func, arg, da->p_kernel_state_object );
    gc_fcall( func, arg, da->callback );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (212, N'pvm_gc_finalizer_connection', N'    // is it called?
    struct data_area_4_connection *da = (struct data_area_4_connection *)os->da;
    //#warning disconnect!
    errno_t ret = phantom_disconnect_object( da );
    if( ret )
        printf("automatic disconnect failed - %d\n", ret );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (213, N'pvm_restart_connection', N'    struct data_area_4_connection *da = pvm_object_da( o, connection );
printf("restarting connection");
    da->kernel = 0;

    int ret = pvm_connect_object(o,0);

    if( ret )
        printf("reconnect failed %d\n", ret );

//#warning restart!
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (214, N'pvm_internal_init_sema', N'    struct data_area_4_sema *      da = (struct data_area_4_sema *)os->da;

    da->waiting_threads_array = pvm_create_object( pvm_get_array_class() );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (215, N'pvm_gc_iter_void', N'    (void)func;
    (void)os;
    (void)arg;
    // Empty
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (216, N'pvm_gc_iter_int', N'    (void)os;
    (void)arg;
    (void)func;
    // Empty
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (217, N'pvm_gc_iter_long', N'    (void)os;
    (void)arg;
    (void)func;
    // Empty
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (218, N'pvm_gc_iter_float', N'    (void)os;
    (void)arg;
    (void)func;
    // Empty
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (219, N'pvm_gc_iter_double', N'    (void)os;
    (void)arg;
    (void)func;
    // Empty
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (220, N'pvm_gc_iter_string', N'    (void)func;
    (void)os;
    (void)arg;
    // Empty
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (221, N'pvm_gc_iter_code', N'    (void)func;
    (void)os;
    (void)arg;
    // Empty
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (222, N'pvm_gc_iter_boot', N'    (void)func;
    (void)os;
    (void)arg;
    // Empty
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (223, N'pvm_gc_iter_mutex', N'   #define gc_fcall( f, arg, o )   f( o, arg )

    struct data_area_4_mutex *      da = (struct data_area_4_mutex *)os->da;
    //int i;

    gc_fcall( func, arg, da->waiting_threads_array );

    //for( i = 0; i < MAX_MUTEX_THREADS; i++ )
    //    gc_fcall( func, arg, da->waiting_threads[i] );


    gc_fcall( func, arg, pvm_da_to_object(da->owner_thread) );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (224, N'pvm_gc_iter_cond', N'    #define gc_fcall( f, arg, o )   f( o, arg )

    struct data_area_4_cond *      da = (struct data_area_4_cond *)os->da;
    //int i;

    gc_fcall( func, arg, da->waiting_threads_array );

    //for( i = 0; i < MAX_MUTEX_THREADS; i++ )
    //    gc_fcall( func, arg, da->waiting_threads[i] );

    gc_fcall( func, arg, pvm_da_to_object(da->owner_thread) );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (225, N'pvm_gc_iter_sema', N'    #define gc_fcall( f, arg, o )   f( o, arg )

    struct data_area_4_sema *      da = (struct data_area_4_sema *)os->da;
    //int i;

    gc_fcall( func, arg, da->waiting_threads_array );

    //for( i = 0; i < MAX_MUTEX_THREADS; i++ )
    //    gc_fcall( func, arg, da->waiting_threads[i] );

    gc_fcall( func, arg, pvm_da_to_object(da->owner_thread) );
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (226, N'pvm_gc_iter_binary', N'    (void)func;
    (void)os;
    (void)arg;
    // Empty
')
INSERT INTO [dbo].[SyscallFunctions] ([Id], [Function], [Code]) VALUES (228, N'pvm_gc_iter_world', N'    (void)func;
    (void)os;
    (void)arg;
    // Empty
')
SET IDENTITY_INSERT [dbo].[SyscallFunctions] OFF
