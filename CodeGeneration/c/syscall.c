﻿#include "syscall.h"
// for pvm_ostack_push(), pvm_ostack_pop(), pvm_istack_pop()
#include "stacks.h"
// for pvm_create_null_object(), pvm_create_string_object(), and others in clone methods
#include "create.h"
// for pvm_get_int and others
#include "object_tools.h" // should be extracted from "internal_da.h"
// for ref_dec_o() 
#include "gc_tools.h" // should be extracted from "gc.h" and "gc.c"
// for printf()
#include <stdio.h>

//	Default syscalls.
//
//	Any class with internal implementation will present at least:
//	sys 0:	Construct. No args.
//	sys 1:	Destruct. No args.
//	sys 2:	GetClass. No args. Returns class object.
//	sys 3:	clone. No args. Returns copy of this, if possible.
//	sys 4:	equals. arg is object. Compares by value.
//	sys 5:	ToString. No args, returns string object, representing
//			contents of this object.
//	sys 6:  ToXML. Returns string object, representing
//			contents of this object in XML form.
//	sys 7:	fromXML.
//	sys 8:	default activity. depends on class
//	sys 9:	secondary activity, depends on class
//	sys 10:	third activity
//	sys 11:	fourth activity
//	sys 15:	int hashCode - returns int

// --------------------------------------------------------------------------
// Macros for syscall bodies
// --------------------------------------------------------------------------


// push on object stack
#define SYSCALL_RETURN(obj) do { pvm_ostack_push( tc->_ostack, obj ); return 1; } while(0)
//#define SYSCALL_THROW(obj) do { pvm_ostack_push( tc->_ostack, obj ); return 0; } while(0)
#define SYSCALL_THROW(obj) ({ pvm_ostack_push( tc->_ostack, obj ); return 0; })
#define SYSCALL_RETURN_NOTHING SYSCALL_RETURN( pvm_create_null_object() )

#define SYSCALL_THROW_STRING(str) SYSCALL_THROW(pvm_create_string_object( str ));

#define POP_ARG ( pvm_ostack_pop( tc->_ostack ))
#define POP_ISTACK ( pvm_istack_pop( tc->_istack ))

//#define DECLARE_SIZE(class) int n_syscall_table_4_##class =	(sizeof syscall_table_4_##class) / sizeof(syscall_func_t)
//#define DECLARE_SIZE(class)

#define CHECK_PARAM_COUNT(n_param, must_have) \
    do { \
    if( n_param < must_have ) \
    	SYSCALL_THROW(pvm_create_string_object( "sys: need more parameters" )); \
    } while(0)

//             SYSCALL_THROW_STRING( /*"not a string arg: "*/  __func__ );
// TODO it does not SYS_FREE_O(obj)!
#define ASSERT_STRING(obj) \
    do { \
	if( !IS_PHANTOM_STRING(obj) ) \
        SYSCALL_THROW_STRING( "not a string arg: " __FILE__ ":" __XSTRING(__LINE__) ); \
    } while(0)


#define ASSERT_INT(obj) \
    do { \
	if( !IS_PHANTOM_INT(obj) ) \
        SYSCALL_THROW_STRING("not an integer arg: " __FILE__ ":" __XSTRING(__LINE__)  ); \
    } while(0)

#define DEBUG_INFO \
if( debug_print) printf("\n\n --- syscall %s at %s line %d called ---\n\n", __func__, __FILE__, __LINE__ )

// --------------------------------------------------------------------------
// Int/string parameters shortcuts
// --------------------------------------------------------------------------

#define SYS_FREE_O(o) ref_dec_o(o)


// Depends on GCC {} value extension

#define POP_INT() \
    ({ \
    struct pvm_object __ival = POP_ARG; \
    ASSERT_INT(__ival); \
    int v = pvm_get_int(__ival); \
    SYS_FREE_O(__ival); \
    v; \
    })

/* can't be used since string has to be SYS_FREE_'ed
#define POP_STRING() \
    ({ \
    struct pvm_object __sval = POP_ARG; \
    ASSERT_STRING(__sval); \
    pvm_object_da( __sval, string ); \
    })
*/


// --------------------------------------------------------------------------
// Thread sleep/wakeup
// --------------------------------------------------------------------------

#define SYSCALL_PUT_THIS_THREAD_ASLEEP(__unl_spin) phantom_thread_put_asleep( tc, (__unl_spin) )
#define SYSCALL_WAKE_THIS_THREAD_UP() phantom_thread_wake_up( tc )

#define SYSCALL_PUT_THREAD_ASLEEP(__thread, __unl_spin) phantom_thread_put_asleep( (__thread), (__unl_spin) )
#define SYSCALL_WAKE_THREAD_UP(__thread) phantom_thread_wake_up( (__thread) )

// --------------------------------------------------------------------------
// Types and syscall table declaration macro
// --------------------------------------------------------------------------

struct pvm_object;
struct data_area_4_thread;

/*
#define DECLARE_SYSTABLE(type,tab_id) \
    extern syscall_func_t syscall_table_4_##type[]; extern int n_syscall_table_4_##type; \
    void pvm_syscall_init_##tab_id() { pvm_exec_systables[tab_id] = (syscall_table_4_##type); }
*/

// --------- invalid method stub --------------------------------------------

int invalid_syscall(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
    printf("invalid syscal for object: "); dumpo( (addr_t)(o.data) );//pvm_object_print( o ); printf("\n");
    //printf("invalid value's class: "); pvm_object_print( o.data->_class); printf("\n");
        SYSCALL_THROW_STRING( "invalid syscall called" );
}

// -------- syscall function (methods) for NULL class. --------


int si_void_0_construct(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)oo;
        //(void)tc;
    
        DEBUG_INFO;
        SYSCALL_RETURN_NOTHING;
}

int si_void_1_destruct(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)o;
        //(void)tc;
    
        DEBUG_INFO;
        SYSCALL_RETURN_NOTHING;
}

int si_void_2_class(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        //ref_inc_o( this_obj.data->_class );  //increment if class is refcounted
        SYSCALL_RETURN(this_obj.data->_class);
}

int si_void_3_clone(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)o;
        DEBUG_INFO;
        SYSCALL_THROW_STRING( "void clone called" );
}

int si_void_4_equals(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
        struct pvm_object him = POP_ARG;
    
        int ret = (me.data == him.data);
    
        SYS_FREE_O(him);
    
        SYSCALL_RETURN(pvm_create_int_object( ret ) );
}

int si_void_5_tostring(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)o;
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_string_object( "(void)" ));
}

int si_void_6_toXML(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)o;
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_string_object( "<void>" ));
}

int si_void_7_fromXML(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)o;
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_string_object( "(void)" ));
}

int si_void_8_def_op_1(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)o;
        DEBUG_INFO;
        SYSCALL_THROW_STRING( "void default op 1 called" );
}

int si_void_9_def_op_2(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)o;
        DEBUG_INFO;
        SYSCALL_THROW_STRING( "void default op 2 called" );
}

int si_void_15_hashcode(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        size_t os = me.data->_da_size;
        void *oa = me.data->da;
    
        //SYSCALL_RETURN(pvm_create_int_object( ((addr_t)me.data)^0x3685A634^((addr_t)&si_void_15_hashcode) ));
        SYSCALL_RETURN(pvm_create_int_object( calc_hash( oa, oa+os) ));
}

syscall_func_t syscall_table_4_void[] =
{
    si_void_0_construct, si_void_1_destruct, 
    si_void_2_class, si_void_3_clone, 
    si_void_4_equals, si_void_5_tostring, 
    si_void_6_toXML, si_void_7_fromXML, 
    si_void_8_def_op_1, si_void_9_def_op_2, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, si_void_15_hashcode, 
};

int n_syscall_table_4_void = 16;

// -------- syscall function (methods) for CLASS class. --------


int si_class_class_5_tostring(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)me;
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_string_object( "class" ));
}

int si_class_class_8_new_class(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)me;
        DEBUG_INFO;
    
        int n_param = POP_ISTACK;
    
        CHECK_PARAM_COUNT(n_param, 3);
    
        struct pvm_object class_name = POP_ARG;
        int n_object_slots = POP_INT();
        struct pvm_object iface = POP_ARG;
    
        ASSERT_STRING(class_name);
    
        struct pvm_object new_class = pvm_create_class_object(class_name, iface, sizeof(struct pvm_object) * n_object_slots);
    
        //SYS_FREE_O(class_name);  //linked in class object
        //SYS_FREE_O(iface);  //linked in class object
    
        SYSCALL_RETURN( new_class );
}

int si_class_10_set_static(struct pvm_object o, struct data_area_4_thread *tc)
{
        struct data_area_4_class *meda = pvm_object_da( me, class );
    
        DEBUG_INFO;
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 2);
    
        struct pvm_object static_val = POP_ARG;
        int n_slot = POP_INT();
    
        pvm_set_ofield( meda->static_vars, n_slot, static_val );
    
        SYSCALL_RETURN_NOTHING;
}

int si_class_11_get_static(struct pvm_object o, struct data_area_4_thread *tc)
{
        struct data_area_4_class *meda = pvm_object_da( me, class );
        DEBUG_INFO;
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 2);
    
        int n_slot = POP_INT();
    
        pvm_object_t ret = pvm_get_ofield( meda->static_vars, n_slot );
        ref_inc_o( ret );
        SYSCALL_RETURN( ret );
}

int si_class_14_instanceof(struct pvm_object o, struct data_area_4_thread *tc)
{
        //struct data_area_4_class *meda = pvm_object_da( me, class );
        DEBUG_INFO;
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
        struct pvm_object instance = POP_ARG;
    #if VM_INSTOF_RECURSIVE
        int is = pvm_object_class_is_or_child( instance, me );
    #else
        int is = pvm_object_class_exactly_is( instance, me );
    #endif // VM_INSTOF_RECURSIVE
        SYS_FREE_O(instance);
    
        SYSCALL_RETURN(pvm_create_int_object( is ));
}

syscall_func_t syscall_table_4_class[] =
{
    si_void_0_construct, si_void_1_destruct, 
    si_void_2_class, si_void_3_clone, 
    si_void_4_equals, si_class_class_5_tostring, 
    si_void_6_toXML, si_void_7_fromXML, 
    si_class_class_8_new_class, si_void_9_def_op_2, 
    si_class_10_set_static, si_class_11_get_static, 
    invalid_syscall, invalid_syscall, 
    si_class_14_instanceof, si_void_15_hashcode, 
};

int n_syscall_table_4_class = 16;

// -------- syscall function (methods) for INTERFACE class. --------


int si_interface_5_tostring(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)me;
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_string_object( "interface" ));
}

syscall_func_t syscall_table_4_interface[] =
{
    si_void_0_construct, si_void_1_destruct, 
    si_void_2_class, si_void_3_clone, 
    si_void_4_equals, si_interface_5_tostring, 
    si_void_6_toXML, si_void_7_fromXML, 
    si_void_8_def_op_1, si_void_9_def_op_2, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, si_void_15_hashcode, 
};

int n_syscall_table_4_interface = 16;

// -------- syscall function (methods) for CODE class. --------


int si_code_5_tostring(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)me;
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_string_object( "code" ));
}

syscall_func_t syscall_table_4_code[] =
{
    si_void_0_construct, si_void_1_destruct, 
    si_void_2_class, si_void_3_clone, 
    si_void_4_equals, si_code_5_tostring, 
    si_void_6_toXML, si_void_7_fromXML, 
    si_void_8_def_op_1, si_void_9_def_op_2, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, si_void_15_hashcode, 
};

int n_syscall_table_4_code = 16;

// -------- syscall function (methods) for INT class. --------


int si_int_3_clone(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_int_object( pvm_get_int(me) ));
}

int si_int_4_equals(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
        struct pvm_object him = POP_ARG;
    
        int same_class = me.data->_class.data == him.data->_class.data;
        int same_value = pvm_get_int(me) == pvm_get_int(him);
    
        SYS_FREE_O(him);
    
        SYSCALL_RETURN(pvm_create_int_object( same_class && same_value));
}

int si_int_5_tostring(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        char buf[32];
        snprintf( buf, sizeof(buf), "%d", pvm_get_int(me) );
        SYSCALL_RETURN(pvm_create_string_object( buf ));
}

int si_int_6_toXML(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        char buf[32];
        snprintf( buf, 31, "%d", pvm_get_int(me) );
    	//SYSCALL_RETURN(pvm_create_string_object( "<void>" ));
        SYSCALL_THROW_STRING( "int toXML called" );
}

syscall_func_t syscall_table_4_int[] =
{
    si_void_0_construct, si_void_1_destruct, 
    si_void_2_class, si_int_3_clone, 
    si_int_4_equals, si_int_5_tostring, 
    si_int_6_toXML, si_void_7_fromXML, 
    si_void_8_def_op_1, si_void_9_def_op_2, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, si_void_15_hashcode, 
};

int n_syscall_table_4_int = 16;

// -------- syscall function (methods) for STRING class. --------


int si_string_3_clone(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        ASSERT_STRING(me);
        struct data_area_4_string *meda = pvm_object_da( me, string );
        SYSCALL_RETURN(pvm_create_string_object_binary( (char *)meda->data, meda->length ));
}

int si_string_4_equals(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
        struct pvm_object him = POP_ARG;
    
        int ret = 0;
        if( !pvm_is_null(him) )
        {
            ASSERT_STRING(him);
    
            struct data_area_4_string *meda = pvm_object_da( me, string );
            struct data_area_4_string *himda = pvm_object_da( him, string );
    
            ret =
                me.data->_class.data == him.data->_class.data &&
                meda->length == himda->length &&
                0 == strncmp( (const char*)meda->data, (const char*)himda->data, meda->length )
                ;
        }
        SYS_FREE_O(him);
    
        // BUG - can compare just same classes
        SYSCALL_RETURN(pvm_create_int_object( ret ) );
}

int si_string_5_tostring(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        SYSCALL_RETURN(me);
}

int si_string_8_substring(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        ASSERT_STRING(me);
        struct data_area_4_string *meda = pvm_object_da( me, string );
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 2);
    
        int parmlen = POP_INT();
        int index = POP_INT();
    
    
        if( index < 0 || index >= meda->length )
            SYSCALL_THROW_STRING( "string.substring index is out of bounds" );
    
        int len = meda->length - index;
        if( parmlen < len ) len = parmlen;
    
        if( len < 0 )
            SYSCALL_THROW_STRING( "string.substring length is negative" );
    
    
        //printf("substr inx %x len %d parmlen %d\n", index, len, parmlen);
    
        SYSCALL_RETURN(pvm_create_string_object_binary( (char *)meda->data + index, len ));
}

int si_string_9_charat(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        ASSERT_STRING(me);
        struct data_area_4_string *meda = pvm_object_da( me, string );
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
        int index = POP_INT();
    
    
        int len = meda->length;
    
        if(index > len-1 )
            SYSCALL_THROW_STRING( "string.charAt index is out of bounds" );
    
        SYSCALL_RETURN(pvm_create_int_object( meda->data[index]  ));
}

int si_string_10_concat(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
        struct pvm_object him = POP_ARG;
        ASSERT_STRING(him);
    
        struct data_area_4_string *meda = pvm_object_da( me, string );
        struct data_area_4_string *himda = pvm_object_da( him, string );
    
        pvm_object_t ret = pvm_create_string_object_binary_cat(
        		(char *)meda->data, meda->length,
                    (char *)himda->data, himda->length );
    
        SYS_FREE_O(him);
    
        SYSCALL_RETURN( ret );
}

int si_string_11_length(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        ASSERT_STRING(me);
        struct data_area_4_string *meda = pvm_object_da( me, string );
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 0);
    
        SYSCALL_RETURN(pvm_create_int_object( meda->length ));
}

int si_string_12_find(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
        struct pvm_object him = POP_ARG;
        ASSERT_STRING(him);
    
        struct data_area_4_string *meda = pvm_object_da( me, string );
        struct data_area_4_string *himda = pvm_object_da( him, string );
    
        unsigned char * ret = (unsigned char *)strnstrn(
        		(char *)meda->data, meda->length,
                    (char *)himda->data, himda->length );
    
        SYS_FREE_O(him);
    
        int pos = -1;
    
        if( ret != 0 )
            pos = ret - (meda->data);
    
        SYSCALL_RETURN(pvm_create_int_object( pos ));
}

syscall_func_t syscall_table_4_string[] =
{
    si_void_0_construct, si_void_1_destruct, 
    si_void_2_class, si_string_3_clone, 
    si_string_4_equals, si_string_5_tostring, 
    si_void_6_toXML, si_void_7_fromXML, 
    si_string_8_substring, si_string_9_charat, 
    si_string_10_concat, si_string_11_length, 
    si_string_12_find, invalid_syscall, 
    invalid_syscall, si_void_15_hashcode, 
};

int n_syscall_table_4_string = 16;

// -------- syscall function (methods) for ARRAY class. --------


int si_array_5_tostring(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)me;
        DEBUG_INFO;
        // BUG? Recursively call tostring for all of them?
        SYSCALL_RETURN(pvm_create_string_object( "array" ));
}

int si_array_8_get_iterator(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)me;
        DEBUG_INFO;
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 0);
        SYSCALL_THROW_STRING( "get iterator is not implemented yet" );
}

int si_array_9_get_subarray(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)me;
        DEBUG_INFO;
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 2);
    
        /*
        int len = POP_INT();
        int base = POP_INT();
        SYSCALL_RETURN();
        */
    
        SYSCALL_THROW_STRING( "get subarray is not implemented yet" );
}

int si_array_10_get(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
        unsigned int index = POP_INT();
    
        struct data_area_4_array *da = (struct data_area_4_array *)me.data->da;
    
        if( index >= da->used_slots )
            SYSCALL_THROW_STRING( "array get - index is out of bounds" );
    
        struct pvm_object o = pvm_get_ofield( da->page, index);
        SYSCALL_RETURN( ref_inc_o( o ) );
}

int si_array_11_set(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 2);
    
        int index = POP_INT();
    
        struct pvm_object value = POP_ARG;
    
        pvm_set_array_ofield( me.data, index, value );
    
        // we increment refcount and return object back.
        // it will possibly be dropped and refcount will decrement again then.
        SYSCALL_RETURN( ref_inc_o( value ) );
}

int si_array_12_size(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 0);
    
        struct data_area_4_array *da = (struct data_area_4_array *)me.data->da;
    
        SYSCALL_RETURN(pvm_create_int_object( da->used_slots ) );
}

syscall_func_t syscall_table_4_array[] =
{
    si_void_0_construct, si_void_1_destruct, 
    si_void_2_class, si_void_3_clone, 
    si_void_4_equals, si_array_5_tostring, 
    si_void_6_toXML, si_void_7_fromXML, 
    si_array_8_get_iterator, si_array_9_get_subarray, 
    si_array_10_get, si_array_11_set, 
    si_array_12_size, invalid_syscall, 
    invalid_syscall, si_void_15_hashcode, 
};

int n_syscall_table_4_array = 16;

// -------- syscall function (methods) for PAGE class. --------


int si_page_5_tostring(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)me;
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_string_object( "page" ));
}

syscall_func_t syscall_table_4_page[] =
{
    si_void_0_construct, si_void_1_destruct, 
    si_void_2_class, si_void_3_clone, 
    si_void_4_equals, si_page_5_tostring, 
    si_void_6_toXML, si_void_7_fromXML, 
    si_void_8_def_op_1, si_void_9_def_op_2, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, si_void_15_hashcode, 
};

int n_syscall_table_4_page = 16;

// -------- syscall function (methods) for THREAD class. --------


int si_thread_5_tostring(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)me;
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_string_object( "thread" ));
}

int si_thread_10_pause(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_thread *meda = pvm_object_da( me, thread );
    
        if(meda != tc)
        	SYSCALL_THROW_STRING("Thread can pause itself only");
    
    #if OLD_VM_SLEEP
        SYSCALL_PUT_THIS_THREAD_ASLEEP(0);
    #else
        SYSCALL_THROW_STRING("Not this way");
    #endif
    
        SYSCALL_RETURN_NOTHING;
}

int si_thread_11_continue(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
    #if OLD_VM_SLEEP
        struct data_area_4_thread *meda = pvm_object_da( me, thread );
    
        //hal_spin_lock(&meda->spin);
        if( !meda->sleep_flag )
        	SYSCALL_THROW_STRING("Thread is not sleeping in continue");
        //hal_spin_unlock(&meda->spin);
    
        SYSCALL_WAKE_THREAD_UP(meda);
    #else
        SYSCALL_THROW_STRING("Not this way");
    #endif
    
        SYSCALL_RETURN_NOTHING;
}

int si_thread_12_getEnvironment(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)me;
        DEBUG_INFO;
        struct data_area_4_thread *meda = pvm_object_da( me, thread );
    
        if( pvm_is_null(meda->environment) )
        {
            struct pvm_object env = pvm_create_string_object(".phantom.environment");
            struct pvm_object cl = pvm_exec_lookup_class_by_name( env );
            meda->environment = pvm_create_object(cl);
            ref_dec_o(env);
            //ref_dec_o(cl);  // object keep class ref
        }
    
        SYSCALL_RETURN(meda->environment);
}

int si_thread_13_getUser(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)me;
        DEBUG_INFO;
        struct data_area_4_thread *meda = pvm_object_da( me, thread );
    
        SYSCALL_RETURN(meda->owner);
}

int si_thread_14_getOsInterface(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)me;
        DEBUG_INFO;
        struct pvm_object_storage *root = get_root_object_storage();
        struct pvm_object o = pvm_get_field( root, PVM_ROOT_OBJECT_OS_ENTRY );
        SYSCALL_RETURN( ref_inc_o( o ) );
}

syscall_func_t syscall_table_4_thread[] =
{
    si_void_0_construct, si_void_1_destruct, 
    si_void_2_class, si_void_3_clone, 
    si_void_4_equals, si_thread_5_tostring, 
    si_void_6_toXML, si_void_7_fromXML, 
    si_void_8_def_op_1, si_void_9_def_op_2, 
    si_thread_10_pause, si_thread_11_continue, 
    si_thread_12_getEnvironment, si_thread_13_getUser, 
    si_thread_14_getOsInterface, si_void_15_hashcode, 
};

int n_syscall_table_4_thread = 16;

// -------- syscall function (methods) for CALL_FRAME class. --------


int si_call_frame_5_tostring(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_string_object( "call_frame" ));
}

syscall_func_t syscall_table_4_call_frame[] =
{
    si_void_0_construct, si_void_1_destruct, 
    si_void_2_class, si_void_3_clone, 
    si_void_4_equals, si_call_frame_5_tostring, 
    si_void_6_toXML, si_void_7_fromXML, 
    si_void_8_def_op_1, si_void_9_def_op_2, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, si_void_15_hashcode, 
};

int n_syscall_table_4_call_frame = 16;

// -------- syscall function (methods) for ISTACK class. --------


int si_istack_5_tostring(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)me;
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_string_object( "istack" ));
}

syscall_func_t syscall_table_4_istack[] =
{
    si_void_0_construct, si_void_1_destruct, 
    si_void_2_class, si_void_3_clone, 
    si_void_4_equals, si_istack_5_tostring, 
    si_void_6_toXML, si_void_7_fromXML, 
    si_void_8_def_op_1, si_void_9_def_op_2, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, si_void_15_hashcode, 
};

int n_syscall_table_4_istack = 16;

// -------- syscall function (methods) for OSTACK class. --------


int si_ostack_5_tostring(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)me;
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_string_object( "ostack" ));
}

syscall_func_t syscall_table_4_ostack[] =
{
    si_void_0_construct, si_void_1_destruct, 
    si_void_2_class, si_void_3_clone, 
    si_void_4_equals, si_ostack_5_tostring, 
    si_void_6_toXML, si_void_7_fromXML, 
    si_void_8_def_op_1, si_void_9_def_op_2, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, si_void_15_hashcode, 
};

int n_syscall_table_4_ostack = 16;

// -------- syscall function (methods) for ESTACK class. --------


int si_estack_5_tostring(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)me;
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_string_object( "estack" ));
}

syscall_func_t syscall_table_4_estack[] =
{
    si_void_0_construct, si_void_1_destruct, 
    si_void_2_class, si_void_3_clone, 
    si_void_4_equals, si_estack_5_tostring, 
    si_void_6_toXML, si_void_7_fromXML, 
    si_void_8_def_op_1, si_void_9_def_op_2, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, si_void_15_hashcode, 
};

int n_syscall_table_4_estack = 16;

// -------- syscall function (methods) for LONG class. --------


int si_long_3_clone(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_long_object( pvm_get_long(me) ));
}

int si_long_4_equals(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
        struct pvm_object him = POP_ARG;
    
        int same_class = me.data->_class.data == him.data->_class.data;
        int same_value = pvm_get_long(me) == pvm_get_long(him);
    
        SYS_FREE_O(him);
    
        SYSCALL_RETURN(pvm_create_int_object( same_class && same_value));
}

int si_long_5_tostring(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        char buf[100];
        snprintf( buf, sizeof(buf), "%Ld", pvm_get_long(me) ); // TODO right size?
        SYSCALL_RETURN(pvm_create_string_object( buf ));
}

int si_long_6_toXML(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        char buf[100];
        snprintf( buf, sizeof(buf), "%Ld", pvm_get_long(me) );
    	//SYSCALL_RETURN(pvm_create_string_object( "<void>" ));
        SYSCALL_THROW_STRING( "int toXML called" );
}

syscall_func_t syscall_table_4_long[] =
{
    si_void_0_construct, si_void_1_destruct, 
    si_void_2_class, si_long_3_clone, 
    si_long_4_equals, si_long_5_tostring, 
    si_long_6_toXML, si_void_7_fromXML, 
    si_void_8_def_op_1, si_void_9_def_op_2, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, si_void_15_hashcode, 
};

int n_syscall_table_4_long = 16;

// -------- syscall function (methods) for FLOAT class. --------


int si_float_3_clone(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_float_object( pvm_get_float(me) ));
}

int si_float_4_equals(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
        struct pvm_object him = POP_ARG;
    
        int same_class = me.data->_class.data == him.data->_class.data;
        int same_value = pvm_get_float(me) == pvm_get_float(him);
    
        SYS_FREE_O(him);
    
        SYSCALL_RETURN(pvm_create_int_object( same_class && same_value));
}

int si_float_5_tostring(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        char buf[100];
        snprintf( buf, sizeof(buf), "%f", pvm_get_float(me) ); // TODO right size?
        SYSCALL_RETURN(pvm_create_string_object( buf ));
}

int si_float_6_toXML(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        char buf[100];
        snprintf( buf, sizeof(buf), "<float>%f</float>", pvm_get_float(me) );
    	SYSCALL_RETURN(pvm_create_string_object( buf ));
        //SYSCALL_THROW_STRING( "int toXML called" );
}

syscall_func_t syscall_table_4_float[] =
{
    si_void_0_construct, si_void_1_destruct, 
    si_void_2_class, si_float_3_clone, 
    si_float_4_equals, si_float_5_tostring, 
    si_float_6_toXML, si_void_7_fromXML, 
    si_void_8_def_op_1, si_void_9_def_op_2, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, si_void_15_hashcode, 
};

int n_syscall_table_4_float = 16;

// -------- syscall function (methods) for DOUBLE class. --------


int si_double_3_clone(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_double_object( pvm_get_double(me) ));
}

int si_double_4_equals(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
        struct pvm_object him = POP_ARG;
    
        int same_class = me.data->_class.data == him.data->_class.data;
        int same_value = pvm_get_double(me) == pvm_get_double(him);
    
        SYS_FREE_O(him);
    
        SYSCALL_RETURN(pvm_create_int_object( same_class && same_value));
}

int si_double_5_tostring(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        char buf[100];
        snprintf( buf, sizeof(buf), "%f", pvm_get_double(me) ); // TODO right size?
        SYSCALL_RETURN(pvm_create_string_object( buf ));
}

int si_double_6_toXML(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        char buf[100];
        snprintf( buf, sizeof(buf), "<double>%f</double>", pvm_get_double(me) );
    	SYSCALL_RETURN(pvm_create_string_object( buf ));
        //SYSCALL_THROW_STRING( "int toXML called" );
}

syscall_func_t syscall_table_4_double[] =
{
    si_void_0_construct, si_void_1_destruct, 
    si_void_2_class, si_double_3_clone, 
    si_double_4_equals, si_double_5_tostring, 
    si_double_6_toXML, si_void_7_fromXML, 
    si_void_8_def_op_1, si_void_9_def_op_2, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, si_void_15_hashcode, 
};

int n_syscall_table_4_double = 16;

// -------- syscall function (methods) for BOOT class. --------


int si_bootstrap_5_tostring(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)me;
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_string_object( "bootstrap" ));
}

int si_bootstrap_8_load_class(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)me;
        DEBUG_INFO;
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
        const int bufs = 1024;
        char buf[bufs+1];
    
        {
        struct pvm_object name = POP_ARG;
        ASSERT_STRING(name);
    
        struct data_area_4_string *nameda = pvm_object_da( name, string );
    
    
        int len = nameda->length > bufs ? bufs : nameda->length;
        memcpy( buf, nameda->data, len );
        buf[len] = '\0';
    
        SYS_FREE_O(name);
        }
    
        // BUG! Need some diagnostics from loader here
    
        struct pvm_object new_class;
    
        if( pvm_load_class_from_module(buf, &new_class))
        {
            const char *msg = " - class load error";
            if( strlen(buf) >= bufs - 2 - strlen(msg) )
            {
                SYSCALL_THROW_STRING( msg+3 );
            }
            else
            {
                strcat( buf, msg );
                SYSCALL_THROW_STRING( buf );
            }
        }
        else
        {
        	SYSCALL_RETURN(new_class);
        }
}

int si_bootstrap_16_print(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)me;
        DEBUG_INFO;
    
        int n_param = POP_ISTACK;
    
        while( n_param-- )
            {
        	struct pvm_object o = POP_ARG;
            pvm_object_print( o );
            SYS_FREE_O( o );
            }
    
        SYSCALL_RETURN_NOTHING;
}

int si_bootstrap_17_register_class_loader(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)me;
        DEBUG_INFO;
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
        struct pvm_object loader = POP_ARG;
    
        pvm_root.class_loader = loader;
        pvm_object_storage_t *root = get_root_object_storage();
        pvm_set_field( root, PVM_ROOT_OBJECT_CLASS_LOADER, pvm_root.class_loader );
    
        // Don't need do SYS_FREE_O(loader) since we store it
    
        SYSCALL_RETURN_NOTHING;
}

int si_bootstrap_18_thread(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
        struct pvm_object object = POP_ARG;
    
        // Don't need do SYS_FREE_O(object) since we store it as 'this'
    
    #if 1
        // TODO check object class to be runnable or subclass
    
        {
        struct pvm_object new_cf = pvm_create_call_frame_object();
        struct data_area_4_call_frame* cfda = pvm_object_da( new_cf, call_frame );
    
        pvm_ostack_push( pvm_object_da(cfda->ostack, object_stack), me );
        pvm_istack_push( pvm_object_da(cfda->istack, integer_stack), 1); // pass him real number of parameters
    
        struct pvm_object_storage *code = pvm_exec_find_method( object, 8 );
        pvm_exec_set_cs( cfda, code );
        cfda->this_object = object;
    
        struct pvm_object thread = pvm_create_thread_object( new_cf );
    
        //printf("here?\n");
    
        phantom_activate_thread(thread);
        }
    #endif
    
    
        SYSCALL_RETURN_NOTHING;
}

int si_bootstrap_19_create_binary(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)me;
        DEBUG_INFO;
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
        int nbytes = POP_INT();
    
        SYSCALL_RETURN( pvm_create_binary_object(nbytes, NULL) );
}

int si_bootstrap_20_set_screen_background(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)me;
        DEBUG_INFO;
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
        struct pvm_object _bmp = POP_ARG;
    
    #if !BACK_WIN
        if( drv_video_bmpblt(_bmp,0,0,0) )
        	SYSCALL_THROW_STRING( "not a bitmap" );
    
    
        drv_video_window_repaint_all();
    #else
        // TODO black screen :(
    
        if( !pvm_object_class_exactly_is( _bmp, pvm_get_bitmap_class() ) )
        	SYSCALL_THROW_STRING( "not a bitmap" );
    
        struct data_area_4_bitmap *bmp = pvm_object_da( _bmp, bitmap );
        struct data_area_4_binary *bin = pvm_object_da( bmp->image, binary );
    
    
    #if !VIDEO_NEW_BG_WIN
        if(back_win == 0)
        	back_win = drv_video_window_create( scr_get_xsize(), scr_get_ysize(), 0, 0, COLOR_BLACK, "Background", WFLAG_WIN_DECORATED );
    
        back_win->flags &= ~WFLAG_WIN_DECORATED;
        back_win->flags |= WFLAG_WIN_NOFOCUS;
    
        w_to_bottom(back_win);
    
        //drv_video_bitblt( (void *)bin->data, 0, 0, bmp->xsize, bmp->ysize, (zbuf_t)zpos );
    #else
        window_handle_t back_win = w_get_bg_window();
    #endif
    
        bitmap2bitmap(
        		back_win->w_pixel, back_win->xsize, back_win->ysize, 0, 0,
                         (void *)bin->data, bmp->xsize, bmp->ysize, 0, 0,
                         bmp->xsize, bmp->ysize
                        );
    
        //drv_video_winblt(back_win);
        w_update( back_win );
        //scr_repaint_all();
    #endif
        // Remove it if will store bmp!
        SYS_FREE_O(_bmp);
    
        SYSCALL_RETURN_NOTHING;
}

int si_bootstrap_21_sleep(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)me;
        DEBUG_INFO;
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
        int msec = POP_INT();
    #if OLD_VM_SLEEP
        phantom_wakeup_after_msec(msec,tc);
    
        //#warning to kill
        SHOW_ERROR0( 0, "si_bootstrap_21_sleep used" );
    
        if(phantom_is_a_real_kernel())
            SYSCALL_PUT_THIS_THREAD_ASLEEP(0);
    #else
        (void) msec;
        SHOW_ERROR0( 0, "si_bootstrap_21_sleep used" );
    #endif
        SYSCALL_RETURN_NOTHING;
}

int si_bootstrap_22_set_os_interface(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)me;
        DEBUG_INFO;
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
        pvm_root.os_entry = POP_ARG;
        ref_saturate_o(pvm_root.os_entry); // make sure refcount is disabled for this object
        struct pvm_object_storage *root = get_root_object_storage();
        pvm_set_field( root, PVM_ROOT_OBJECT_OS_ENTRY, pvm_root.os_entry );
        // No ref dec - we store it.
    
        SYSCALL_RETURN_NOTHING;
}

int si_bootstrap_23_getenv(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)me;
        DEBUG_INFO;
        SYSCALL_RETURN( ref_inc_o( pvm_root.kernel_environment ) );
}

syscall_func_t syscall_table_4_boot[] =
{
    si_void_0_construct, si_void_1_destruct, 
    si_void_2_class, si_void_3_clone, 
    si_void_4_equals, si_bootstrap_5_tostring, 
    si_void_6_toXML, si_void_7_fromXML, 
    si_bootstrap_8_load_class, invalid_syscall, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, si_void_15_hashcode, 
    si_bootstrap_16_print, si_bootstrap_17_register_class_loader, 
    si_bootstrap_18_thread, si_bootstrap_19_create_binary, 
    si_bootstrap_20_set_screen_background, si_bootstrap_21_sleep, 
    si_bootstrap_22_set_os_interface, si_bootstrap_23_getenv, 
};

int n_syscall_table_4_boot = 24;

// -------- syscall function (methods) for TTY class. --------


int tostring_5(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void) me;
        DEBUG_INFO;
        SYSCALL_RETURN( pvm_create_string_object( "tty window" ));
}

int getwc_16(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void) me;
        DEBUG_INFO;
        char c[1];
    
        // TODO XXX syscall blocks!
        c[0] = phantom_dev_keyboard_getc();
    
        SYSCALL_RETURN( pvm_create_string_object_binary( c, 1 ));
}

int putws_17(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
    
        struct data_area_4_tty      *da = pvm_data_area( me, tty );
    
        //printf("putws font %d,%d\n", da->font_width, da->font_height );
    
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
        struct pvm_object _text = POP_ARG;
        ASSERT_STRING(_text);
    
        int len = pvm_get_str_len( _text );
        const char * data = (const char *)pvm_get_str_data(_text);
    
        char buf[BS+2];
    
        if( len > BS ) len = BS;
        strncpy( buf, data, len );
        //buf[len] = '\n';
        buf[len] = 0;
    
        SYS_FREE_O(_text);
    
        //printf("tty print: '%s' at %d,%d\n", buf, da->x, da->y );
    
        struct rgba_t fg = da->fg;
        struct rgba_t bg = da->bg;
    
        w_font_tty_string( &(da->w), tty_font, buf, fg, bg, &(da->x), &(da->y) );
        w_update( &(da->w) );
    
        SYSCALL_RETURN_NOTHING;
}

int debug_18(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void) me;
        DEBUG_INFO;
    
        //struct data_area_4_tty      *da = pvm_data_area( me, tty );
    
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
        struct pvm_object o = POP_ARG;
    
        //pvm_object_print( o );
        printf("\n\nobj dump: ");
        dumpo((addr_t)(o.data));
        printf("\n\n");
    
        SYS_FREE_O(o);
    
        SYSCALL_RETURN_NOTHING;
}

int gotoxy_19(struct pvm_object o, struct data_area_4_thread *tc)
{
        struct data_area_4_tty      *da = pvm_data_area( me, tty );
    
        DEBUG_INFO;
        int n_param = POP_ISTACK;
    
        CHECK_PARAM_COUNT(n_param, 2);
    
        int goy = POP_INT();
        int gox = POP_INT();
    
        da->x = da->font_width * gox;
        da->y = da->font_height * goy;
    
        SYSCALL_RETURN_NOTHING;
}

int clear_20(struct pvm_object o, struct data_area_4_thread *tc)
{
        struct data_area_4_tty      *da = pvm_data_area( me, tty );
    
        DEBUG_INFO;
    
        da->x = da->y = 0;
    
        w_fill( &(da->w), da->bg );
        w_update( &(da->w) );
    
        SYSCALL_RETURN_NOTHING;
}

int setcolor_21(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void) me;
        //struct data_area_4_tty      *da = pvm_data_area( me, tty );
    
        DEBUG_INFO;
        int n_param = POP_ISTACK;
    
        CHECK_PARAM_COUNT(n_param, 1);
    
        int color = POP_INT();
        (void) color;
        //int attr = (short)color;
    
        // TODO colors from attrs
        //printf("setcolor  font %d,%d\n", da->font_width, da->font_height );
    
        SYSCALL_RETURN_NOTHING;
}

int fill_22(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void) me;
        DEBUG_INFO;
        SYSCALL_THROW_STRING( "not implemented" );
}

int putblock_23(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void) me;
        DEBUG_INFO;
        SYSCALL_THROW_STRING( "not implemented" );
}

int tty_setWinPos_24(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_tty      *da = pvm_data_area( me, tty );
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 2);
    
        int y = POP_INT();
        int x = POP_INT();
    
        w_move( &(da->w), x, y );
    
        SYSCALL_RETURN_NOTHING;
}

int tty_setWinTitle_25(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_tty      *da = pvm_data_area( me, tty );
    
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
        struct pvm_object _text = POP_ARG;
        ASSERT_STRING(_text);
    
        int len = pvm_get_str_len( _text );
        const char * data = (const char *)pvm_get_str_data(_text);
    
        if( len > PVM_MAX_TTY_TITLE-1 ) len = PVM_MAX_TTY_TITLE-1 ;
        strlcpy( da->title, data, len+1 );
        //buf[len] = 0;
    
        SYS_FREE_O(_text);
    
        w_set_title( &(da->w), da->w.title );
    
        SYSCALL_RETURN_NOTHING;
}

syscall_func_t syscall_table_4_tty[] =
{
    si_void_0_construct, si_void_1_destruct, 
    si_void_2_class, si_void_3_clone, 
    si_void_4_equals, tostring_5, 
    si_void_6_toXML, si_void_7_fromXML, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, si_void_15_hashcode, 
    getwc_16, putws_17, 
    debug_18, gotoxy_19, 
    clear_20, setcolor_21, 
    fill_22, putblock_23, 
    tty_setWinPos_24, tty_setWinTitle_25, 
};

int n_syscall_table_4_tty = 26;

// -------- syscall function (methods) for MUTEX class. --------


int si_mutex_5_tostring(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)o;
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_string_object( "mutex" ));
}

int si_mutex_8_lock(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        vm_mutex_lock( me, tc );
        SYSCALL_RETURN_NOTHING;
}

int si_mutex_9_unlock(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        //struct data_area_4_mutex *da = pvm_object_da( me, mutex );
        //(void)da;
    
        // No locking in syscalls!!
        //pthread_mutex_unlock(&(da->mutex));
    
        errno_t rc = vm_mutex_unlock( me, tc );
        switch(rc)
        {
        case EINVAL:
            printf("mutex unlock - not owner");
            SYSCALL_THROW_STRING( "mutex unlock - not owner" );
            // unreached
            break;
    
        default: break;
        }
    
        SYSCALL_RETURN_NOTHING;
}

syscall_func_t syscall_table_4_mutex[] =
{
    si_void_0_construct, si_void_1_destruct, 
    si_void_2_class, si_void_3_clone, 
    si_void_4_equals, si_mutex_5_tostring, 
    si_void_6_toXML, si_void_7_fromXML, 
    si_mutex_8_lock, si_mutex_9_unlock, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, si_void_15_hashcode, 
};

int n_syscall_table_4_mutex = 16;

// -------- syscall function (methods) for COND class. --------


int si_cond_5_tostring(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)o;
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_string_object( ".internal.cond" ));
}

int si_cond_8_wait(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)me;
        DEBUG_INFO;
        struct data_area_4_cond *da = pvm_object_da( me, cond );
        (void)da;
    
        // No locking in syscalls!!
        //pthread_cond_wait(&(da->cond));
    
        //SYSCALL_PUT_THIS_THREAD_ASLEEP();
        SYSCALL_THROW_STRING( "wait not impl" );
    
        SYSCALL_RETURN_NOTHING;
}

int si_cond_9_twait(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)me;
        DEBUG_INFO;
        struct data_area_4_cond *da = pvm_object_da( me, cond );
        (void)da;
    
        SYSCALL_THROW_STRING( "timed wait not impl" );
    
        // No locking in syscalls!!
        //pthread_cond_timedwait(&(da->cond));
    
        //SYSCALL_PUT_THIS_THREAD_ASLEEP();
    
    
        SYSCALL_RETURN_NOTHING;
}

int si_cond_10_broadcast(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_cond *da = pvm_object_da( me, cond );
        (void)da;
    
        // No locking in syscalls!!
        //pthread_cond_broadcast(&(da->cond));
    
        //SYSCALL_WAKE_THREAD_UP(thread)
    
        SYSCALL_RETURN_NOTHING;
}

int si_cond_11_signal(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_cond *da = pvm_object_da( me, cond );
        (void)da;
    
        // No locking in syscalls!!
        //pthread_cond_signal(&(da->cond));
    
        //SYSCALL_WAKE_THREAD_UP(thread)
    
        SYSCALL_RETURN_NOTHING;
}

syscall_func_t syscall_table_4_cond[] =
{
    si_void_0_construct, si_void_1_destruct, 
    si_void_2_class, si_void_3_clone, 
    si_void_4_equals, si_cond_5_tostring, 
    si_void_6_toXML, si_void_7_fromXML, 
    si_cond_8_wait, si_cond_9_twait, 
    si_cond_10_broadcast, si_cond_11_signal, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, si_void_15_hashcode, 
};

int n_syscall_table_4_cond = 16;

// -------- syscall function (methods) for BINARY class. --------


int si_binary_5_tostring(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)o;
        DEBUG_INFO;
        // TODO hexdump
    
        if(1)
        {
            struct data_area_4_binary *da = pvm_object_da( o, binary );
            int size = o.data->_da_size - sizeof( struct data_area_4_binary );
    
            hexdump( da->data, size, "", 0);
        }
    
        SYSCALL_RETURN(pvm_create_string_object( "(binary)" ));
}

int si_binary_8_getbyte(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_binary *da = pvm_object_da( me, binary );
    
        unsigned int index = POP_INT();
    
        int size = me.data->_da_size - sizeof( struct data_area_4_binary );
    
        //if( index < 0 || index >= size )
        if( index >= size )
            SYSCALL_THROW_STRING( "binary index out of bounds" );
    
        SYSCALL_RETURN(pvm_create_int_object( da->data[index] ));
}

int si_binary_9_setbyte(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_binary *da = pvm_object_da( me, binary );
    
        unsigned int byte = POP_INT();
        unsigned int index = POP_INT();
    
        int size = me.data->_da_size - sizeof( struct data_area_4_binary );
    
        //if( index < 0 || index >= size )
        if( index >= size )
            SYSCALL_THROW_STRING( "binary index out of bounds" );
    
        da->data[index] = byte;
    
        SYSCALL_RETURN_NOTHING;
}

int si_binary_10_setrange(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_binary *da = pvm_object_da( me, binary );
    
        unsigned int len = POP_INT();
        unsigned int frompos = POP_INT();
        unsigned int topos = POP_INT();
    
        // TODO assert his class!!
        struct pvm_object _src = POP_ARG;
        struct data_area_4_binary *src = pvm_object_da( _src, binary );
    
    
        int size = me.data->_da_size - sizeof( struct data_area_4_binary );
    
        //if( topos < 0 || topos+len > size )
        if( topos+len > size )
            SYSCALL_THROW_STRING( "binary copy dest index/len out of bounds" );
    
        int src_size = _src.data->_da_size - sizeof( struct data_area_4_binary );
    
        //if( frompos < 0 || frompos+len > src_size )
        if( frompos+len > src_size )
            SYSCALL_THROW_STRING( "binary copy src index/len out of bounds" );
    
        //da->data[index] = byte;
        memcpy( (da->data)+topos, (src->data)+frompos, len );
    
        SYS_FREE_O(_src);
    
        SYSCALL_RETURN_NOTHING;
}

syscall_func_t syscall_table_4_binary[] =
{
    si_void_0_construct, si_void_1_destruct, 
    si_void_2_class, si_void_3_clone, 
    si_void_4_equals, si_binary_5_tostring, 
    si_void_6_toXML, si_void_7_fromXML, 
    si_binary_8_getbyte, si_binary_9_setbyte, 
    si_binary_10_setrange, invalid_syscall, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, si_void_15_hashcode, 
};

int n_syscall_table_4_binary = 16;

// -------- syscall function (methods) for BITMAP class. --------


int si_bitmap_5_tostring(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)o;
        DEBUG_INFO;
        // TODO hexdump
        SYSCALL_RETURN(pvm_create_string_object( "(bitmap)" ));
}

int si_bitmap_8_fromstring(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_bitmap *da = pvm_object_da( me, bitmap );
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
    //printf("Load from string\n");
    
        pvm_object_t _s = POP_ARG;
    
        if( drv_video_string2bmp( da, pvm_object_da( _s, string)->data ) )
        	SYSCALL_THROW_STRING("can not parse graphics data");
    
        SYS_FREE_O(_s);
    
        SYSCALL_RETURN_NOTHING;
}

int si_bitmap_9_paintto(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_bitmap *da = pvm_object_da( me, bitmap );
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
        int y = POP_INT();
        int x = POP_INT();
        struct pvm_object _tty = POP_ARG;
    
        // TODO check class!
        struct data_area_4_tty *tty = pvm_object_da( _tty, tty );
        struct data_area_4_binary *pixels = pvm_object_da( da->image, binary );
    
        bitmap2bitmap(
        		tty->pixel, tty->w.xsize, tty->w.ysize, x, y,
        		(rgba_t *)pixels, da->xsize, da->ysize, 0, 0,
        		da->xsize, da->ysize
        );
        //drv_video_winblt( &(tty->w), tty->w.x, tty->w.y);
        w_update( &(tty->w) );
    
        SYS_FREE_O(_tty);
    
        SYSCALL_RETURN_NOTHING;
}

syscall_func_t syscall_table_4_bitmap[] =
{
    si_void_0_construct, si_void_1_destruct, 
    si_void_2_class, si_void_3_clone, 
    si_void_4_equals, si_bitmap_5_tostring, 
    si_void_6_toXML, si_void_7_fromXML, 
    si_bitmap_8_fromstring, si_bitmap_9_paintto, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, si_void_15_hashcode, 
};

int n_syscall_table_4_bitmap = 16;

// -------- syscall function (methods) for WORLD class. --------


int si_world_5_tostring(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)o;
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_string_object( "(world)" ));
}

int si_world_8_getMyThread(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)o;
        DEBUG_INFO;
    
        // TODO spinlock!
        if(thread_iface == 0 )
        {
            struct data_area_4_class *cda = pvm_object_da( pvm_get_thread_class(), class );
            thread_iface = cda->object_default_interface.data;
        }
    
        struct pvm_object out;
    
        out.data =
            (pvm_object_storage_t *)
            (tc - DA_OFFSET()); // TODO XXX HACK!
        out.interface = thread_iface;
    
        SYSCALL_RETURN( ref_inc_o( out ) );
}

syscall_func_t syscall_table_4_world[] =
{
    si_void_0_construct, si_void_1_destruct, 
    si_void_2_class, si_void_3_clone, 
    si_void_4_equals, si_world_5_tostring, 
    si_void_6_toXML, si_void_7_fromXML, 
    si_world_8_getMyThread, invalid_syscall, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, si_void_15_hashcode, 
};

int n_syscall_table_4_world = 16;

// -------- syscall function (methods) for CLOSURE class. --------


int si_closure_9_getordinal(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_closure *da = pvm_object_da( me, closure );
    
        SYSCALL_RETURN(pvm_create_int_object( da->ordinal ));
}

int si_closure_10_setordinal(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_closure *da = pvm_object_da( me, closure );
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
        da->ordinal = POP_INT();
    
        SYSCALL_RETURN_NOTHING;
}

int si_closure_11_setobject(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_closure *da = pvm_object_da( me, closure );
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
        // We do not decrement its refcount, 'cause we store it.
        da->object = POP_ARG;
    
        SYSCALL_RETURN_NOTHING;
}

syscall_func_t syscall_table_4_closure[] =
{
    si_void_0_construct, si_void_1_destruct, 
    si_void_2_class, si_void_3_clone, 
    si_void_4_equals, si_binary_5_tostring, 
    si_void_6_toXML, si_void_7_fromXML, 
    invalid_syscall, si_closure_9_getordinal, 
    si_closure_10_setordinal, si_closure_11_setobject, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, si_void_15_hashcode, 
};

int n_syscall_table_4_closure = 16;

// -------- syscall function (methods) for WINDOW class. --------


int si_window_5_tostring(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)o;
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_string_object( "(window)" ));
}

int win_getXSize(struct pvm_object o, struct data_area_4_thread *tc)
{
        struct data_area_4_window      *da = pvm_data_area( o, window );
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_int_object( da->w.xsize ));
}

int win_getYSize(struct pvm_object o, struct data_area_4_thread *tc)
{
        struct data_area_4_window      *da = pvm_data_area( o, window );
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_int_object( da->w.ysize ));
}

int win_getX(struct pvm_object o, struct data_area_4_thread *tc)
{
        struct data_area_4_window      *da = pvm_data_area( o, window );
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_int_object( da->x ));
}

int win_getY(struct pvm_object o, struct data_area_4_thread *tc)
{
        struct data_area_4_window      *da = pvm_data_area( o, window );
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_int_object( da->y ));
}

int win_clear_20(struct pvm_object o, struct data_area_4_thread *tc)
{
        struct data_area_4_window      *da = pvm_data_area( me, window );
    
        DEBUG_INFO;
    
        da->x = da->y = 0;
    
        w_fill( &(da->w), da->bg );
        w_update( &(da->w) );
    
        SYSCALL_RETURN_NOTHING;
}

int win_fill_21(struct pvm_object o, struct data_area_4_thread *tc)
{
        struct data_area_4_window      *da = pvm_data_area( me, window );
        DEBUG_INFO;
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
        int color = POP_INT();
    
        rgba_t c;
        INT32_TO_RGBA(c, color);
        w_fill( &(da->w), c );
    
        SYSCALL_RETURN_NOTHING;
}

int win_setFGcolor_22(struct pvm_object o, struct data_area_4_thread *tc)
{
        struct data_area_4_window      *da = pvm_data_area( me, window );
    
        DEBUG_INFO;
        int n_param = POP_ISTACK;
    
        CHECK_PARAM_COUNT(n_param, 1);
    
        int color = POP_INT();
        INT32_TO_RGBA(da->fg, color);
    
        SYSCALL_RETURN_NOTHING;
}

int win_setBGcolor_23(struct pvm_object o, struct data_area_4_thread *tc)
{
        struct data_area_4_window      *da = pvm_data_area( me, window );
    
        DEBUG_INFO;
        int n_param = POP_ISTACK;
    
        CHECK_PARAM_COUNT(n_param, 1);
    
        int color = POP_INT();
        INT32_TO_RGBA(da->bg, color);
    
        SYSCALL_RETURN_NOTHING;
}

int win_putString_24(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
    
        struct data_area_4_tty      *da = pvm_data_area( me, tty );
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 3);
    
        struct pvm_object _text = POP_ARG;
        ASSERT_STRING(_text);
    
        int x = POP_INT();
        int y = POP_INT();
    
    
        int len = pvm_get_str_len( _text );
        const char * data = (const char *)pvm_get_str_data(_text);
    
    #define BS 1024
        char buf[BS+2];
    
        if( len > BS ) len = BS;
        strncpy( buf, data, len );
        buf[len] = 0;
    
        SYS_FREE_O(_text);
    
        //printf("tty print: '%s' at %d,%d\n", buf, da->x, da->y );
    
        struct rgba_t fg = da->fg;
        struct rgba_t bg = da->bg;
    
        // TODO make a version of drv_video_font_tty_string that accepts non-zero terminated strings with len
        w_font_tty_string( &(da->w), tty_font, buf, fg, bg, &x, &y );
        w_update( &(da->w) );
    
        SYSCALL_RETURN_NOTHING;
}

int win_putImage_25(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_window      *da = pvm_data_area( me, window );
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 3);
    
        struct pvm_object _img = POP_ARG;
        int y = POP_INT();
        int x = POP_INT();
    
        // TODO check class!
        struct data_area_4_bitmap *_bmp = pvm_object_da( _img, bitmap );
        //struct data_area_4_tty *tty = pvm_object_da( _tty, tty );
        struct data_area_4_binary *pixels = pvm_object_da( _bmp->image, binary );
    
        bitmap2bitmap(
        		da->pixel, da->w.xsize, da->w.ysize, x, y,
        		(rgba_t *)pixels, _bmp->xsize, _bmp->ysize, 0, 0,
        		_bmp->xsize, _bmp->ysize
        );
        //drv_video_winblt( &(tty->w), tty->w.x, tty->w.y);
        // Sure?
        w_update( &(da->w) );
    
        SYS_FREE_O(_img);
    
        SYSCALL_RETURN_NOTHING;
}

int win_setSize_26(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_window      *da = pvm_data_area( me, window );
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 2);
    
        int y = POP_INT();
        int x = POP_INT();
    
        if(x*y > PVM_MAX_WIN_PIXELS)
            SYSCALL_THROW_STRING( "new win size > PVM_MAX_WIN_PIXELS" );
    
        w_resize( &(da->w), x, y );
    
        SYSCALL_RETURN_NOTHING;
}

int win_setPos_27(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_window      *da = pvm_data_area( me, window );
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 2);
    
        int y = POP_INT();
        int x = POP_INT();
    
        w_move( &(da->w), x, y );
    
        SYSCALL_RETURN_NOTHING;
}

int win_drawLine_28(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_window      *da = pvm_data_area( me, window );
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 4);
    
        int ys = POP_INT();
        int xs = POP_INT();
        int y = POP_INT();
        int x = POP_INT();
    
        w_draw_line( &(da->w), x, y, x+xs, y+ys, da->fg );
    
    
        SYSCALL_RETURN_NOTHING;
}

int win_drawBox_29(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_window      *da = pvm_data_area( me, window );
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 4);
    
        int ys = POP_INT();
        int xs = POP_INT();
        int y = POP_INT();
        int x = POP_INT();
    
        w_draw_box( &(da->w), x, y, xs, ys, da->fg );
    
        SYSCALL_RETURN_NOTHING;
}

int win_fillBox_30(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_window      *da = pvm_data_area( me, window );
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 4);
    
        int ys = POP_INT();
        int xs = POP_INT();
        int y = POP_INT();
        int x = POP_INT();
    
        w_fill_box( &(da->w), x, y, xs, ys, da->fg );
    
        SYSCALL_RETURN_NOTHING;
}

int win_fillEllipse_31(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_window      *da = pvm_data_area( me, window );
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 4);
    
        int ys = POP_INT();
        int xs = POP_INT();
        int y = POP_INT();
        int x = POP_INT();
    
        w_fill_ellipse( &(da->w), x, y, xs, ys, da->fg );
    
        SYSCALL_RETURN_NOTHING;
}

int win_setHandler_32(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_window      *da = pvm_data_area( me, window );
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
        struct pvm_object handler = POP_ARG;
    
        // TODO check class!
    
    //    da->event_handler = handler;
    
        {
        struct data_area_4_connection  *cda = (struct data_area_4_connection *)da->connector.data->da;
        // No sync - assume caller does it before getting real callbacks
        cda->callback = handler;
        cda->callback_method = 8; // TODO BUG FIXME
        }
    
        //SYS_FREE_O(_img);
    
        SYSCALL_RETURN_NOTHING;
}

int win_setTitle_33(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_window      *da = pvm_data_area( me, window );
    
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
        struct pvm_object _text = POP_ARG;
        ASSERT_STRING(_text);
    
        int len = pvm_get_str_len( _text );
        const char * data = (const char *)pvm_get_str_data(_text);
    
        if( len > PVM_MAX_TTY_TITLE-1 ) len = PVM_MAX_TTY_TITLE-1 ;
        strlcpy( da->title, data, len+1 );
        //buf[len] = 0;
    
        SYS_FREE_O(_text);
    
        w_set_title( &(da->w), da->w.title );
    
        SYSCALL_RETURN_NOTHING;
}

int win_update_34(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_window      *da = pvm_data_area( me, window );
    
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 0);
    
    
        w_update( &(da->w) );
    
        SYSCALL_RETURN_NOTHING;
}

int win_scrollHor_35(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_window      *da = pvm_data_area( me, window );
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 5);
    
        int s = POP_INT();
        int ys = POP_INT();
        int xs = POP_INT();
        int y = POP_INT();
        int x = POP_INT();
    
        errno_t err = w_scroll_hor( &(da->w), x, y, xs, ys, s );
    
        //SYSCALL_RETURN_NOTHING;
        SYSCALL_RETURN(pvm_create_int_object( err ));
}

syscall_func_t syscall_table_4_window[] =
{
    si_void_0_construct, si_void_1_destruct, 
    si_void_2_class, si_void_3_clone, 
    si_void_4_equals, si_window_5_tostring, 
    si_void_6_toXML, si_void_7_fromXML, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, si_void_15_hashcode, 
    win_getXSize, win_getYSize, 
    win_getX, win_getY, 
    win_clear_20, win_fill_21, 
    win_setFGcolor_22, win_setBGcolor_23, 
    win_putString_24, win_putImage_25, 
    win_setSize_26, win_setPos_27, 
    win_drawLine_28, win_drawBox_29, 
    win_fillBox_30, win_fillEllipse_31, 
    win_setHandler_32, win_setTitle_33, 
    win_update_34, win_scrollHor_35, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, invalid_syscall, 
};

int n_syscall_table_4_window = 40;

// -------- syscall function (methods) for DIRECTORY class. --------


int si_directory_4_equals(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)o;
        DEBUG_INFO;
        SYSCALL_THROW_STRING( "dir.equals: not implemented" );
        //SYSCALL_RETURN(pvm_create_string_object( "(directory)" ));
}

int si_directory_5_tostring(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)o;
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_string_object( "(directory)" ));
}

int si_directory_8_put(struct pvm_object o, struct data_area_4_thread *tc)
{
        struct data_area_4_directory *da = pvm_object_da( o, directory );
        DEBUG_INFO;
    
        struct pvm_object val = POP_ARG;
        struct pvm_object key = POP_ARG;
        ASSERT_STRING(key);
    
        errno_t rc = hdir_add( da, pvm_get_str_data(key), pvm_get_str_len(key), val );
    
        SYS_FREE_O(key); // dir code creates it's own binary object
        if( rc ) SYS_FREE_O( val ); // we didn't put it there
    
        SYSCALL_RETURN(pvm_create_int_object( rc ));
}

int si_directory_9_get(struct pvm_object o, struct data_area_4_thread *tc)
{
        struct data_area_4_directory *da = pvm_object_da( o, directory );
        DEBUG_INFO;
    
        struct pvm_object key = POP_ARG;
        ASSERT_STRING(key);
    
        pvm_object_t out;
        errno_t rc = hdir_find( da, pvm_get_str_data(key), pvm_get_str_len(key), &out, 0 );
        if( rc )
            SYSCALL_RETURN_NOTHING;
        else
            SYSCALL_RETURN(out);
}

int si_directory_10_remove(struct pvm_object o, struct data_area_4_thread *tc)
{
        struct data_area_4_directory *da = pvm_object_da( o, directory );
        DEBUG_INFO;
    
        struct pvm_object key = POP_ARG;
        ASSERT_STRING(key);
    
        pvm_object_t out; // unused
        errno_t rc = hdir_find( da, pvm_get_str_data(key), pvm_get_str_len(key), &out, 1 );
        SYSCALL_RETURN(pvm_create_int_object( rc ));
}

int si_directory_11_size(struct pvm_object o, struct data_area_4_thread *tc)
{
        struct data_area_4_directory *da = pvm_object_da( o, directory );
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_int_object( da->nEntries ));
}

int si_directory_12_iterate(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)o;
        DEBUG_INFO;
        // TODO implement dir iterator
    
        SYSCALL_THROW_STRING( "dir.iterate: not implemented" );
        SYSCALL_RETURN_NOTHING;
        //return pvm_create_null_object();
}

syscall_func_t syscall_table_4_directory[] =
{
    si_void_0_construct, si_void_1_destruct, 
    si_void_2_class, si_void_3_clone, 
    si_directory_4_equals, si_directory_5_tostring, 
    si_void_6_toXML, si_void_7_fromXML, 
    si_directory_8_put, si_directory_9_get, 
    si_directory_10_remove, si_directory_11_size, 
    si_directory_12_iterate, invalid_syscall, 
    invalid_syscall, si_void_15_hashcode, 
};

int n_syscall_table_4_directory = 16;

// -------- syscall function (methods) for CONNECTION class. --------


int si_connection_5_tostring(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)o;
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_string_object( "(connection)" ));
}

int si_connection_8_connect(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_connection *da = pvm_object_da( o, connection );
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
    
        pvm_object_t _s = POP_ARG;
    
        if(!IS_PHANTOM_STRING(_s))
        {
            SYS_FREE_O(_s);
            SYSCALL_THROW_STRING( "connection.connect: not a string arg" );
        }
    
        int slen = pvm_get_str_len(_s);
    
        if( slen+1 > sizeof( da->name ) )
        {
            SYS_FREE_O(_s);
            SYSCALL_THROW_STRING( "string arg too long" );
        }
    
        strncpy( da->name, pvm_get_str_data(_s), slen );
        SYS_FREE_O(_s);
    
        printf(".internal.connection: Connect to '%s'\n", da->name );
    
        int ret = pvm_connect_object(o,tc);
    
        SYSCALL_RETURN(pvm_create_int_object( ret ) );
}

int si_connection_9_disconnect(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        //struct data_area_4_connection *da = pvm_object_da( o, connection );
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 0);
    
        int ret = pvm_disconnect_object(o,tc);
    
        SYSCALL_RETURN(pvm_create_int_object( ret ) );
}

int si_connection_10_check(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_connection *da = pvm_object_da( o, connection );
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 1);
    
        int op_index = POP_INT();
    
    #if !PVM_CONNECTION_WAKE
        (void) op_index;
        (void) da;
        int ret = ENXIO;
    #else
        int ret = 0;
    
        if( da->kernel == 0 || da->kernel->check_operation || da->kernel->req_wake )
        {
            ret = ENXIO;
        }
        else
        {
            ret = da->kernel->check_operation( op_index, da, tc );
            if( ret )
            {
                // TODO races?
                ret = da->kernel->req_wake( op_index, da, tc );
                if( ret == 0 )
                    SYSCALL_PUT_THIS_THREAD_ASLEEP();
            }
        }
    #endif
        SYSCALL_RETURN(pvm_create_int_object( ret ) );
}

int si_connection_11_do(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_connection *da = pvm_object_da( o, connection );
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 2);
    
        int op_index = POP_INT();
        pvm_object_t arg = POP_ARG;
    
        int ret = 0;
    
        if( (da->kernel == 0) || (da->kernel->do_operation == 0) )
        {
            ret = ENXIO;
        }
        else
        {
            ret = da->kernel->do_operation( op_index, da, tc, arg );
        }
    
        SYS_FREE_O(arg);
        SYSCALL_RETURN(pvm_create_int_object( ret ) );
}

int si_connection_12_set_callback(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_connection *da = pvm_object_da( o, connection );
    
        int n_param = POP_ISTACK;
        CHECK_PARAM_COUNT(n_param, 2);
    
        int nmethod = POP_INT();
        pvm_object_t callback_object = POP_ARG;
    
        int ret = !pvm_isnull( da->callback );
    
        // No sync - assume caller does it before getting real callbacks
        da->callback = callback_object;
        da->callback_method = nmethod;
    
        SYSCALL_RETURN(pvm_create_int_object( ret ) );
}

int si_connection_13_blocking(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_connection *da = pvm_object_da( o, connection );
    
        /*
        int n_param = POP_ISTACK;
        pvm_istack_push( tc->_istack, n_param-1 ); // we'll take one
    
        if( n_param < 1 )
            SYSCALL_THROW(pvm_create_string_object( "blocking: need at least 1 parameter" )); \
    
        int nmethod = POP_INT();
        */
    
        pvm_object_t (*syscall_worker)( pvm_object_t , struct data_area_4_thread *, int nmethod, pvm_object_t arg ) = da->blocking_syscall_worker;
    
        //SHOW_FLOW( 1, "blocking call to nmethod = %d", nmethod);
        return vm_syscall_block( o, tc, syscall_worker );
        // vm_syscall_block pushes retcode itself
}

syscall_func_t syscall_table_4_connection[] =
{
    si_void_0_construct, si_void_1_destruct, 
    si_void_2_class, si_void_3_clone, 
    si_void_4_equals, si_connection_5_tostring, 
    si_void_6_toXML, si_void_7_fromXML, 
    si_connection_8_connect, si_connection_9_disconnect, 
    si_connection_10_check, si_connection_11_do, 
    si_connection_12_set_callback, si_connection_13_blocking, 
    invalid_syscall, si_void_15_hashcode, 
};

int n_syscall_table_4_connection = 16;

// -------- syscall function (methods) for SEMA class. --------


int si_sema_5_tostring(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)o;
        DEBUG_INFO;
        SYSCALL_RETURN(pvm_create_string_object( ".internal.sema" ));
}

int si_sema_8_acquire(struct pvm_object o, struct data_area_4_thread *tc)
{
    #if OLD_VM_SLEEP
        DEBUG_INFO;
        struct data_area_4_sema *da = pvm_object_da( me, sema );
        VM_SPIN_LOCK(da->poor_mans_pagefault_compatible_spinlock);
    
        while( da->sem_value <= 0 )
        {
            // Sema is busy, fall asleep now
            pvm_object_t this_thread = pvm_da_to_object(tc);
    
            assert(!pvm_isnull(this_thread));
            assert(pvm_object_class_is( this_thread, pvm_get_thread_class() ) );
    
            pvm_set_ofield( da->waiting_threads_array, da->nwaiting++, this_thread );
    
    //#warning have SYSCALL_PUT_THIS_THREAD_ASLEEP unlock the spinlock!
            //VM_SPIN_UNLOCK(da->poor_mans_pagefault_compatible_spinlock);
            SYSCALL_PUT_THIS_THREAD_ASLEEP(&da->poor_mans_pagefault_compatible_spinlock);
            VM_SPIN_LOCK(da->poor_mans_pagefault_compatible_spinlock);
        }
    
        da->sem_value--;
        da->owner_thread = tc;
    
        VM_SPIN_UNLOCK(da->poor_mans_pagefault_compatible_spinlock);
        SYSCALL_RETURN_NOTHING;
    #else
        SYSCALL_THROW_STRING("Not this way");
    #endif
}

int si_sema_9_tacquire(struct pvm_object o, struct data_area_4_thread *tc)
{
        (void)me;
        DEBUG_INFO;
        struct data_area_4_sema *da = pvm_object_da( me, sema );
        (void)da;
    
        SYSCALL_THROW_STRING( "timed acquire not impl" );
    
    
        SYSCALL_RETURN_NOTHING;
}

int si_sema_10_zero(struct pvm_object o, struct data_area_4_thread *tc)
{
        DEBUG_INFO;
        struct data_area_4_sema *da = pvm_object_da( me, sema );
    
        if( da->sem_value > 0 )
            da->sem_value = 0;
    
        SYSCALL_RETURN_NOTHING;
}

int si_sema_11_release(struct pvm_object o, struct data_area_4_thread *tc)
{
    #if OLD_VM_SLEEP
        DEBUG_INFO;
        struct data_area_4_sema *da = pvm_object_da( me, sema );
        VM_SPIN_LOCK(da->poor_mans_pagefault_compatible_spinlock);
    
        da->sem_value++;
    
        if( da->nwaiting > 0 )
        {
            // Wakeup one
            // TODO takes last, must take first
            pvm_object_t next_thread = pvm_get_ofield( da->waiting_threads_array, --da->nwaiting );
    
            assert(!pvm_isnull(next_thread));
            assert(pvm_object_class_is( next_thread, pvm_get_thread_class() ) );
    
            da->owner_thread = pvm_object_da( next_thread, thread );
            SYSCALL_WAKE_THREAD_UP( da->owner_thread );
        }
    
        VM_SPIN_UNLOCK(da->poor_mans_pagefault_compatible_spinlock);
        SYSCALL_RETURN_NOTHING;
    #else
        SYSCALL_THROW_STRING("Not this way");
    #endif
}

syscall_func_t syscall_table_4_sema[] =
{
    si_void_0_construct, si_void_1_destruct, 
    si_void_2_class, si_void_3_clone, 
    si_void_4_equals, si_sema_5_tostring, 
    si_void_6_toXML, si_void_7_fromXML, 
    si_sema_8_acquire, si_sema_9_tacquire, 
    si_sema_10_zero, si_sema_11_release, 
    invalid_syscall, invalid_syscall, 
    invalid_syscall, si_void_15_hashcode, 
};

int n_syscall_table_4_sema = 16;
