﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeGeneration
{
    class SyscallC
    {
        static public void Test(string[] args)
        {
            string sqlCon = @"Data Source=(LocalDB)\MSSQLLocalDB;" +
                "AttachDbFilename=" + Path.Combine(Directory.GetCurrentDirectory(), "pvm.mdf") + ";" +
                "Integrated Security=True; Connect Timeout=5";

            string code = GetFunctionCode(sqlCon, "invalid_syscall");
            Console.WriteLine("Code: " + code);

            SqlConnection conn = new SqlConnection(sqlCon);
            conn.Open();

            string command = string.Format("SELECT [Id], [Name], [SyscalTable] FROM [{0}] WHERE [isEnabled] = 1 ORDER BY [Id]", "InternalClasses");
            SqlCommand comm = new SqlCommand(command, conn);

            SqlDataReader reader = comm.ExecuteReader();

            List<string> functions = new List<string> { "'invalid_syscall'" };
            
            while (reader.Read())
            {
                string list = string.Join(",", functions);
                string command2 = string.Format(
                    "SELECT MIN([Ordinal]) AS [Ordinal], [Function] " +
                    "FROM [SyscallTable2Function] " +
                    "WHERE [Table] = '{0}' " +
                    "AND [Function] NOT IN (" + list + ") " +
                    "GROUP BY [Function] " +
                    "ORDER BY [Ordinal]; "
                    , reader["SyscalTable"]);
                using (var conn2 = new SqlConnection(sqlCon))
                using (var comm2 = new SqlCommand(command2, conn2))
                {
                    try
                    {
                        conn2.Open();
                        SqlDataReader reader2 = comm2.ExecuteReader();
                        while (reader2.Read())
                        {
                            string function = reader2.GetString(reader2.GetOrdinal("Function"));
                            Console.WriteLine(string.Format("{0}, {1}", reader2["Ordinal"], function));

                            code = GetFunctionCode(sqlCon, function);
                            Console.WriteLine("Code: " + code);

                            functions.Add("'" + reader2["Function"] + "'");
                        }
                        reader2.Close();
                        conn2.Close();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error executing.", ex);
                    }
                }

                string command3 = string.Format(
                    "SELECT [Ordinal], [Function] " +
                    "FROM [SyscallTable2Function] " +
                    "WHERE [Table] = '{0}' " +
                    "ORDER BY [Ordinal]; "
                    , reader["SyscalTable"]);
                //Console.WriteLine(command3);
                using (var conn3 = new SqlConnection(sqlCon))
                using (var comm3 = new SqlCommand(command3, conn3))
                {
                    try
                    {
                        conn3.Open();
                        SqlDataReader reader3 = comm3.ExecuteReader();

                        IDictionary < byte, string> funcdic = new Dictionary<byte, string>();
                        while (reader3.Read())
                        {
                            //Console.WriteLine(string.Format("{0}, {1}", reader3["Ordinal"], reader3["Function"]));
                            byte ordinal = reader3.GetByte(0);
                            string function = reader3.GetString(1);
                            funcdic.Add(ordinal, function);
                        }
                        reader3.Close();
                        conn3.Close();

                        if (funcdic.Count <= 0 ) funcdic.Add(15, "invalid_syscall");
                        int count = 0;
                        foreach (var kvp in funcdic)
                        {
                            string value = "invalid_syscall";
                            while (count <= kvp.Key)
                            {
                                if (kvp.Key == count) value = kvp.Value;
                                if (count % 2 == 0)
                                {
                                    Console.Write(string.Format("lll {0}, {1}, {2}    ", count, kvp.Key, value));
                                }
                                else
                                {
                                    Console.WriteLine(string.Format("lll {0}, {1}, {2}", count, kvp.Key, value));
                                }
                                count++;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error executing.", ex);
                    }
                }
                Console.WriteLine();
            }
            reader.Close();

            conn.Close();
        }

        static private string GetFunctionCode (string sqlCon, string function)
        {
            string code = "";

            string command = string.Format(
                "SELECT [Function], [Code] " +
                "FROM [SyscallFunctions] " +
                "WHERE [Function] = '{0}' "
                , function);
            Console.WriteLine(command);
            using (var conn = new SqlConnection(sqlCon))
            using (var comm = new SqlCommand(command, conn))
            {
                try
                {
                    conn.Open();
                    SqlDataReader reader = comm.ExecuteReader();

                    if (reader.Read()) code = reader.GetString(1);

                    reader.Close();
                    conn.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error executing.", ex);
                }
            }

            return code;
        }
    }
}


