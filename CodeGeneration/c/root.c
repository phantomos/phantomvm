﻿#include "root.h"
// for pvm_internal_classes and class IDs (such as PVM_ROOT_OBJECT_NULL_CLASS etc.)
#include "internal.h"
// for memory allocation
#include "alloc.h"

/**
*
* Either load OS state from disk or (if persistent memory is empty) create
* a new object set for a new OS instance to start from.
*
* In a latter case special bootstrap object will be created and activated to
* look for the rest of the system and bring it in.
*
**/

#define STAT_CNT_PERSISTENT_DA_SIZE 3

void pvm_root_init(void)
{
	struct pvm_object_storage *root = get_root_object_storage();

	//dbg_add_command(runclass, "runclass", "runclass class [method ordinal] - create object of given class and run method (ord 8 by default)");


	if (root->_ah.object_start_marker != PVM_OBJECT_START_MARKER)
	{
		// Not an object in the beginning of the address space?
		// We have a fresh OS instance then. Set it up from scratch.

		root->_ah.object_start_marker = PVM_OBJECT_START_MARKER;
		pvm_alloc_clear_mem();
		pvm_create_root_objects();
		//pvm_save_root_objects();

		//load_kernel_boot_env();

		/* test code
		{
		char buf[512];
		phantom_getenv("root.shell", buf, 511 );
		printf("Root shell env var is '%s'\n", buf );
		phantom_getenv("root.init", buf, 511 );
		printf("Root init env var is '%s'\n", buf );
		getchar();
		}
		*/

		pvm_boot();

		return;
	}

	//assert(pvm_object_is_allocated(root));
	/*
	// internal classes
	int i;
	for (i = 0; i < pvm_n_internal_classes; i++)
	{
		pvm_internal_classes[i].class_object = pvm_get_field(root,
			pvm_internal_classes[i].root_index
		);
	}

	set_root_from_table();


	pvm_root.null_object = pvm_get_field(root, PVM_ROOT_OBJECT_NULL);
	pvm_root.threads_list = pvm_get_field(root, PVM_ROOT_OBJECT_THREAD_LIST);
	pvm_root.sys_interface_object = pvm_get_field(root, PVM_ROOT_OBJECT_SYSINTERFACE);
	pvm_root.restart_list = pvm_get_field(root, PVM_ROOT_OBJECT_RESTART_LIST);
	pvm_root.users_list = pvm_get_field(root, PVM_ROOT_OBJECT_USERS_LIST);
	pvm_root.class_loader = pvm_get_field(root, PVM_ROOT_OBJECT_CLASS_LOADER);
	pvm_root.kernel_environment = pvm_get_field(root, PVM_ROOT_OBJECT_KERNEL_ENVIRONMENT);
	pvm_root.os_entry = pvm_get_field(root, PVM_ROOT_OBJECT_OS_ENTRY);
	pvm_root.root_dir = pvm_get_field(root, PVM_ROOT_OBJECT_ROOT_DIR);

	pvm_root.kernel_stats = pvm_get_field(root, PVM_ROOT_KERNEL_STATISTICS);


	process_specific_restarts();
	process_generic_restarts(root);
	*/
}

static void pvm_create_root_objects()
{
	int root_da_size = PVM_ROOT_OBJECTS_COUNT * sizeof(struct pvm_object);
	unsigned int flags = 0; // usual plain vanilla array
							// make sure refcount is disabled for all objects created here: 3-rd argument of pvm_object_alloc is true (obsolete: ref_saturate_p)

							// Allocate the very first object
	struct pvm_object_storage *root = get_root_object_storage();
	struct pvm_object_storage *roota = pvm_object_alloc(root_da_size, flags, 1);

	// and make sure it is really the first
	//assert(root == roota);

	// TODO set class later

	// Partially build interface object for internal classes

	flags = PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERFACE;
	pvm_root.sys_interface_object.data = pvm_object_alloc(N_SYS_METHODS * sizeof(struct pvm_object), flags, 0);
	pvm_root.sys_interface_object.interface = pvm_root.sys_interface_object.data;

	pvm_root.null_object.data = pvm_object_alloc(0, 0, 1); // da does not exist
	pvm_root.null_object.interface = pvm_root.sys_interface_object.data;


	int i;
	for (i = 0; i < pvm_n_internal_classes; i++)
	{
		flags = PHANTOM_OBJECT_STORAGE_FLAG_IS_INTERNAL | PHANTOM_OBJECT_STORAGE_FLAG_IS_CLASS;
		struct pvm_object_storage *curr = pvm_object_alloc(sizeof(struct data_area_4_class), flags, 1);
		struct data_area_4_class *da = (struct data_area_4_class *)curr->da;

		da->sys_table_id = i; // so simple
		da->object_flags = pvm_internal_classes[i].flags;
		da->object_data_area_size = pvm_internal_classes[i].da_size;

		pvm_internal_classes[i].class_object.data = curr;
		pvm_internal_classes[i].class_object.interface = pvm_root.sys_interface_object.data;
	}
	
	set_root_from_table();

	pvm_root.null_object.data->_class = pvm_root.null_class;

	pvm_root.sys_interface_object.data->_class = pvm_root.interface_class;

	for (i = 0; i < pvm_n_internal_classes; i++)
	{
		struct pvm_object_storage *curr = pvm_internal_classes[i].class_object.data;
		struct data_area_4_class *da = (struct data_area_4_class *)curr->da;

		da->class_parent = pvm_root.null_class;
		da->object_default_interface = pvm_root.sys_interface_object;

		curr->_class = pvm_root.class_class;
	}


	// It must be ok to create strings in usual way now
	for (i = 0; i < pvm_n_internal_classes; i++)
	{
		struct pvm_object_storage *curr = pvm_internal_classes[i].class_object.data;
		struct data_area_4_class *da = (struct data_area_4_class *)curr->da;

		da->class_name = pvm_create_string_object(pvm_internal_classes[i].name);
	}

	pvm_fill_syscall_interface(pvm_root.sys_interface_object, N_SYS_METHODS);

	// -------------------------------------------------------------------

	pvm_root.threads_list = pvm_create_object(pvm_get_array_class());
	pvm_root.kernel_environment = pvm_create_object(pvm_get_array_class());
	pvm_root.restart_list = pvm_create_object(pvm_get_array_class());
	pvm_root.users_list = pvm_create_object(pvm_get_array_class());
	//bakulev pvm_root.root_dir = pvm_create_directory_object();

	//bakulev pvm_root.kernel_stats = pvm_create_binary_object(STAT_CNT_PERSISTENT_DA_SIZE, 0);

	//ref_saturate_o(pvm_root.threads_list); //Need it?
	//ref_saturate_o(pvm_root.kernel_environment); //Need it?
												 //ref_saturate_o(pvm_root.restart_list); //Need it? definitely no - this list is recreated each restart
	//ref_saturate_o(pvm_root.users_list); //Need it?

	//ref_saturate_o(pvm_root.kernel_stats);
	//start_persistent_stats();
	
	//pvm_root.os_entry = pvm_get_null_object();
}

static void set_root_from_table()
{
	//SET_ROOT_CLASS(null, NULL);
	pvm_root.null_class = pvm_internal_classes[pvm_iclass_by_root_index(PVM_ROOT_OBJECT_NULL_CLASS)].class_object; //,
	//SET_ROOT_CLASS(class, CLASS);
	pvm_root.class_class = pvm_internal_classes[pvm_iclass_by_root_index(PVM_ROOT_OBJECT_CLASS_CLASS)].class_object; //,
	//SET_ROOT_CLASS(interface, INTERFACE);
	pvm_root.interface_class = pvm_internal_classes[pvm_iclass_by_root_index(PVM_ROOT_OBJECT_INTERFACE_CLASS)].class_object; //,
	//SET_ROOT_CLASS(code, CODE);
	pvm_root.code_class = pvm_internal_classes[pvm_iclass_by_root_index(PVM_ROOT_OBJECT_CODE_CLASS)].class_object; //,
	//SET_ROOT_CLASS(int, INT);
	pvm_root.int_class = pvm_internal_classes[pvm_iclass_by_root_index(PVM_ROOT_OBJECT_INT_CLASS)].class_object; //,
	//SET_ROOT_CLASS(string, STRING);
	pvm_root.string_class = pvm_internal_classes[pvm_iclass_by_root_index(PVM_ROOT_OBJECT_STRING_CLASS)].class_object; //,
	//SET_ROOT_CLASS(array, ARRAY);
	pvm_root.array_class = pvm_internal_classes[pvm_iclass_by_root_index(PVM_ROOT_OBJECT_ARRAY_CLASS)].class_object; //,
	//SET_ROOT_CLASS(page, PAGE);
	pvm_root.page_class = pvm_internal_classes[pvm_iclass_by_root_index(PVM_ROOT_OBJECT_PAGE_CLASS)].class_object; //,
	//SET_ROOT_CLASS(thread, THREAD);
	pvm_root.thread_class = pvm_internal_classes[pvm_iclass_by_root_index(PVM_ROOT_OBJECT_THREAD_CLASS)].class_object; //,
	//SET_ROOT_CLASS(call_frame, CALL_FRAME);
	pvm_root.call_frame_class = pvm_internal_classes[pvm_iclass_by_root_index(PVM_ROOT_OBJECT_CALL_FRAME_CLASS)].class_object; //,
	//SET_ROOT_CLASS(istack, ISTACK);
	pvm_root.istack_class = pvm_internal_classes[pvm_iclass_by_root_index(PVM_ROOT_OBJECT_ISTACK_CLASS)].class_object; //,
	//SET_ROOT_CLASS(ostack, OSTACK);
	pvm_root.ostack_class = pvm_internal_classes[pvm_iclass_by_root_index(PVM_ROOT_OBJECT_OSTACK_CLASS)].class_object; //,
	//SET_ROOT_CLASS(estack, ESTACK);
	pvm_root.estack_class = pvm_internal_classes[pvm_iclass_by_root_index(PVM_ROOT_OBJECT_ESTACK_CLASS)].class_object; //,
	//SET_ROOT_CLASS(long, LONG);
	pvm_root.long_class = pvm_internal_classes[pvm_iclass_by_root_index(PVM_ROOT_OBJECT_LONG_CLASS)].class_object; //,
	//SET_ROOT_CLASS(float, FLOAT);
	pvm_root.float_class = pvm_internal_classes[pvm_iclass_by_root_index(PVM_ROOT_OBJECT_FLOAT_CLASS)].class_object; //,
	//SET_ROOT_CLASS(double, DOUBLE);
	pvm_root.double_class = pvm_internal_classes[pvm_iclass_by_root_index(PVM_ROOT_OBJECT_DOUBLE_CLASS)].class_object; //,
	//SET_ROOT_CLASS(boot, BOOT);
	pvm_root.boot_class = pvm_internal_classes[pvm_iclass_by_root_index(PVM_ROOT_OBJECT_BOOT_CLASS)].class_object; //,
	//SET_ROOT_CLASS(tty, TTY);
	pvm_root.tty_class = pvm_internal_classes[pvm_iclass_by_root_index(PVM_ROOT_OBJECT_TTY_CLASS)].class_object; //,
	//SET_ROOT_CLASS(mutex, MUTEX);
	pvm_root.mutex_class = pvm_internal_classes[pvm_iclass_by_root_index(PVM_ROOT_OBJECT_MUTEX_CLASS)].class_object; //,
	//SET_ROOT_CLASS(cond, COND);
	pvm_root.cond_class = pvm_internal_classes[pvm_iclass_by_root_index(PVM_ROOT_OBJECT_COND_CLASS)].class_object; //,
	//SET_ROOT_CLASS(binary, BINARY);
	pvm_root.binary_class = pvm_internal_classes[pvm_iclass_by_root_index(PVM_ROOT_OBJECT_BINARY_CLASS)].class_object; //,
	//SET_ROOT_CLASS(bitmap, BITMAP);
	pvm_root.bitmap_class = pvm_internal_classes[pvm_iclass_by_root_index(PVM_ROOT_OBJECT_BITMAP_CLASS)].class_object; //,
	//SET_ROOT_CLASS(world, WORLD);
	pvm_root.world_class = pvm_internal_classes[pvm_iclass_by_root_index(PVM_ROOT_OBJECT_WORLD_CLASS)].class_object; //,
	//SET_ROOT_CLASS(closure, CLOSURE);
	pvm_root.closure_class = pvm_internal_classes[pvm_iclass_by_root_index(PVM_ROOT_OBJECT_CLOSURE_CLASS)].class_object; //,
	//SET_ROOT_CLASS(window, WINDOW);
	pvm_root.window_class = pvm_internal_classes[pvm_iclass_by_root_index(PVM_ROOT_OBJECT_WINDOW_CLASS)].class_object; //,
	//SET_ROOT_CLASS(directory, DIRECTORY);
	pvm_root.directory_class = pvm_internal_classes[pvm_iclass_by_root_index(PVM_ROOT_OBJECT_DIRECTORY_CLASS)].class_object; //,
	//SET_ROOT_CLASS(connection, CONNECTION);
	pvm_root.connection_class = pvm_internal_classes[pvm_iclass_by_root_index(PVM_ROOT_OBJECT_CONNECTION_CLASS)].class_object; //,
	//SET_ROOT_CLASS(sema, SEMA);
	pvm_root.sema_class = pvm_internal_classes[pvm_iclass_by_root_index(PVM_ROOT_OBJECT_SEMA_CLASS)].class_object; //
}

struct pvm_object     pvm_get_null_class() { return pvm_root.null_class; }
struct pvm_object     pvm_get_class_class() { return pvm_root.class_class; }
struct pvm_object     pvm_get_interface_class() { return pvm_root.interface_class; }
struct pvm_object     pvm_get_code_class() { return pvm_root.code_class; }
struct pvm_object     pvm_get_int_class() { return pvm_root.int_class; }
struct pvm_object     pvm_get_string_class() { return pvm_root.string_class; }
struct pvm_object     pvm_get_array_class() { return pvm_root.array_class; }
struct pvm_object     pvm_get_page_class() { return pvm_root.page_class; }
struct pvm_object     pvm_get_thread_class() { return pvm_root.thread_class; }
struct pvm_object     pvm_get_call_frame_class() { return pvm_root.call_frame_class; }
struct pvm_object     pvm_get_istack_class() { return pvm_root.istack_class; }
struct pvm_object     pvm_get_ostack_class() { return pvm_root.ostack_class; }
struct pvm_object     pvm_get_estack_class() { return pvm_root.estack_class; }
struct pvm_object     pvm_get_long_class() { return pvm_root.long_class; }
struct pvm_object     pvm_get_float_class() { return pvm_root.float_class; }
struct pvm_object     pvm_get_double_class() { return pvm_root.double_class; }
struct pvm_object     pvm_get_boot_class() { return pvm_root.boot_class; }
struct pvm_object     pvm_get_tty_class() { return pvm_root.tty_class; }
struct pvm_object     pvm_get_mutex_class() { return pvm_root.mutex_class; }
struct pvm_object     pvm_get_cond_class() { return pvm_root.cond_class; }
struct pvm_object     pvm_get_binary_class() { return pvm_root.binary_class; }
struct pvm_object     pvm_get_bitmap_class() { return pvm_root.bitmap_class; }
struct pvm_object     pvm_get_world_class() { return pvm_root.world_class; }
struct pvm_object     pvm_get_closure_class() { return pvm_root.closure_class; }
struct pvm_object     pvm_get_window_class() { return pvm_root.window_class; }
struct pvm_object     pvm_get_directory_class() { return pvm_root.directory_class; }
struct pvm_object     pvm_get_connection_class() { return pvm_root.connection_class; }
struct pvm_object     pvm_get_sema_class() { return pvm_root.sema_class; }

/**
*
* Boot code - pull in the very first user code.
* This code runs just once in system instance's life.
*
**/

static void pvm_boot()
{
	//struct pvm_object system_boot = pvm_object_create_fixed( pvm_get_boot_class() );
	struct pvm_object system_boot = pvm_create_object(pvm_get_boot_class());

	struct pvm_object user_boot_class;

	char boot_class[128];
	if (!phantom_getenv("root.boot", boot_class, sizeof(boot_class)))
		strcpy(boot_class, ".ru.dz.phantom.system.boot");

	if (pvm_load_class_from_module(boot_class, &user_boot_class))
	{
		printf("Unable to load boot class '%s'", boot_class);
		//pvm_exec_panic("Unable to load user boot class");
	}

	struct pvm_object user_boot = pvm_create_object(user_boot_class);


	struct pvm_object args[1] = { system_boot };
	pvm_exec_run_method(user_boot, 8, 1, args); // runs VM. code from exec.c
}

/*
*
* Kernel environment access.
* TODO: locks!
*
*/

static int get_env_name_pos(const char *name)
{
	int items = 0; // get_array_size(pvm_root.kernel_environment.data);

	int i;
	for (i = 0; i < items; i++)
	{
		pvm_object_t o = { 0, 0 }; // pvm_get_array_ofield(pvm_root.kernel_environment.data, i);
		//char *ed = pvm_get_str_data(o);
		char *ed = (char *)(((struct data_area_4_string *)&(o.data->da))->data);
		//int el = pvm_get_str_len(o);

		char *eqpos = strchr(ed, '=');
		if (eqpos == 0)
		{
			// Strange...
			continue;
		}
		int keylen = eqpos - ed;

		if (0 == strncmp(ed, name, keylen))
		{
			return i;
		}
	}

	return -1;
}

#define MAX_ENV_KEY 256
void phantom_setenv(const char *name, const char *value)
{
	char ename[MAX_ENV_KEY];

	strncpy(ename, name, MAX_ENV_KEY - 2);
	ename[MAX_ENV_KEY - 2] = '\0';
	strcat(ename, "=");

	pvm_object_t s = { 0,0 }; // pvm_create_string_object_binary_cat(ename, strlen(ename), value, strlen(value));

	int pos = get_env_name_pos(name);

	/*if (pos < 0)
		pvm_append_array(pvm_root.kernel_environment.data, s);
	else
		 pvm_set_array_ofield(pvm_root.kernel_environment.data, pos, s);
	*/
}

int phantom_getenv(const char *name, char *value, int vsize)
{
	int pos = get_env_name_pos(name);
	if (pos < 0) return 0;

	//assert(vsize > 1); // trailing zero and at least one char

	pvm_object_t o = { 0, 0 }; // pvm_get_array_ofield(pvm_root.kernel_environment.data, pos);
	if (o.data == 0) return 0;
	//char *ed = pvm_get_str_data(o);
	char *ed = (char *)(((struct data_area_4_string *)&(o.data->da))->data);
	//int el = pvm_get_str_len(o);
	int el = (int)(((struct data_area_4_string *)&(o.data->da))->length);

	//printf("full '%.*s'\n", el, ed );

	char *eqpos = strchr(ed, '=');
	if (eqpos == 0)
	{
		*value = '\0';
		return 0;
	}

	eqpos++; // skip '='

	el -= (eqpos - ed);

	el++; // add place for trailing zero

	if (vsize > el) vsize = el;

	strncpy(value, eqpos, vsize);
	value[vsize - 1] = '\0';

	return 1;
}



static void load_kernel_boot_env(void)
{
	// Default env first
	phantom_setenv("root.shell", ".ru.dz.phantom.system.shell");
	phantom_setenv("root.init", ".ru.dz.phantom.system.init");
	phantom_setenv("root.boot", ".ru.dz.phantom.system.boot");


	int i = 0; // main_envc;
	const char **ep = ""; // main_env;
	while (i--)
	{
		const char *e = *ep++;

		//const char *eq = index(e, '='); // index is depricated
		const char *eq = strchr(e, '=');

		if (eq == 0)
		{
			printf("Warning: env without =, '%s'\n", e);
			continue;
		}

		int nlen = eq - e;
		eq++; // skip =

		char buf[128 + 1];
		if (nlen > 128) nlen = 128;

		strncpy(buf, e, nlen);
		buf[nlen] = '\0';

		printf("Loading env '%s'='%s'\n", buf, eq);
		phantom_setenv(buf, eq);
	}
}
