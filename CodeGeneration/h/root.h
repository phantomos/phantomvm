﻿#pragma once

#include "object.h"
#include "object_flags.h"
#include "internal_da.h"
#include "internal.h"

struct pvm_root_t
{
	struct pvm_object           null_class;
	struct pvm_object           class_class;
	struct pvm_object           interface_class;
	struct pvm_object           code_class;
	struct pvm_object           int_class;
	struct pvm_object           string_class;
	struct pvm_object           array_class;
	struct pvm_object           page_class;
	struct pvm_object           thread_class;
	struct pvm_object           call_frame_class;
	struct pvm_object           istack_class;
	struct pvm_object           ostack_class;
	struct pvm_object           estack_class;
	struct pvm_object           long_class;
	struct pvm_object           float_class;
	struct pvm_object           double_class;
	struct pvm_object           boot_class;
	struct pvm_object           tty_class;
	struct pvm_object           mutex_class;
	struct pvm_object           cond_class;
	struct pvm_object           binary_class;
	struct pvm_object           bitmap_class;
	struct pvm_object           world_class;
	struct pvm_object           closure_class;
	struct pvm_object           window_class;
	struct pvm_object           directory_class;
	struct pvm_object           connection_class;
	struct pvm_object           sema_class;
};

struct pvm_root_t pvm_root;

static void set_root_from_table();

struct pvm_object     pvm_get_null_class();
struct pvm_object     pvm_get_class_class();
struct pvm_object     pvm_get_interface_class();
struct pvm_object     pvm_get_code_class();
struct pvm_object     pvm_get_int_class();
struct pvm_object     pvm_get_string_class();
struct pvm_object     pvm_get_array_class();
struct pvm_object     pvm_get_page_class();
struct pvm_object     pvm_get_thread_class();
struct pvm_object     pvm_get_call_frame_class();
struct pvm_object     pvm_get_istack_class();
struct pvm_object     pvm_get_ostack_class();
struct pvm_object     pvm_get_estack_class();
struct pvm_object     pvm_get_long_class();
struct pvm_object     pvm_get_float_class();
struct pvm_object     pvm_get_double_class();
struct pvm_object     pvm_get_boot_class();
struct pvm_object     pvm_get_tty_class();
struct pvm_object     pvm_get_mutex_class();
struct pvm_object     pvm_get_cond_class();
struct pvm_object     pvm_get_binary_class();
struct pvm_object     pvm_get_bitmap_class();
struct pvm_object     pvm_get_world_class();
struct pvm_object     pvm_get_closure_class();
struct pvm_object     pvm_get_window_class();
struct pvm_object     pvm_get_directory_class();
struct pvm_object     pvm_get_connection_class();
struct pvm_object     pvm_get_sema_class();

// Max number of methods in system class - determines size of interface class
#define N_SYS_METHODS 64

// In addition to class IDs definitions from "internal.h" here is defined IDs for system objects.

// Runtime restoration facilities

// NULL object (is_null() checks against this one)
#define PVM_ROOT_OBJECT_NULL 64

#define PVM_ROOT_OBJECT_SYSINTERFACE 65

// Pointer to actual thread list, used to restart threads
#define PVM_ROOT_OBJECT_THREAD_LIST 66

#define PVM_ROOT_OBJECT_USERS_LIST 67

#define PVM_ROOT_OBJECT_RESTART_LIST 68

#define PVM_ROOT_OBJECT_KERNEL_ENVIRONMENT 69

#define PVM_ROOT_OBJECT_OS_ENTRY 70

// Root object directory
#define PVM_ROOT_OBJECT_ROOT_DIR 71
#define PVM_ROOT_KERNEL_STATISTICS 72
#define PVM_ROOT_OBJECTS_COUNT (PVM_ROOT_KERNEL_STATISTICS+31)

static void pvm_boot();

static int get_env_name_pos(const char *name);

void phantom_setenv(const char *name, const char *value);

int phantom_getenv(const char *name, char *value, int vsize);

static void load_kernel_boot_env(void);
