﻿#pragma once

// for general object definitions
#include "object.h"
// for data_area_4_thread
#include "internal_da.h"

typedef int(*syscall_func_t)(struct pvm_object o, struct data_area_4_thread *tc);

// -------- syscall function (methods) for NULL class. --------

int si_void_0_construct(struct pvm_object, struct data_area_4_thread *tc);
int si_void_1_destruct(struct pvm_object, struct data_area_4_thread *tc);
int si_void_2_class(struct pvm_object, struct data_area_4_thread *tc);
int si_void_3_clone(struct pvm_object, struct data_area_4_thread *tc);
int si_void_4_equals(struct pvm_object, struct data_area_4_thread *tc);
int si_void_5_tostring(struct pvm_object, struct data_area_4_thread *tc);
int si_void_6_toXML(struct pvm_object, struct data_area_4_thread *tc);
int si_void_7_fromXML(struct pvm_object, struct data_area_4_thread *tc);
int si_void_8_def_op_1(struct pvm_object, struct data_area_4_thread *tc);
int si_void_9_def_op_2(struct pvm_object, struct data_area_4_thread *tc);
int si_void_15_hashcode(struct pvm_object, struct data_area_4_thread *tc);

extern syscall_func_t syscall_table_4_void[];

extern int n_syscall_table_4_void;

// -------- syscall function (methods) for CLASS class. --------

int si_class_class_5_tostring(struct pvm_object, struct data_area_4_thread *tc);
int si_class_class_8_new_class(struct pvm_object, struct data_area_4_thread *tc);
int si_class_10_set_static(struct pvm_object, struct data_area_4_thread *tc);
int si_class_11_get_static(struct pvm_object, struct data_area_4_thread *tc);
int si_class_14_instanceof(struct pvm_object, struct data_area_4_thread *tc);

extern syscall_func_t syscall_table_4_class[];

extern int n_syscall_table_4_class;

// -------- syscall function (methods) for INTERFACE class. --------

int si_interface_5_tostring(struct pvm_object, struct data_area_4_thread *tc);

extern syscall_func_t syscall_table_4_interface[];

extern int n_syscall_table_4_interface;

// -------- syscall function (methods) for CODE class. --------

int si_code_5_tostring(struct pvm_object, struct data_area_4_thread *tc);

extern syscall_func_t syscall_table_4_code[];

extern int n_syscall_table_4_code;

// -------- syscall function (methods) for INT class. --------

int si_int_3_clone(struct pvm_object, struct data_area_4_thread *tc);
int si_int_4_equals(struct pvm_object, struct data_area_4_thread *tc);
int si_int_5_tostring(struct pvm_object, struct data_area_4_thread *tc);
int si_int_6_toXML(struct pvm_object, struct data_area_4_thread *tc);

extern syscall_func_t syscall_table_4_int[];

extern int n_syscall_table_4_int;

// -------- syscall function (methods) for STRING class. --------

int si_string_3_clone(struct pvm_object, struct data_area_4_thread *tc);
int si_string_4_equals(struct pvm_object, struct data_area_4_thread *tc);
int si_string_5_tostring(struct pvm_object, struct data_area_4_thread *tc);
int si_string_8_substring(struct pvm_object, struct data_area_4_thread *tc);
int si_string_9_charat(struct pvm_object, struct data_area_4_thread *tc);
int si_string_10_concat(struct pvm_object, struct data_area_4_thread *tc);
int si_string_11_length(struct pvm_object, struct data_area_4_thread *tc);
int si_string_12_find(struct pvm_object, struct data_area_4_thread *tc);

extern syscall_func_t syscall_table_4_string[];

extern int n_syscall_table_4_string;

// -------- syscall function (methods) for ARRAY class. --------

int si_array_5_tostring(struct pvm_object, struct data_area_4_thread *tc);
int si_array_8_get_iterator(struct pvm_object, struct data_area_4_thread *tc);
int si_array_9_get_subarray(struct pvm_object, struct data_area_4_thread *tc);
int si_array_10_get(struct pvm_object, struct data_area_4_thread *tc);
int si_array_11_set(struct pvm_object, struct data_area_4_thread *tc);
int si_array_12_size(struct pvm_object, struct data_area_4_thread *tc);

extern syscall_func_t syscall_table_4_array[];

extern int n_syscall_table_4_array;

// -------- syscall function (methods) for PAGE class. --------

int si_page_5_tostring(struct pvm_object, struct data_area_4_thread *tc);

extern syscall_func_t syscall_table_4_page[];

extern int n_syscall_table_4_page;

// -------- syscall function (methods) for THREAD class. --------

int si_thread_5_tostring(struct pvm_object, struct data_area_4_thread *tc);
int si_thread_10_pause(struct pvm_object, struct data_area_4_thread *tc);
int si_thread_11_continue(struct pvm_object, struct data_area_4_thread *tc);
int si_thread_12_getEnvironment(struct pvm_object, struct data_area_4_thread *tc);
int si_thread_13_getUser(struct pvm_object, struct data_area_4_thread *tc);
int si_thread_14_getOsInterface(struct pvm_object, struct data_area_4_thread *tc);

extern syscall_func_t syscall_table_4_thread[];

extern int n_syscall_table_4_thread;

// -------- syscall function (methods) for CALL_FRAME class. --------

int si_call_frame_5_tostring(struct pvm_object, struct data_area_4_thread *tc);

extern syscall_func_t syscall_table_4_call_frame[];

extern int n_syscall_table_4_call_frame;

// -------- syscall function (methods) for ISTACK class. --------

int si_istack_5_tostring(struct pvm_object, struct data_area_4_thread *tc);

extern syscall_func_t syscall_table_4_istack[];

extern int n_syscall_table_4_istack;

// -------- syscall function (methods) for OSTACK class. --------

int si_ostack_5_tostring(struct pvm_object, struct data_area_4_thread *tc);

extern syscall_func_t syscall_table_4_ostack[];

extern int n_syscall_table_4_ostack;

// -------- syscall function (methods) for ESTACK class. --------

int si_estack_5_tostring(struct pvm_object, struct data_area_4_thread *tc);

extern syscall_func_t syscall_table_4_estack[];

extern int n_syscall_table_4_estack;

// -------- syscall function (methods) for LONG class. --------

int si_long_3_clone(struct pvm_object, struct data_area_4_thread *tc);
int si_long_4_equals(struct pvm_object, struct data_area_4_thread *tc);
int si_long_5_tostring(struct pvm_object, struct data_area_4_thread *tc);
int si_long_6_toXML(struct pvm_object, struct data_area_4_thread *tc);

extern syscall_func_t syscall_table_4_long[];

extern int n_syscall_table_4_long;

// -------- syscall function (methods) for FLOAT class. --------

int si_float_3_clone(struct pvm_object, struct data_area_4_thread *tc);
int si_float_4_equals(struct pvm_object, struct data_area_4_thread *tc);
int si_float_5_tostring(struct pvm_object, struct data_area_4_thread *tc);
int si_float_6_toXML(struct pvm_object, struct data_area_4_thread *tc);

extern syscall_func_t syscall_table_4_float[];

extern int n_syscall_table_4_float;

// -------- syscall function (methods) for DOUBLE class. --------

int si_double_3_clone(struct pvm_object, struct data_area_4_thread *tc);
int si_double_4_equals(struct pvm_object, struct data_area_4_thread *tc);
int si_double_5_tostring(struct pvm_object, struct data_area_4_thread *tc);
int si_double_6_toXML(struct pvm_object, struct data_area_4_thread *tc);

extern syscall_func_t syscall_table_4_double[];

extern int n_syscall_table_4_double;

// -------- syscall function (methods) for BOOT class. --------

int si_bootstrap_5_tostring(struct pvm_object, struct data_area_4_thread *tc);
int si_bootstrap_8_load_class(struct pvm_object, struct data_area_4_thread *tc);
int si_bootstrap_16_print(struct pvm_object, struct data_area_4_thread *tc);
int si_bootstrap_17_register_class_loader(struct pvm_object, struct data_area_4_thread *tc);
int si_bootstrap_18_thread(struct pvm_object, struct data_area_4_thread *tc);
int si_bootstrap_19_create_binary(struct pvm_object, struct data_area_4_thread *tc);
int si_bootstrap_20_set_screen_background(struct pvm_object, struct data_area_4_thread *tc);
int si_bootstrap_21_sleep(struct pvm_object, struct data_area_4_thread *tc);
int si_bootstrap_22_set_os_interface(struct pvm_object, struct data_area_4_thread *tc);
int si_bootstrap_23_getenv(struct pvm_object, struct data_area_4_thread *tc);

extern syscall_func_t syscall_table_4_boot[];

extern int n_syscall_table_4_boot;

// -------- syscall function (methods) for TTY class. --------

int tostring_5(struct pvm_object, struct data_area_4_thread *tc);
int getwc_16(struct pvm_object, struct data_area_4_thread *tc);
int putws_17(struct pvm_object, struct data_area_4_thread *tc);
int debug_18(struct pvm_object, struct data_area_4_thread *tc);
int gotoxy_19(struct pvm_object, struct data_area_4_thread *tc);
int clear_20(struct pvm_object, struct data_area_4_thread *tc);
int setcolor_21(struct pvm_object, struct data_area_4_thread *tc);
int fill_22(struct pvm_object, struct data_area_4_thread *tc);
int putblock_23(struct pvm_object, struct data_area_4_thread *tc);
int tty_setWinPos_24(struct pvm_object, struct data_area_4_thread *tc);
int tty_setWinTitle_25(struct pvm_object, struct data_area_4_thread *tc);

extern syscall_func_t syscall_table_4_tty[];

extern int n_syscall_table_4_tty;

// -------- syscall function (methods) for MUTEX class. --------

int si_mutex_5_tostring(struct pvm_object, struct data_area_4_thread *tc);
int si_mutex_8_lock(struct pvm_object, struct data_area_4_thread *tc);
int si_mutex_9_unlock(struct pvm_object, struct data_area_4_thread *tc);

extern syscall_func_t syscall_table_4_mutex[];

extern int n_syscall_table_4_mutex;

// -------- syscall function (methods) for COND class. --------

int si_cond_5_tostring(struct pvm_object, struct data_area_4_thread *tc);
int si_cond_8_wait(struct pvm_object, struct data_area_4_thread *tc);
int si_cond_9_twait(struct pvm_object, struct data_area_4_thread *tc);
int si_cond_10_broadcast(struct pvm_object, struct data_area_4_thread *tc);
int si_cond_11_signal(struct pvm_object, struct data_area_4_thread *tc);

extern syscall_func_t syscall_table_4_cond[];

extern int n_syscall_table_4_cond;

// -------- syscall function (methods) for BINARY class. --------

int si_binary_5_tostring(struct pvm_object, struct data_area_4_thread *tc);
int si_binary_8_getbyte(struct pvm_object, struct data_area_4_thread *tc);
int si_binary_9_setbyte(struct pvm_object, struct data_area_4_thread *tc);
int si_binary_10_setrange(struct pvm_object, struct data_area_4_thread *tc);

extern syscall_func_t syscall_table_4_binary[];

extern int n_syscall_table_4_binary;

// -------- syscall function (methods) for BITMAP class. --------

int si_bitmap_5_tostring(struct pvm_object, struct data_area_4_thread *tc);
int si_bitmap_8_fromstring(struct pvm_object, struct data_area_4_thread *tc);
int si_bitmap_9_paintto(struct pvm_object, struct data_area_4_thread *tc);

extern syscall_func_t syscall_table_4_bitmap[];

extern int n_syscall_table_4_bitmap;

// -------- syscall function (methods) for WORLD class. --------

int si_world_5_tostring(struct pvm_object, struct data_area_4_thread *tc);
int si_world_8_getMyThread(struct pvm_object, struct data_area_4_thread *tc);

extern syscall_func_t syscall_table_4_world[];

extern int n_syscall_table_4_world;

// -------- syscall function (methods) for CLOSURE class. --------

int si_closure_9_getordinal(struct pvm_object, struct data_area_4_thread *tc);
int si_closure_10_setordinal(struct pvm_object, struct data_area_4_thread *tc);
int si_closure_11_setobject(struct pvm_object, struct data_area_4_thread *tc);

extern syscall_func_t syscall_table_4_closure[];

extern int n_syscall_table_4_closure;

// -------- syscall function (methods) for WINDOW class. --------

int si_window_5_tostring(struct pvm_object, struct data_area_4_thread *tc);
int win_getXSize(struct pvm_object, struct data_area_4_thread *tc);
int win_getYSize(struct pvm_object, struct data_area_4_thread *tc);
int win_getX(struct pvm_object, struct data_area_4_thread *tc);
int win_getY(struct pvm_object, struct data_area_4_thread *tc);
int win_clear_20(struct pvm_object, struct data_area_4_thread *tc);
int win_fill_21(struct pvm_object, struct data_area_4_thread *tc);
int win_setFGcolor_22(struct pvm_object, struct data_area_4_thread *tc);
int win_setBGcolor_23(struct pvm_object, struct data_area_4_thread *tc);
int win_putString_24(struct pvm_object, struct data_area_4_thread *tc);
int win_putImage_25(struct pvm_object, struct data_area_4_thread *tc);
int win_setSize_26(struct pvm_object, struct data_area_4_thread *tc);
int win_setPos_27(struct pvm_object, struct data_area_4_thread *tc);
int win_drawLine_28(struct pvm_object, struct data_area_4_thread *tc);
int win_drawBox_29(struct pvm_object, struct data_area_4_thread *tc);
int win_fillBox_30(struct pvm_object, struct data_area_4_thread *tc);
int win_fillEllipse_31(struct pvm_object, struct data_area_4_thread *tc);
int win_setHandler_32(struct pvm_object, struct data_area_4_thread *tc);
int win_setTitle_33(struct pvm_object, struct data_area_4_thread *tc);
int win_update_34(struct pvm_object, struct data_area_4_thread *tc);
int win_scrollHor_35(struct pvm_object, struct data_area_4_thread *tc);

extern syscall_func_t syscall_table_4_window[];

extern int n_syscall_table_4_window;

// -------- syscall function (methods) for DIRECTORY class. --------

int si_directory_4_equals(struct pvm_object, struct data_area_4_thread *tc);
int si_directory_5_tostring(struct pvm_object, struct data_area_4_thread *tc);
int si_directory_8_put(struct pvm_object, struct data_area_4_thread *tc);
int si_directory_9_get(struct pvm_object, struct data_area_4_thread *tc);
int si_directory_10_remove(struct pvm_object, struct data_area_4_thread *tc);
int si_directory_11_size(struct pvm_object, struct data_area_4_thread *tc);
int si_directory_12_iterate(struct pvm_object, struct data_area_4_thread *tc);

extern syscall_func_t syscall_table_4_directory[];

extern int n_syscall_table_4_directory;

// -------- syscall function (methods) for CONNECTION class. --------

int si_connection_5_tostring(struct pvm_object, struct data_area_4_thread *tc);
int si_connection_8_connect(struct pvm_object, struct data_area_4_thread *tc);
int si_connection_9_disconnect(struct pvm_object, struct data_area_4_thread *tc);
int si_connection_10_check(struct pvm_object, struct data_area_4_thread *tc);
int si_connection_11_do(struct pvm_object, struct data_area_4_thread *tc);
int si_connection_12_set_callback(struct pvm_object, struct data_area_4_thread *tc);
int si_connection_13_blocking(struct pvm_object, struct data_area_4_thread *tc);

extern syscall_func_t syscall_table_4_connection[];

extern int n_syscall_table_4_connection;

// -------- syscall function (methods) for SEMA class. --------

int si_sema_5_tostring(struct pvm_object, struct data_area_4_thread *tc);
int si_sema_8_acquire(struct pvm_object, struct data_area_4_thread *tc);
int si_sema_9_tacquire(struct pvm_object, struct data_area_4_thread *tc);
int si_sema_10_zero(struct pvm_object, struct data_area_4_thread *tc);
int si_sema_11_release(struct pvm_object, struct data_area_4_thread *tc);

extern syscall_func_t syscall_table_4_sema[];

extern int n_syscall_table_4_sema;

