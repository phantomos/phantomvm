﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeGeneration
{
    class SyscallH
    {
        static public void Test(string[] args)
        {
            string sqlCon = @"Data Source=(LocalDB)\MSSQLLocalDB;" +
                "AttachDbFilename=" + Path.Combine(Directory.GetCurrentDirectory(), "pvm.mdf") + ";" +
                "Integrated Security=True; Connect Timeout=5";
            SqlConnection conn = new SqlConnection(sqlCon);
            conn.Open();

            string command = string.Format("SELECT [Id], [Name], [SyscalTable] FROM [{0}] WHERE [isEnabled] = 1 ORDER BY [Id]", "InternalClasses");
            SqlCommand comm = new SqlCommand(command, conn);

            SqlDataReader reader = comm.ExecuteReader();

            while (reader.Read())
            {
                string command2 = string.Format("SELECT [Id], [Ordinal], [Function] FROM [SyscallTable2Function] WHERE [Table] = '{0}' ORDER BY [Ordinal]", reader["SyscalTable"]);
                using (var conn2 = new SqlConnection(sqlCon))
                using (var comm2 = new SqlCommand(command2, conn2))
                {
                    try
                    {
                        conn2.Open();
                        SqlDataReader reader2 = comm2.ExecuteReader();
                        while (reader2.Read())
                        {
                            Console.WriteLine(string.Format("{0}, {1}, {2}", reader["SyscalTable"], reader2["Ordinal"], reader2["Function"]));
                        }
                        reader2.Close();
                        conn2.Close();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error executing.", ex);
                    }
                }
            }
            reader.Close();

            /*string command = string.Format("select [{0}], [{1}] from [{2}] order by [{0}]", "field1", "field2", "Table");
            SqlCommand comm = new SqlCommand(command, conn);

            SqlDataReader reader = comm.ExecuteReader();
            bool loop = reader.Read();

            while (loop)
            {
                Console.Write(reader["field2"]);
                loop = reader.Read();
                if (loop) Console.WriteLine(",");
            }
            */

            conn.Close();
        }
    }
}
