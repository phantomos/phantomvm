﻿#pragma once

// general object definitions
#include "object.h"
// object syscall functions (methods)
#include "syscall.h"
// for init_func_t and o_restart_func_t
#include "init_object.h"
// gc iterator and finalizer function
#include "gc_object.h"

// internal number of null class
#define PVM_ROOT_OBJECT_NULL_CLASS 0
// internal number of class class
#define PVM_ROOT_OBJECT_CLASS_CLASS 1
// internal number of interface class
#define PVM_ROOT_OBJECT_INTERFACE_CLASS 2
// internal number of code class
#define PVM_ROOT_OBJECT_CODE_CLASS 3
// internal number of int class
#define PVM_ROOT_OBJECT_INT_CLASS 4
// internal number of string class
#define PVM_ROOT_OBJECT_STRING_CLASS 5
// internal number of array class
#define PVM_ROOT_OBJECT_ARRAY_CLASS 6
// internal number of page class
#define PVM_ROOT_OBJECT_PAGE_CLASS 7
// internal number of thread class
#define PVM_ROOT_OBJECT_THREAD_CLASS 8
// internal number of call_frame class
#define PVM_ROOT_OBJECT_CALL_FRAME_CLASS 9
// internal number of istack class
#define PVM_ROOT_OBJECT_ISTACK_CLASS 10
// internal number of ostack class
#define PVM_ROOT_OBJECT_OSTACK_CLASS 11
// internal number of estack class
#define PVM_ROOT_OBJECT_ESTACK_CLASS 12
// internal number of long class
#define PVM_ROOT_OBJECT_LONG_CLASS 13
// internal number of float class
#define PVM_ROOT_OBJECT_FLOAT_CLASS 14
// internal number of double class
#define PVM_ROOT_OBJECT_DOUBLE_CLASS 15
// internal number of boot class
#define PVM_ROOT_OBJECT_BOOT_CLASS 16
// internal number of tty class
#define PVM_ROOT_OBJECT_TTY_CLASS 17
// internal number of mutex class
#define PVM_ROOT_OBJECT_MUTEX_CLASS 20
// internal number of cond class
#define PVM_ROOT_OBJECT_COND_CLASS 21
// internal number of binary class
#define PVM_ROOT_OBJECT_BINARY_CLASS 22
// internal number of bitmap class
#define PVM_ROOT_OBJECT_BITMAP_CLASS 23
// internal number of world class
#define PVM_ROOT_OBJECT_WORLD_CLASS 24
// internal number of closure class
#define PVM_ROOT_OBJECT_CLOSURE_CLASS 25
// internal number of window class
#define PVM_ROOT_OBJECT_WINDOW_CLASS 29
// internal number of directory class
#define PVM_ROOT_OBJECT_DIRECTORY_CLASS 30
// internal number of connection class
#define PVM_ROOT_OBJECT_CONNECTION_CLASS 31
// internal number of sema class
#define PVM_ROOT_OBJECT_SEMA_CLASS 32

struct internal_class
{
	const char *                name;
	int                         root_index; 		// index into the root object, where class is stored
	syscall_func_t *            syscalls_table;         // syscalls implementations
	int                         *syscalls_table_size_ptr;    // n of syscalls
	init_func_t                 init;                   // constructor
	gc_iterator_func_t          iter;                   // Call func for all the object pointer contained in this internal object
	gc_finalizer_func_t         finalizer;
	o_restart_func_t            restart;                // Called on OS restart if object put itself to restart list, see root.c
	int                         da_size;                // Data area size
	int                         flags;
	struct pvm_object           class_object;           // inited on start, used for lookup
};

extern struct internal_class pvm_internal_classes[];

extern int pvm_n_internal_classes;

int pvm_iclass_by_root_index(int index);

int pvm_iclass_by_class(struct pvm_object_storage *cs);

struct pvm_object pvm_lookup_internal_class(struct pvm_object name);

